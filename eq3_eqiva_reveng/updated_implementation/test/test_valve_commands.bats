#!/usr/bin/env bats

setup() {
    . ./valve_commands.sh
}

@test "send_init command composition" {
    run send_init FF:FF:FF:FF:FF:FF
    [ "$status" -eq 0 ]
    if [[ "$output" =~ ^03 ]]; then correct=1; else correct=0; fi
    [ "$correct" -eq 1 ]

    #lack of a parameter causes an error
    run send_init
    [ "$status" -ne 0 ]
    [ "$output" = "Usage: send_init <device_address>" ]
}

@test "boost_mode/stop_boost_mode behavior" {
    run boost_mode FF:FF:FF:FF:FF:FF
    [ "$status" -eq 0 ]

    #lack of a parameter causes an error
    run boost_mode
    [ "$status" -ne 0 ]
    [ "$output" = "Usage: boost_mode <device_address>" ]

    run stop_boost_mode FF:FF:FF:FF:FF:FF
    [ "$status" -eq 0 ]

    #lack of a parameter causes an error
    run stop_boost_mode
    [ "$status" -ne 0 ]
    [ "$output" = "Usage: stop_boost_mode <device_address>" ]
}

@test "auto_mode/manual_mode behavior" {
    run auto_mode FF:FF:FF:FF:FF:FF
    [ "$status" -eq 0 ]

    #lack of a parameter causes an error
    run auto_mode
    [ "$status" -ne 0 ]
    [ "$output" = "Usage: auto_mode <device_address>" ]

    run manual_mode FF:FF:FF:FF:FF:FF
    [ "$status" -eq 0 ]

    #lack of a parameter causes an error
    run manual_mode
    [ "$status" -ne 0 ]
    [ "$output" = "Usage: manual_mode <device_address>" ]
}

@test "set_temperature command composition" {
    run set_temperature FF:FF:FF:FF:FF:FF 18
    [ "$output" = "4124" ]
    [ "$status" -eq 0 ]

    run set_temperature FF:FF:FF:FF:FF:FF 21.5
    [ "$output" = "412b" ]
    [ "$status" -eq 0 ]

    #negative temperature causes an error
    run set_temperature FF:FF:FF:FF:FF:FF -21
    [ "$status" -ne 0 ]

    #lack of a parameter causes an error
    run set_temperature FF:FF:FF:FF:FF:FF
    [ "$output" = "Usage: set_temperature <device_address> <temperature>" ]
    [ "$status" -ne 0 ]
}

@test "set_comfort_reduction_temp command composition" {
    run set_comfort_reduction_temp FF:FF:FF:FF:FF:FF 21 18
    [ "$output" = "112a24" ]
    [ "$status" -eq 0 ]

    #negative temperature causes an error
    run set_comfort_reduction_temp FF:FF:FF:FF:FF:FF -20.5 18
    [ "$status" -ne 0 ]

    #negative temperature causes an error
    run set_comfort_reduction_temp FF:FF:FF:FF:FF:FF 20.5 -18
    [ "$status" -ne 0 ]

    #lack of a parameter causes an error
    run set_comfort_reduction_temp FF:FF:FF:FF:FF:FF 18
    [ "$output" = "Usage: set_comfort_reduction_temp <device_address> <comfort_temp> <reduct_temp>" ]
    [ "$status" -ne 0 ]
}

@test "holiday_mode command composition" {
    run holiday_mode FF:FF:FF:FF:FF:FF 25/06/2020 10:30 18
    [ "$output" = "40A419141506" ]
    [ "$status" -eq 0 ]

    #wrong date format causes an error
    run holiday_mode FF:FF:FF:FF:FF:FF 25-06-20 10:30 18
    [ "$status" -ne 0 ]

    #wrong time causes an error
    run holiday_mode FF:FF:FF:FF:FF:FF 25/06/20 3f:30 18
    [ "$status" -ne 0 ]

    #negative temperature causes an error
    run holiday_mode FF:FF:FF:FF:FF:FF 25/06/2020 10:30 -18
    [ "$status" -ne 0 ]

    #lack of one or more parameters causes an error
    run holiday_mode FF:FF:FF:FF:FF:FF 25/06/2020
    [ "$output" = "Usage: holiday_mode <device addr> <DD/MM/YYYY> <hh:mm> <temperature>" ]
    [ "$status" -ne 0 ]
}

@test "read_profile command composition" {
    run read_profile FF:FF:FF:FF:FF:FF 2
    [ "$output" = "2002" ]
    [ "$status" -eq 0 ]

    #day out of range causes an error
    run read_profile FF:FF:FF:FF:FF:FF 7
    [ "$status" -ne 0 ]

    #lack of parameters causes an error
    run read_profile FF:FF:FF:FF:FF:FF
    [ "$output" = "Usage: read_profile <device_address> <day>" ]
    [ "$status" -ne 0 ]
}

@test "set_profile command composition" {
    run set_profile FF:FF:FF:FF:FF:FF 2 18.5/14:00 21/00:00
    [ "$output" = "100225542a90" ]
    [ "$status" -eq 0 ]

    run set_profile FF:FF:FF:FF:FF:FF 2 18.5/14:00 21/24:00
    [ "$output" = "100225542a90" ]
    [ "$status" -eq 0 ]

    #last interval must end at midnight
    run set_profile FF:FF:FF:FF:FF:FF 2 18.5/14:00 21/23:00
    [ "$status" -ne 0 ]

    #wrong syntax causes an error
    run set_profile FF:FF:FF:FF:FF:FF 2 18-14:00 21/00:00
    [ "$status" -ne 0 ]

    #invalid time causes and error
    run set_profile FF:FF:FF:FF:FF:FF 2 18/10:72 21/00:00
    [ "$status" -ne 0 ]

    #lack of parameters causes an error
    run set_profile FF:FF:FF:FF:FF:FF 2
    [ "$output" = "Usage: set_profile <device address> <day> <int1> [int2] [int3] [int4] [int5] [int6] [int7]" ]
    [ "$status" -ne 0 ]

    #day out of range causes an error
    run set_profile FF:FF:FF:FF:FF:FF 7 21/00:00
    [ "$output" = "Week goes from 00 (saturday) to 06 (friday)." ]
    [ "$status" -ne 0 ]
}

@test "lock/unlock behavior" {
    run lock FF:FF:FF:FF:FF:FF
    [ "$status" -eq 0 ]

    #lack of a parameter causes an error
    run lock
    [ "$status" -ne 0 ]
    [ "$output" = "Usage: lock <device_address>" ]

    run unlock FF:FF:FF:FF:FF:FF
    [ "$status" -eq 0 ]

    #lack of a parameter causes an error
    run unlock
    [ "$status" -ne 0 ]
    [ "$output" = "Usage: unlock <device_address>" ]
}

@test "set_window command composition" {
    run set_window FF:FF:FF:FF:FF:FF 12 15
    [ "$status" -eq 0 ]
    [ "$output" = "141803" ]

    #rounds (up) if duration is not a multiple of 5
    run set_window FF:FF:FF:FF:FF:FF 12 23
    [ "$status" -eq 0 ]
    [ "$output" = "141805" ]

    #lack of a parameter causes an error
    run set_window FF:FF:FF:FF:FF:FF 12
    [ "$status" -ne 0 ]
    [ "$output" = "Usage: set_window <device_address> <temperature> <duration>" ]

    #negative temperature causes an error
    run set_window FF:FF:FF:FF:FF:FF -12 15
    [ "$status" -ne 0 ]

    #duration > 60 causes an error
    run set_window FF:FF:FF:FF:FF:FF 12 65
    [ "$status" -ne 0 ]

    #duration < 0 causes an error
    run set_window FF:FF:FF:FF:FF:FF 12 -5
    [ "$status" -ne 0 ]
}

@test "set_offset command composition" {
    run set_offset FF:FF:FF:FF:FF:FF 2.0
    [ "$output" = "130b" ]
    [ "$status" -eq 0 ]

    run set_offset FF:FF:FF:FF:FF:FF -3.5
    [ "$output" = "1300" ]
    [ "$status" -eq 0 ]

    #lack of a parameter causes an error
    run set_offset FF:FF:FF:FF:FF:FF
    [ "$status" -ne 0 ]
    [ "$output" = "Usage: set_offset <device_address> <temperature>" ]

    #temperature > 3.5 causes an error
    run set_offset FF:FF:FF:FF:FF:FF 4.5
    [ "$status" -ne 0 ]
    [ "$output" = "Temperature must be between -3.5 and 3.5." ]

    #temperature < -3.5 causes an error
    run set_offset FF:FF:FF:FF:FF:FF -3.6
    [ "$status" -ne 0 ]
    [ "$output" = "Temperature must be between -3.5 and 3.5." ]
}
