#!/bin/bash

#USAGE:
#	./profile_req day_name valve_name [valve_name ...]
#	Requires and prints the daily schedule for one or more valves
#	<day_name> must be in english and is NOT case sensitive

. ./valve_commands.sh

if [ -z $2 ] ; then
	printf "Usage: ./profile_req day_name valve_name [valve_name ...]\n"
	exit 1
fi

#check the name of the day contains only letters
if [[ ! "$1" =~ ^[a-zA-Z]+$ ]]; then
	printf "The name of the day must be in English (without digits). E.g. 'saturday'\n"
	exit 1
fi

#check if bluetooth is active
check_bt_status

#convert to the required format
day=$( get_day_number $1 )

for index in $(seq 2 $#); do
	name=${!index}
	address=$( search_by_name $name $VALVE_FILE )

	if [[ "$address" != "-1" && $(validate_mac $address) != "-1" ]]; then

		notification=$(read_profile $address $day)

		if [[ ! -z $notification ]]; then
			printf "\n%s:\n" "$name"
			parse_return_value $notification
		else
			printf "\n%s: error. try to increase the timeout or move close to the valve\n" "$name"
		fi
	else
		printf "\n%s: not found in '%s' or invalid MAC address\n" "$name" "$VALVE_FILE"
	fi
done
