#!/bin/bash

#USAGE:
#	./profile_req <day> <valve_name> [valve_name ...]
#	Requires and prints the daily schedule for one or more valves
#	<day> is calculated as: 01 = monday .. 07 = sunday


. ./valve_commands.sh

if [ -z $2 ] ; then
	printf "Usage: ./profile_req <day> <valve_name> [valve_name ...]\n"
	exit
fi

if [ $1 -lt 1 -o $1 -gt 7 ] ; then
  printf "Week goes from 01 (monday) to 07 (sunday)\n."
	exit
fi

day=$(( ($1 + 1) % 7 ))
for index in $(seq 2 $#); do
	name=${!index}
	address=$( search_by_name $name $VALVE_FILE )
	if [ "$address" != "-1" ]; then
  	printf "%s:\n" "$name"
		parse_return_value $(read_profile $address $day)
		printf "\n"
	else
		printf "%s valve not found\n" "$name"
	fi
done
