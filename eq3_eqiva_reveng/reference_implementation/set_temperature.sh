#!/bin/bash

#USAGE:
#	./set_temperature.sh <temperature> <valve_name> [valve_name ...]
#	changes the temperature of one or more valves.
#	valve's names are supplied as a command line argument.

. ./valve_commands.sh

if [ -z $2 ]; then
	printf "Usage: ./set_temperature.sh <temperature> <valve_name> [valve_name ...]\n"
	exit
fi

for index in $(seq 2 $#); do
	name=${!index}
	address=$( search_by_name $name $VALVE_FILE )
	if [ "$address" != "-1" ]; then
		set_temperature $address $1
	else
		printf "%s valve not found\n" "$name"
	fi
done
