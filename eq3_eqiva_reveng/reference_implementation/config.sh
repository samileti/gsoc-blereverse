#!/bin/bash

#	File containing valve names and addresses.
#   Each valve must be represented on a new line as
#   NAME/ADDRESS (e.g. bedroom/00:11:22:33:44:55)
VALVE_FILE="valves.txt"

# Timeout seconds waiting for a notification after sending a command.
# Used by the send_command() function in the basic_functions.sh file
# Default value: TIMEOUT_SEC=5
TIMEOUT_SEC=5
