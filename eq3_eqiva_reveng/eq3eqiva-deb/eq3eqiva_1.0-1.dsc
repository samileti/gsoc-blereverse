-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: eq3eqiva
Binary: eq3eqiva
Architecture: all
Version: 1.0-1
Maintainer: Sergio Alberti <parallelosec@gmail.com>
Homepage: https://gitlab.com/sergioalberti/gsoc-blereverse/tree/master/eq3_eqiva_reveng/eq3eqiva-deb
Standards-Version: 3.9.7
Build-Depends: debhelper (>= 9)
Package-List:
 eq3eqiva deb misc optional arch=all
Checksums-Sha1:
 52e8ca51c3c3ca6cbdbf729067b76797a5cba237 164 eq3eqiva_1.0.orig.tar.xz
 819c2aca272b22dfc15fb9ad2ebf0e6d1fda6931 16232 eq3eqiva_1.0-1.debian.tar.xz
Checksums-Sha256:
 6445d549a47a0376ea5f7d7ca5d5e058c926d9c72be083068e3ff64e4327e957 164 eq3eqiva_1.0.orig.tar.xz
 27e670858a57552bd42085399c06f8777a500b282efa76603fc6a63e95c09d6a 16232 eq3eqiva_1.0-1.debian.tar.xz
Files:
 d89457c857ad72bc7c77672a76ff8273 164 eq3eqiva_1.0.orig.tar.xz
 567750cadf765568e588ac63ae8501a2 16232 eq3eqiva_1.0-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAEBAgAGBQJbaWZLAAoJEL6IEuwNWruYoBoH/3dKfmlU5qrTDaMiYB8v+CKS
kClg/qvo+a/S7c1BzwUttkRHJth2Y+zCgDAXikhV/Fv5+cXVR0Lu9HiAWiAUE1WD
7khM7glBG81eZi50hrDuziDJO3XBg8F2US6RzKBNItzqZuqADAyc1yr4BwS3ILa7
h5HHVbdWUS3LXUc1e5teumBnYkKDYNIp283jRp95l9LVfg5ekAMj2XRqXi5jcVvI
expyMU0qw0Z62eI5Y5ywPtbleDDucVDdItJB6AH0OPcUfI9OyyesNuVKlxCfSpMK
kKkA49ScTtTuKhjS9XNG1v45OoUYP4Nek4k+/VHQjXk9tPj6BNnqPiwjHEL5HoE=
=wHZF
-----END PGP SIGNATURE-----
