Description: add status req script
 add a script to request the status of one or more valves
 .
 eq3eqiva (1.0-1) unstable; urgency=medium
 .
   * Initial release
Author: Sergio Alberti <parallelosec@gmail.com>

---
The information above should follow the Patch Tagging Guidelines, please
checkout http://dep.debian.net/deps/dep3/ to learn about the format. Here
are templates for supplementary fields that you might want to add:

Origin: <vendor|upstream|other>, <url of original patch>
Bug: <url in upstream bugtracker>
Bug-Debian: https://bugs.debian.org/<bugnumber>
Bug-Ubuntu: https://launchpad.net/bugs/<bugnumber>
Forwarded: <no|not-needed|url proving that it has been forwarded>
Reviewed-By: <name and email of someone who approved the patch>
Last-Update: <YYYY-MM-DD>

--- /dev/null
+++ eq3eqiva-1.0/eq3-status
@@ -0,0 +1,37 @@
+#!/bin/bash
+
+#USAGE:
+#	eq3-status valve_name [valve_name ...]
+#	provides information on the status of one or more valves
+#	valve's names are supplied as a command line argument.
+
+. /usr/share/eq3eqiva/valve_commands
+
+if [ -z $1 ]; then
+	printf "Usage: eq3-status [-p|--parse] valve_name [valve_name ...]\n"
+	exit
+fi
+
+#check if bluetooth is active
+check_bt_status
+
+for name in "$@"; do
+	address=$( search_by_name $name $VALVE_FILE )
+
+	#check if the address was found and is valid
+	if [[ "$address" != "-1" && $(validate_mac $address) != "-1" ]]; then
+
+		#send the command
+		notification=$(send_init $address)
+
+		#check if notification is NOT an empty string
+		if [[ ! -z $notification ]]; then
+			printf "\n%s:\n" "$name"
+			parse_return_value $notification
+		else
+			printf "\n%s: error. try to increase the timeout or move close to the valve\n" "$name"
+		fi
+	else
+		printf "\n%s: not found in '%s' or invalid MAC address\n" "$name" "$VALVE_FILE"
+	fi
+done
--- eq3eqiva-1.0.orig/eq3eqiva
+++ eq3eqiva-1.0/eq3eqiva
@@ -4,6 +4,7 @@ echo
 echo "eq3eqiva package. Command-line EQ3 Eqiva radiator valves manager."
 echo
 echo "The eq3eqiva package includes the following commands:"
+echo "  eq3-status - provides information on the status of one or more valves"
 echo "  eq3-auto - puts one or more valves in automatic mode"
 echo "  eq3-manual - puts one or more valves in manual mode"
 echo "  eq3-temperature - sets a given temperature on one or more valves"
