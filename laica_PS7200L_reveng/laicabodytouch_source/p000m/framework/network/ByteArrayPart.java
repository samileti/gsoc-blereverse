package p000m.framework.network;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import p000m.framework.utils.Data;

public class ByteArrayPart extends HTTPPart {
    private byte[] buffer;

    public ByteArrayPart append(byte[] array) throws Throwable {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (this.buffer != null && this.buffer.length > 0) {
            baos.write(this.buffer);
        }
        baos.write(array);
        baos.flush();
        this.buffer = baos.toByteArray();
        baos.close();
        return this;
    }

    protected InputStream getInputStream() throws Throwable {
        if (this.buffer == null || this.buffer.length <= 0) {
            return new ByteArrayInputStream(new byte[0]);
        }
        return new ByteArrayInputStream(this.buffer);
    }

    public String toString() {
        return Data.byteToHex(this.buffer);
    }

    protected long length() throws Throwable {
        return (long) (this.buffer == null ? 0 : this.buffer.length);
    }
}
