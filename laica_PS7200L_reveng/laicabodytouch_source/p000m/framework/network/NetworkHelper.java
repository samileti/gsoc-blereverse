package p000m.framework.network;

import com.vc.pulltorefresh.PullToRefreshBase;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;
import org.androidannotations.api.rest.MediaType;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

public class NetworkHelper {
    public static int connectionTimeout;
    public static int readTimout;

    public String httpGet(String url, ArrayList<KVPair<String>> values, ArrayList<KVPair<String>> headers) throws Throwable {
        HttpClient client;
        if (values != null) {
            String param = kvPairsToUrl(values);
            if (param.length() > 0) {
                url = new StringBuilder(String.valueOf(url)).append("?").append(param).toString();
            }
        }
        HttpGet get = new HttpGet(url);
        if (headers != null) {
            Iterator it = headers.iterator();
            while (it.hasNext()) {
                KVPair<String> header = (KVPair) it.next();
                get.setHeader(header.name, (String) header.value);
            }
        }
        BasicHttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, connectionTimeout);
        HttpConnectionParams.setSoTimeout(httpParams, readTimout);
        get.setParams(httpParams);
        if (url.startsWith("https://")) {
            client = getSSLHttpClient();
        } else {
            client = new DefaultHttpClient();
        }
        HttpResponse response = client.execute(get);
        if (response.getStatusLine().getStatusCode() == PullToRefreshBase.SMOOTH_SCROLL_DURATION_MS) {
            String resp = EntityUtils.toString(response.getEntity(), "utf-8");
            client.getConnectionManager().shutdown();
            return resp;
        }
        String error = EntityUtils.toString(response.getEntity(), "utf-8");
        client.getConnectionManager().shutdown();
        throw new Throwable(error);
    }

    public void httpGet(String url, ArrayList<KVPair<String>> values, ArrayList<KVPair<String>> headers, ResponseCallback callback) throws Throwable {
        HttpClient client;
        if (values != null) {
            String param = kvPairsToUrl(values);
            if (param.length() > 0) {
                url = new StringBuilder(String.valueOf(url)).append("?").append(param).toString();
            }
        }
        HttpGet get = new HttpGet(url);
        if (headers != null) {
            Iterator it = headers.iterator();
            while (it.hasNext()) {
                KVPair<String> header = (KVPair) it.next();
                get.setHeader(header.name, (String) header.value);
            }
        }
        BasicHttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, connectionTimeout);
        HttpConnectionParams.setSoTimeout(httpParams, readTimout);
        get.setParams(httpParams);
        if (url.startsWith("https://")) {
            client = getSSLHttpClient();
        } else {
            client = new DefaultHttpClient();
        }
        HttpResponse response = client.execute(get);
        if (response.getStatusLine().getStatusCode() == PullToRefreshBase.SMOOTH_SCROLL_DURATION_MS) {
            InputStream is = response.getEntity().getContent();
            if (callback != null) {
                callback.onResponse(is);
            }
            is.close();
            client.getConnectionManager().shutdown();
            return;
        }
        String error = EntityUtils.toString(response.getEntity(), "utf-8");
        client.getConnectionManager().shutdown();
        throw new Throwable(error);
    }

    public void download(String url, File file) throws Throwable {
        HttpClient client;
        HttpGet get = new HttpGet(url);
        if (url.startsWith("https://")) {
            client = getSSLHttpClient();
        } else {
            client = new DefaultHttpClient();
        }
        HttpResponse response = client.execute(get);
        if (response.getStatusLine().getStatusCode() == PullToRefreshBase.SMOOTH_SCROLL_DURATION_MS) {
            InputStream is = response.getEntity().getContent();
            FileOutputStream fos = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            for (int len = is.read(buf); len > 0; len = is.read(buf)) {
                fos.write(buf, 0, len);
            }
            fos.flush();
            is.close();
            fos.close();
            client.getConnectionManager().shutdown();
            return;
        }
        String error = EntityUtils.toString(response.getEntity(), "utf-8");
        client.getConnectionManager().shutdown();
        throw new Throwable(error);
    }

    public void download(String url, ResponseCallback callback) throws Throwable {
        HttpClient client;
        HttpGet get = new HttpGet(url);
        if (url.startsWith("https://")) {
            client = getSSLHttpClient();
        } else {
            client = new DefaultHttpClient();
        }
        HttpResponse response = client.execute(get);
        if (response.getStatusLine().getStatusCode() == PullToRefreshBase.SMOOTH_SCROLL_DURATION_MS) {
            InputStream is = response.getEntity().getContent();
            if (callback != null) {
                callback.onResponse(is);
            }
            is.close();
            client.getConnectionManager().shutdown();
            return;
        }
        String error = EntityUtils.toString(response.getEntity(), "utf-8");
        client.getConnectionManager().shutdown();
        throw new Throwable(error);
    }

    public String httpPost(String url, ArrayList<KVPair<String>> values, KVPair<String> file, ArrayList<KVPair<String>> headers) throws Throwable {
        HttpPost post;
        HttpClient client;
        if (file == null || file.value == null || !new File((String) file.value).exists()) {
            post = textPost(url, values);
        } else {
            post = filePost(url, values, file);
        }
        if (headers != null) {
            Iterator it = headers.iterator();
            while (it.hasNext()) {
                KVPair<String> header = (KVPair) it.next();
                post.setHeader(header.name, (String) header.value);
            }
        }
        BasicHttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, connectionTimeout);
        HttpConnectionParams.setSoTimeout(httpParams, readTimout);
        post.setParams(httpParams);
        if (url.startsWith("https://")) {
            client = getSSLHttpClient();
        } else {
            client = new DefaultHttpClient();
        }
        HttpResponse httpResponse = client.execute(post);
        int status = httpResponse.getStatusLine().getStatusCode();
        if (status == PullToRefreshBase.SMOOTH_SCROLL_DURATION_MS || status == 201) {
            String resp = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
            client.getConnectionManager().shutdown();
            return resp;
        }
        String error = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
        client.getConnectionManager().shutdown();
        throw new Throwable(error);
    }

    public void httpPost(String url, ArrayList<KVPair<String>> values, KVPair<String> file, ArrayList<KVPair<String>> headers, ResponseCallback callback) throws Throwable {
        HttpPost post;
        HttpClient client;
        if (file == null || file.value == null || !new File((String) file.value).exists()) {
            post = textPost(url, values);
        } else {
            post = filePost(url, values, file);
        }
        if (headers != null) {
            Iterator it = headers.iterator();
            while (it.hasNext()) {
                KVPair<String> header = (KVPair) it.next();
                post.setHeader(header.name, (String) header.value);
            }
        }
        BasicHttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, connectionTimeout);
        HttpConnectionParams.setSoTimeout(httpParams, readTimout);
        post.setParams(httpParams);
        if (url.startsWith("https://")) {
            client = getSSLHttpClient();
        } else {
            client = new DefaultHttpClient();
        }
        HttpResponse httpResponse = client.execute(post);
        int status = httpResponse.getStatusLine().getStatusCode();
        if (status == PullToRefreshBase.SMOOTH_SCROLL_DURATION_MS || status == 201) {
            InputStream is = httpResponse.getEntity().getContent();
            if (callback != null) {
                callback.onResponse(is);
            }
            is.close();
            client.getConnectionManager().shutdown();
            return;
        }
        String error = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
        client.getConnectionManager().shutdown();
        throw new Throwable(error);
    }

    private HttpPost filePost(String url, ArrayList<KVPair<String>> values, KVPair<String> file) throws Throwable {
        HttpPost post = new HttpPost(url);
        String boundary = UUID.randomUUID().toString();
        MultiPart mp = new MultiPart();
        StringPart sp = new StringPart();
        if (values != null) {
            Iterator it = values.iterator();
            while (it.hasNext()) {
                KVPair<String> value = (KVPair) it.next();
                sp.append("--").append(boundary).append("\r\n");
                sp.append("content-disposition: form-data; name=\"").append(value.name).append("\"\r\n\r\n");
                sp.append((String) value.value).append("\r\n");
            }
        }
        post.setHeader("Content-Type", "multipart/form-data; boundary=" + boundary);
        sp.append("--").append(boundary).append("\r\n");
        sp.append("Content-Disposition: form-data; name=\"").append(file.name).append("\"; filename=\"").append(new File((String) file.value).getName()).append("\"\r\n");
        String mime = URLConnection.getFileNameMap().getContentTypeFor((String) file.value);
        if (mime == null || mime.length() <= 0) {
            if (((String) file.value).toLowerCase().endsWith("jpg") || ((String) file.value).toLowerCase().endsWith("jepg")) {
                mime = MediaType.IMAGE_JPEG;
            } else if (((String) file.value).toLowerCase().endsWith("png")) {
                mime = MediaType.IMAGE_PNG;
            } else if (((String) file.value).toLowerCase().endsWith("gif")) {
                mime = MediaType.IMAGE_GIF;
            } else {
                FileInputStream fis = new FileInputStream((String) file.value);
                mime = URLConnection.guessContentTypeFromStream(fis);
                fis.close();
                if (mime == null || mime.length() <= 0) {
                    mime = MediaType.APPLICATION_OCTET_STREAM;
                }
            }
        }
        sp.append("Content-Type: ").append(mime).append("\r\n\r\n");
        mp.append(sp);
        FilePart fp = new FilePart();
        fp.setFile((String) file.value);
        mp.append(fp);
        sp = new StringPart();
        sp.append("\r\n--").append(boundary).append("--\r\n");
        mp.append(sp);
        post.setEntity(mp.getInputStreamEntity());
        return post;
    }

    private HttpPost textPost(String url, ArrayList<KVPair<String>> values) throws Throwable {
        HttpPost post = new HttpPost(url);
        if (values != null) {
            StringPart sp = new StringPart();
            sp.append(kvPairsToUrl(values));
            InputStreamEntity entity = sp.getInputStreamEntity();
            entity.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            post.setEntity(entity);
        }
        return post;
    }

    public void rawPost(String url, ArrayList<KVPair<String>> headers, HTTPPart data, ResponseCallback callback) throws Throwable {
        HttpClient client;
        HttpPost post = new HttpPost(url);
        if (headers != null) {
            Iterator it = headers.iterator();
            while (it.hasNext()) {
                KVPair<String> header = (KVPair) it.next();
                post.setHeader(header.name, (String) header.value);
            }
        }
        post.setEntity(data.getInputStreamEntity());
        if (url.startsWith("https://")) {
            client = getSSLHttpClient();
        } else {
            client = new DefaultHttpClient();
        }
        HttpResponse httpResponse = client.execute(post);
        if (httpResponse.getStatusLine().getStatusCode() == PullToRefreshBase.SMOOTH_SCROLL_DURATION_MS) {
            InputStream is = httpResponse.getEntity().getContent();
            if (callback != null) {
                callback.onResponse(is);
            }
            is.close();
            client.getConnectionManager().shutdown();
        }
        String error = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
        client.getConnectionManager().shutdown();
        throw new Throwable(error);
    }

    public String httpPut(String url, ArrayList<KVPair<String>> values, KVPair<String> file, ArrayList<KVPair<String>> headers) throws Throwable {
        HttpClient client;
        if (values != null) {
            String param = kvPairsToUrl(values);
            if (param.length() > 0) {
                url = new StringBuilder(String.valueOf(url)).append("?").append(param).toString();
            }
        }
        HttpPut put = new HttpPut(url);
        if (headers != null) {
            Iterator it = headers.iterator();
            while (it.hasNext()) {
                KVPair<String> header = (KVPair) it.next();
                put.setHeader(header.name, (String) header.value);
            }
        }
        FilePart fp = new FilePart();
        fp.setFile((String) file.value);
        InputStreamEntity entity = fp.getInputStreamEntity();
        entity.setContentEncoding(MediaType.APPLICATION_OCTET_STREAM);
        put.setEntity(entity);
        BasicHttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, connectionTimeout);
        HttpConnectionParams.setSoTimeout(httpParams, readTimout);
        put.setParams(httpParams);
        if (url.startsWith("https://")) {
            client = getSSLHttpClient();
        } else {
            client = new DefaultHttpClient();
        }
        HttpResponse httpResponse = client.execute(put);
        int status = httpResponse.getStatusLine().getStatusCode();
        if (status == PullToRefreshBase.SMOOTH_SCROLL_DURATION_MS || status == 201) {
            String resp = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
            client.getConnectionManager().shutdown();
            return resp;
        }
        String error = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
        client.getConnectionManager().shutdown();
        throw new Throwable(error);
    }

    private String kvPairsToUrl(ArrayList<KVPair<String>> values) throws Throwable {
        StringBuilder sb = new StringBuilder();
        Iterator it = values.iterator();
        while (it.hasNext()) {
            KVPair<String> value = (KVPair) it.next();
            String encodedName = URLEncoder.encode(value.name, "utf-8");
            String encodedValue = value.value != null ? URLEncoder.encode((String) value.value, "utf-8") : "";
            if (sb.length() > 0) {
                sb.append('&');
            }
            sb.append(encodedName).append('=').append(encodedValue);
        }
        return sb.toString();
    }

    private HttpClient getSSLHttpClient() throws Throwable {
        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        trustStore.load(null, null);
        SSLSocketFactory sf = new SSLSocketFactoryEx(trustStore);
        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        HttpParams params = new BasicHttpParams();
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, "UTF-8");
        SchemeRegistry registry = new SchemeRegistry();
        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        registry.register(new Scheme("https", sf, 443));
        return new DefaultHttpClient(new ThreadSafeClientConnManager(params, registry), params);
    }
}
