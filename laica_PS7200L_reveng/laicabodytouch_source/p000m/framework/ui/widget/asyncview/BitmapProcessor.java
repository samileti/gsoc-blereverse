package p000m.framework.ui.widget.asyncview;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.WeakHashMap;
import p000m.framework.network.NetworkHelper;
import p000m.framework.network.ResponseCallback;
import p000m.framework.utils.Data;
import p000m.framework.utils.Utils;

public class BitmapProcessor {
    private static final int CAPACITY = 5;
    private static final int MAX_REQ_TIME = 200;
    private static final int MAX_SIZE = 40;
    private static final int OVERFLOW_SIZE = 50;
    private static BitmapProcessor instance;
    private File cacheDir;
    private WeakHashMap<String, Bitmap> cacheMap = new WeakHashMap();
    private Vector<ImageReq> netReqTPS = new Vector();
    private Vector<ImageReq> reqList = new Vector();
    private boolean work;
    private WorkerThread[] workerList = new WorkerThread[5];

    public static class ImageReq {
        private BitmapCallback callback;
        private Bitmap image;
        private long reqTime = System.currentTimeMillis();
        private String url;
        private WorkerThread worker;

        private void throwComplete(Bitmap bitmap) {
            this.image = bitmap;
            if (this.callback != null) {
                this.callback.onImageGot(this.url, this.image);
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("url=").append(this.url);
            sb.append("time=").append(this.reqTime);
            sb.append("worker=").append(this.worker.getName()).append(" (").append(this.worker.getId()).append("");
            return sb.toString();
        }
    }

    private static class ManagerThread extends Timer {
        private BitmapProcessor processor;

        class C01861 extends TimerTask {
            private int counter;

            C01861() {
            }

            public void run() {
                if (ManagerThread.this.processor.work) {
                    this.counter--;
                    if (this.counter <= 0) {
                        this.counter = 100;
                        ManagerThread.this.scan();
                    }
                }
            }
        }

        public ManagerThread(BitmapProcessor bp) {
            this.processor = bp;
            schedule(new C01861(), 0, 200);
        }

        private void scan() {
            if (this.processor.work) {
                long curTime = System.currentTimeMillis();
                int i = 0;
                while (i < this.processor.workerList.length) {
                    if (this.processor.workerList[i] == null) {
                        this.processor.workerList[i] = new WorkerThread(this.processor);
                        this.processor.workerList[i].setName("worker " + i);
                        this.processor.workerList[i].localType = i == 0;
                        this.processor.workerList[i].start();
                    } else if (curTime - this.processor.workerList[i].lastReport > 20000) {
                        this.processor.workerList[i].interrupt();
                        boolean localType = this.processor.workerList[i].localType;
                        this.processor.workerList[i] = new WorkerThread(this.processor);
                        this.processor.workerList[i].setName("worker " + i);
                        this.processor.workerList[i].localType = localType;
                        this.processor.workerList[i].start();
                    }
                    i++;
                }
            }
        }
    }

    private static class PatchInputStream extends FilterInputStream {
        InputStream in;

        protected PatchInputStream(InputStream in) {
            super(in);
            this.in = in;
        }

        public long skip(long n) throws IOException {
            long m = 0;
            while (m < n) {
                long _m = this.in.skip(n - m);
                if (_m == 0) {
                    break;
                }
                m += _m;
            }
            return m;
        }
    }

    private static class WorkerThread extends Thread {
        private ImageReq curReq;
        private long lastReport = System.currentTimeMillis();
        private boolean localType;
        private BitmapProcessor processor;

        public WorkerThread(BitmapProcessor bp) {
            this.processor = bp;
        }

        public void run() {
            while (this.processor.work) {
                try {
                    if (this.localType) {
                        doLocalTask();
                    } else {
                        doNetworkTask();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }

        private void doLocalTask() throws Throwable {
            ImageReq req = null;
            int size = this.processor.reqList.size();
            if (size > 0) {
                req = (ImageReq) this.processor.reqList.remove(size - 1);
            }
            if (req != null) {
                Bitmap bm = (Bitmap) this.processor.cacheMap.get(req.url);
                if (bm != null) {
                    this.curReq = req;
                    this.curReq.worker = this;
                    req.throwComplete(bm);
                } else if (new File(this.processor.cacheDir, Data.MD5(req.url)).exists()) {
                    doTask(req);
                    this.lastReport = System.currentTimeMillis();
                    return;
                } else {
                    if (this.processor.netReqTPS.size() > BitmapProcessor.MAX_SIZE) {
                        while (this.processor.reqList.size() > 0) {
                            this.processor.reqList.remove(0);
                        }
                        this.processor.netReqTPS.remove(0);
                    }
                    this.processor.netReqTPS.add(req);
                }
                this.lastReport = System.currentTimeMillis();
                return;
            }
            this.lastReport = System.currentTimeMillis();
            Thread.sleep(30);
        }

        private void doNetworkTask() throws Throwable {
            ImageReq imageReq = null;
            if (this.processor.netReqTPS.size() > 0) {
                imageReq = (ImageReq) this.processor.netReqTPS.remove(0);
            }
            if (imageReq == null) {
                int size = this.processor.reqList.size();
                if (size > 0) {
                    imageReq = (ImageReq) this.processor.reqList.remove(size - 1);
                }
            }
            if (imageReq != null) {
                Bitmap bm = (Bitmap) this.processor.cacheMap.get(imageReq.url);
                if (bm != null) {
                    this.curReq = imageReq;
                    this.curReq.worker = this;
                    imageReq.throwComplete(bm);
                } else {
                    doTask(imageReq);
                }
                this.lastReport = System.currentTimeMillis();
                return;
            }
            this.lastReport = System.currentTimeMillis();
            Thread.sleep(30);
        }

        private void doTask(final ImageReq req) throws Throwable {
            this.curReq = req;
            this.curReq.worker = this;
            Bitmap bm = null;
            final File file = new File(this.processor.cacheDir, Data.MD5(req.url));
            if (file.exists()) {
                bm = Utils.getBitmap(file.getAbsolutePath());
                if (bm != null) {
                    this.processor.cacheMap.put(req.url, bm);
                    req.throwComplete(bm);
                }
                this.curReq = null;
            } else {
                new NetworkHelper().download(req.url, new ResponseCallback() {
                    public void onResponse(InputStream is) {
                        Bitmap bitmap = Utils.getBitmap(new PatchInputStream(is));
                        if (bitmap == null || bitmap.isRecycled()) {
                            WorkerThread.this.curReq = null;
                            return;
                        }
                        WorkerThread.this.saveFile(bitmap, file);
                        if (bitmap != null) {
                            WorkerThread.this.processor.cacheMap.put(req.url, bitmap);
                            req.throwComplete(bitmap);
                        }
                        WorkerThread.this.curReq = null;
                    }
                });
            }
            if (bm != null) {
                this.processor.cacheMap.put(req.url, bm);
                req.throwComplete(bm);
            }
            this.curReq = null;
        }

        private void saveFile(Bitmap bitmap, File file) {
            try {
                if (file.exists()) {
                    file.delete();
                }
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }
                file.createNewFile();
                CompressFormat type = CompressFormat.JPEG;
                String mime = Utils.getFileMime(file.getAbsolutePath());
                if (mime != null && (mime.endsWith("png") || mime.endsWith("gif"))) {
                    type = CompressFormat.PNG;
                }
                FileOutputStream fos = new FileOutputStream(file);
                bitmap.compress(type, 100, fos);
                fos.flush();
                fos.close();
            } catch (Throwable th) {
                if (file.exists()) {
                    file.delete();
                }
            }
        }

        public void interrupt() {
            try {
                super.interrupt();
            } catch (Throwable th) {
            }
        }
    }

    public static synchronized void prepare(String cacheDir) {
        synchronized (BitmapProcessor.class) {
            if (instance == null) {
                instance = new BitmapProcessor(cacheDir);
            }
        }
    }

    public static void start() {
        if (instance == null) {
            throw new RuntimeException("Call BitmapProcessor.prepare(String) before start");
        }
        instance.work = true;
    }

    public static void stop() {
        if (instance != null) {
            instance.work = false;
            instance.reqList.clear();
            for (int i = 0; i < instance.workerList.length; i++) {
                if (instance.workerList[i] != null) {
                    instance.workerList[i].interrupt();
                }
            }
            instance = null;
        }
    }

    public static void process(String url, BitmapCallback callback) {
        if (instance == null) {
            throw new RuntimeException("Call BitmapProcessor.prepare(String) before start");
        } else if (url != null) {
            ImageReq req = new ImageReq();
            req.url = url;
            req.callback = callback;
            instance.reqList.add(req);
            if (instance.reqList.size() > OVERFLOW_SIZE) {
                while (instance.reqList.size() > MAX_SIZE) {
                    instance.reqList.remove(0);
                }
            }
            BitmapProcessor.start();
        }
    }

    public static Bitmap getBitmapFromCache(String url) {
        if (instance != null) {
            return (Bitmap) instance.cacheMap.get(url);
        }
        throw new RuntimeException("Call BitmapProcessor.prepare(String) before start");
    }

    private BitmapProcessor(String cacheDir) {
        this.cacheDir = new File(cacheDir);
        if (!this.cacheDir.exists()) {
            this.cacheDir.mkdirs();
        }
        ManagerThread managerThread = new ManagerThread(this);
    }
}
