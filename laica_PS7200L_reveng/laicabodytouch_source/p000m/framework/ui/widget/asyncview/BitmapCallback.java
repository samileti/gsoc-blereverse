package p000m.framework.ui.widget.asyncview;

import android.graphics.Bitmap;

public interface BitmapCallback {
    void onImageGot(String str, Bitmap bitmap);
}
