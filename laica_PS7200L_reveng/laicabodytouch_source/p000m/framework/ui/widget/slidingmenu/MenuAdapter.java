package p000m.framework.ui.widget.slidingmenu;

import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class MenuAdapter {
    private ArrayList<SlidingMenuGroup> menus = new ArrayList();

    public abstract View getGroupView(int i, ViewGroup viewGroup);

    public abstract View getItemView(SlidingMenuItem slidingMenuItem, ViewGroup viewGroup);

    public MenuAdapter(SlidingMenu menu) {
    }

    private SlidingMenuGroup findGroupById(int id) {
        if (this.menus == null) {
            return null;
        }
        Iterator it = this.menus.iterator();
        while (it.hasNext()) {
            SlidingMenuGroup group = (SlidingMenuGroup) it.next();
            if (group != null && group.id == id) {
                return group;
            }
        }
        return null;
    }

    public void setGroup(int id, String text) {
        SlidingMenuGroup group = findGroupById(id);
        if (group == null) {
            group = new SlidingMenuGroup();
            group.id = id;
            this.menus.add(group);
        }
        group.text = text;
    }

    void setGroup(SlidingMenuGroup group) {
        if (group != null) {
            SlidingMenuGroup groupTmp = findGroupById(group.id);
            if (groupTmp == null) {
                this.menus.add(group);
                return;
            }
            int index = this.menus.indexOf(groupTmp);
            this.menus.remove(index);
            this.menus.add(index, group);
        }
    }

    public void setItem(int groupId, SlidingMenuItem item) {
        if (item != null) {
            SlidingMenuGroup group = findGroupById(groupId);
            if (group != null) {
                group.setItem(item);
            }
        }
    }

    public View getMenuTitle() {
        return null;
    }

    int getGroupCount() {
        return this.menus == null ? 0 : this.menus.size();
    }

    SlidingMenuGroup getGroup(int position) {
        return (SlidingMenuGroup) this.menus.get(position);
    }

    protected String getTitle(int position) {
        return ((SlidingMenuGroup) this.menus.get(position)).text;
    }

    protected SlidingMenuItem getItem(int groupPosition, int position) {
        return ((SlidingMenuGroup) this.menus.get(groupPosition)).getItem(position);
    }

    public void notifyDataSetChanged(SlidingMenuItem item) {
    }

    public boolean onItemTrigger(SlidingMenuItem item) {
        return false;
    }

    public void onMenuSwitch(boolean menuShown) {
    }

    public SlidingMenuItem getMenuItem(int groupId, int itemId) {
        SlidingMenuGroup group = findGroupById(groupId);
        if (group == null) {
            return null;
        }
        return group.findItemById(itemId);
    }
}
