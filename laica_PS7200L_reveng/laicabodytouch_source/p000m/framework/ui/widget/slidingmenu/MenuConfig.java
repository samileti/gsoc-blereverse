package p000m.framework.ui.widget.slidingmenu;

public class MenuConfig {
    int bodyBackground;
    int itemDownBack;
    int itemDownRelease;
    int menuBackground;
    int menuSep;
    float menuWeight = 0.8f;
    int paddingBottom;
    int paddingLeft;
    int paddingRight;
    int paddingTop;
    int rightShadow;
    int titleHeight;
}
