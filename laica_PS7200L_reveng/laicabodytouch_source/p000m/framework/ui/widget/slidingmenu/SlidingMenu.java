package p000m.framework.ui.widget.slidingmenu;

import android.content.Context;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import java.lang.reflect.Method;
import java.util.HashMap;

public class SlidingMenu extends RelativeLayout {
    private MenuAdapter adapter;
    private MenuConfig config;
    private View curBody;
    private FrameLayout flMenu;
    private HashMap<SlidingMenuItem, View> itemToView;
    private LinearLayout llBody;
    private LinearLayout llMenu;
    private boolean menuShown;
    private int menuWidth;
    private OnClickListener ocListener;
    private OnTouchListener otListener;
    private int screenWidth;
    private int showMenuWidth;
    private BodyContainer svBody;
    private View vCover;

    class C01931 implements OnClickListener {

        class C01921 implements Runnable {
            C01921() {
            }

            public void run() {
                SlidingMenu.this.hideMenu();
            }
        }

        C01931() {
        }

        public void onClick(View v) {
            SlidingMenuItem item = (SlidingMenuItem) v.getTag();
            if (item != null && SlidingMenu.this.adapter != null && !SlidingMenu.this.adapter.onItemTrigger(item)) {
                SlidingMenu.this.postDelayed(new C01921(), 500);
            }
        }
    }

    class C01942 implements OnTouchListener {
        C01942() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case 0:
                    v.setBackgroundResource(SlidingMenu.this.config.itemDownBack);
                    break;
                case 1:
                case 3:
                    v.setBackgroundResource(SlidingMenu.this.config.itemDownRelease);
                    break;
            }
            return false;
        }
    }

    class C01963 implements OnGlobalLayoutListener {

        class C01951 implements Runnable {
            C01951() {
            }

            public void run() {
                SlidingMenu.this.hideMenu();
            }
        }

        C01963() {
        }

        public void onGlobalLayout() {
            SlidingMenu.this.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            SlidingMenu.this.post(new C01951());
        }
    }

    class C02006 implements OnGlobalLayoutListener {

        class C01991 implements Runnable {
            C01991() {
            }

            public void run() {
                SlidingMenu.this.svBody.scrollTo(SlidingMenu.this.menuWidth, 0);
            }
        }

        C02006() {
        }

        public void onGlobalLayout() {
            SlidingMenu.this.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            SlidingMenu.this.post(new C01991());
        }
    }

    public SlidingMenu(Context context) {
        super(context);
        init(context);
    }

    public SlidingMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SlidingMenu(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        this.config = new MenuConfig();
        this.itemToView = new HashMap();
        this.ocListener = new C01931();
        this.otListener = new C01942();
        this.screenWidth = context.getResources().getDisplayMetrics().widthPixels;
        this.menuWidth = (int) (((float) this.screenWidth) * this.config.menuWeight);
        this.showMenuWidth = (this.screenWidth - this.menuWidth) / 2;
        setBackgroundResource(this.config.menuBackground);
        initMenu(context);
        initBody(context);
        getViewTreeObserver().addOnGlobalLayoutListener(new C01963());
    }

    private void initMenu(Context context) {
        this.flMenu = new FrameLayout(context) {
            public boolean onInterceptTouchEvent(MotionEvent ev) {
                if (SlidingMenu.this.menuShown) {
                    return super.onInterceptTouchEvent(ev);
                }
                return true;
            }
        };
        this.flMenu.setLayoutParams(new LayoutParams(this.menuWidth, -1));
        addView(this.flMenu);
        LinearLayout llMenuCtn = new LinearLayout(context);
        llMenuCtn.setOrientation(1);
        llMenuCtn.setLayoutParams(new LayoutParams(-1, -1));
        this.flMenu.addView(llMenuCtn);
        ScrollView svMenu = new ScrollView(context);
        svMenu.setVerticalScrollBarEnabled(false);
        svMenu.setVerticalFadingEdgeEnabled(false);
        disableOverScrollMode(svMenu);
        LinearLayout.LayoutParams lpSv = new LinearLayout.LayoutParams(-1, -1);
        lpSv.weight = 1.0f;
        svMenu.setLayoutParams(lpSv);
        llMenuCtn.addView(svMenu);
        this.llMenu = new LinearLayout(context);
        this.llMenu.setOrientation(1);
        this.llMenu.setLayoutParams(new LayoutParams(-1, -1));
        svMenu.addView(this.llMenu);
        this.vCover = new View(context);
        this.vCover.setBackgroundColor(0);
        this.vCover.setLayoutParams(new LayoutParams(-1, -1));
        this.flMenu.addView(this.vCover);
    }

    private void initBody(Context context) {
        this.svBody = new BodyContainer(this);
        this.svBody.setHorizontalScrollBarEnabled(false);
        this.svBody.setHorizontalFadingEdgeEnabled(false);
        disableOverScrollMode(this.svBody);
        this.svBody.setLayoutParams(new LayoutParams(this.screenWidth, -1));
        addView(this.svBody);
        LinearLayout rlBodyContainer = new LinearLayout(context);
        rlBodyContainer.setLayoutParams(new LayoutParams(this.screenWidth + this.menuWidth, -1));
        this.svBody.addView(rlBodyContainer);
        FrameLayout flPlh = new FrameLayout(getContext());
        flPlh.setLayoutParams(new LinearLayout.LayoutParams(this.menuWidth, -1));
        rlBodyContainer.addView(flPlh);
        ImageView ivShadow = new ImageView(context);
        ivShadow.setImageResource(this.config.rightShadow);
        ivShadow.setScaleType(ScaleType.FIT_XY);
        LayoutParams lpShadow = new LayoutParams(-2, -1);
        lpShadow.gravity = 5;
        ivShadow.setLayoutParams(lpShadow);
        flPlh.addView(ivShadow);
        this.llBody = new LinearLayout(context) {
            public boolean onTouchEvent(MotionEvent event) {
                return true;
            }
        };
        this.llBody.setBackgroundResource(this.config.bodyBackground);
        this.llBody.setLayoutParams(new LinearLayout.LayoutParams(this.screenWidth, -1));
        rlBodyContainer.addView(this.llBody);
    }

    public void setAdapter(MenuAdapter adapter) {
        this.adapter = adapter;
        refresh();
    }

    public void refresh() {
        if (this.adapter != null) {
            reInit(getContext());
            invalidateMenu();
        }
    }

    private void reInit(Context context) {
        this.screenWidth = context.getResources().getDisplayMetrics().widthPixels;
        this.menuWidth = (int) (((float) this.screenWidth) * this.config.menuWeight);
        this.showMenuWidth = (this.screenWidth - this.menuWidth) / 2;
        setBackgroundResource(this.config.menuBackground);
        reInitMenu(context);
        reInitBody(context);
    }

    private void reInitMenu(Context context) {
        ViewGroup.LayoutParams lpMenu = this.flMenu.getLayoutParams();
        lpMenu.width = this.menuWidth;
        this.flMenu.setLayoutParams(lpMenu);
        this.llMenu.setPadding(this.config.paddingLeft, this.config.paddingTop, this.config.paddingRight, this.config.paddingBottom);
        if (this.adapter != null) {
            View vTitle = this.adapter.getMenuTitle();
            if (vTitle != null) {
                ViewGroup.LayoutParams lp = vTitle.getLayoutParams();
                int height = -2;
                if (lp != null) {
                    height = lp.height;
                }
                vTitle.setLayoutParams(new LinearLayout.LayoutParams(-1, height));
                ((LinearLayout) this.flMenu.getChildAt(0)).addView(vTitle);
            }
        }
    }

    private void reInitBody(Context context) {
        ViewGroup.LayoutParams lpBody = this.svBody.getLayoutParams();
        lpBody.width = this.screenWidth;
        this.svBody.setLayoutParams(lpBody);
        LinearLayout rlBodyContainer = (LinearLayout) this.svBody.getChildAt(0);
        ViewGroup.LayoutParams lp = rlBodyContainer.getLayoutParams();
        lp.width = this.screenWidth + this.menuWidth;
        rlBodyContainer.setLayoutParams(lp);
        FrameLayout flPlh = (FrameLayout) rlBodyContainer.getChildAt(0);
        lp = flPlh.getLayoutParams();
        lp.width = this.menuWidth;
        flPlh.setLayoutParams(lp);
        lp = this.llBody.getLayoutParams();
        lp.width = this.screenWidth;
        this.llBody.setLayoutParams(lp);
        this.llBody.setBackgroundResource(this.config.bodyBackground);
        ((ImageView) flPlh.getChildAt(0)).setImageResource(this.config.rightShadow);
        if (!this.menuShown) {
            getViewTreeObserver().addOnGlobalLayoutListener(new C02006());
        }
    }

    public void setBodyView(View body) {
        this.curBody = body;
        this.llBody.removeAllViews();
        if (this.curBody != null) {
            this.curBody.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
            this.llBody.addView(this.curBody);
        }
    }

    public View getBodyView() {
        return this.curBody;
    }

    private void invalidateMenu() {
        Context context = getContext();
        this.llMenu.removeAllViews();
        int count = this.adapter.getGroupCount();
        for (int i = 0; i < count; i++) {
            this.llMenu.addView(this.adapter.getGroupView(i, this.llMenu));
            int groupCount = this.adapter.getGroup(i).getCount();
            for (int j = 0; j < groupCount; j++) {
                SlidingMenuItem data = this.adapter.getItem(i, j);
                View item = this.adapter.getItemView(data, this.llMenu);
                this.llMenu.addView(item);
                this.llMenu.addView(getSepView(context));
                this.itemToView.put(data, item);
                item.setTag(data);
                item.setOnClickListener(this.ocListener);
                item.setOnTouchListener(this.otListener);
            }
            int viewCount = this.llMenu.getChildCount();
            if (viewCount > 0) {
                this.llMenu.removeViewAt(viewCount - 1);
            }
        }
    }

    private View getSepView(Context context) {
        View vSep = new View(context);
        vSep.setBackgroundResource(this.config.menuSep);
        vSep.setLayoutParams(new LinearLayout.LayoutParams(-1, 2));
        return vSep;
    }

    public void switchMenu() {
        if (this.menuShown) {
            hideMenu();
        } else {
            showMenu();
        }
    }

    public void showMenu() {
        this.menuShown = true;
        this.svBody.smoothScrollTo(0, 0);
        if (this.adapter != null) {
            this.adapter.onMenuSwitch(this.menuShown);
        }
    }

    public void hideMenu() {
        this.menuShown = false;
        this.svBody.smoothScrollTo(this.menuWidth, 0);
        if (this.adapter != null) {
            this.adapter.onMenuSwitch(this.menuShown);
        }
    }

    public boolean isMenuShown() {
        return this.menuShown;
    }

    int getMenuWidth() {
        return this.menuWidth;
    }

    int getShowMenuWidth() {
        return this.showMenuWidth;
    }

    MenuConfig getMenuConfig() {
        return this.config;
    }

    View getMenuCover() {
        return this.vCover;
    }

    public void setMenuItemBackground(int down, int release) {
        this.config.itemDownBack = down;
        this.config.itemDownRelease = release;
    }

    public void setMenuWeight(float weight) {
        this.config.menuWeight = weight;
    }

    public void setMenuBackground(int resId) {
        this.config.menuBackground = resId;
    }

    public void setMenuPadding(int left, int top, int right, int bottom) {
        this.config.paddingLeft = left;
        this.config.paddingTop = top;
        this.config.paddingRight = right;
        this.config.paddingBottom = bottom;
    }

    public void setTtleHeight(int height) {
        this.config.titleHeight = height;
    }

    public void setShadowRes(int resId) {
        this.config.rightShadow = resId;
    }

    public void setBodyBackground(int resId) {
        this.config.bodyBackground = resId;
    }

    public void setMenuDivider(int resId) {
        this.config.menuSep = resId;
    }

    private void disableOverScrollMode(View view) {
        if (VERSION.SDK_INT >= 9) {
            try {
                Method m = View.class.getMethod("setOverScrollMode", new Class[]{Integer.TYPE});
                m.setAccessible(true);
                m.invoke(view, new Object[]{Integer.valueOf(2)});
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    }

    View itemToView(SlidingMenuItem item) {
        return (View) this.itemToView.get(item);
    }

    public void triggerItem(int groupId, int itemId) {
        if (this.adapter != null) {
            SlidingMenuItem item = this.adapter.getMenuItem(groupId, itemId);
            if (item != null) {
                this.adapter.onItemTrigger(item);
            }
        }
    }

    public void triggerItem(SlidingMenuItem item) {
        if (this.adapter != null && item != null) {
            this.adapter.onItemTrigger(item);
        }
    }
}
