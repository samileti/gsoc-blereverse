package p000m.framework.utils;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;

public class UIHandler {
    private static Handler handler;

    class C02021 implements Callback {
        C02021() {
        }

        public boolean handleMessage(Message msg) {
            UIHandler.handleMessage(msg);
            return false;
        }
    }

    public static void prepare() {
        if (handler == null) {
            handler = new Handler(new C02021());
        }
    }

    private static void handleMessage(Message msg) {
        Object[] objs = msg.obj;
        Message inner = objs[0];
        Callback callback = objs[1];
        if (callback != null) {
            callback.handleMessage(inner);
        }
    }

    private static Message getShellMessage(Message msg, Callback callback) {
        Message shell = new Message();
        shell.obj = new Object[]{msg, callback};
        return shell;
    }

    private static Message getShellMessage(int what, Callback callback) {
        Message msg = new Message();
        msg.what = what;
        return UIHandler.getShellMessage(msg, callback);
    }

    public static boolean sendMessage(Message msg, Callback callback) {
        return handler.sendMessage(UIHandler.getShellMessage(msg, callback));
    }

    public static boolean sendMessageDelayed(Message msg, long delayMillis, Callback callback) {
        return handler.sendMessageDelayed(UIHandler.getShellMessage(msg, callback), delayMillis);
    }

    public static boolean sendMessageAtTime(Message msg, long uptimeMillis, Callback callback) {
        return handler.sendMessageAtTime(UIHandler.getShellMessage(msg, callback), uptimeMillis);
    }

    public static boolean sendMessageAtFrontOfQueue(Message msg, Callback callback) {
        return handler.sendMessageAtFrontOfQueue(UIHandler.getShellMessage(msg, callback));
    }

    public static boolean sendEmptyMessage(int what, Callback callback) {
        return handler.sendMessage(UIHandler.getShellMessage(what, callback));
    }

    public static boolean sendEmptyMessageAtTime(int what, long uptimeMillis, Callback callback) {
        return handler.sendMessageAtTime(UIHandler.getShellMessage(what, callback), uptimeMillis);
    }

    public static boolean sendEmptyMessageDelayed(int what, long delayMillis, Callback callback) {
        return handler.sendMessageDelayed(UIHandler.getShellMessage(what, callback), delayMillis);
    }
}
