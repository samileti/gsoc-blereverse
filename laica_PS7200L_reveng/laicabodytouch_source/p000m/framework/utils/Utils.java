package p000m.framework.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.text.TextUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.Locale;
import org.androidannotations.api.rest.MediaType;

public class Utils {
    private static float density;

    public static int dipToPx(Context context, int dip) {
        if (density <= 0.0f) {
            density = context.getResources().getDisplayMetrics().density;
        }
        return (int) ((((float) dip) * density) + 0.5f);
    }

    public static boolean isNullOrEmpty(String str) {
        return str == null || str.trim().length() <= 0 || "null".equals(str.trim().toLowerCase(Locale.getDefault()));
    }

    public static Bitmap getBitmap(String path) throws Throwable {
        return Utils.getBitmap(path, 1);
    }

    public static Bitmap getBitmap(InputStream is) {
        return Utils.getBitmap(is, 1);
    }

    public static Bitmap getBitmap(String path, int inSampleSize) throws Throwable {
        return Utils.getBitmap(new File(path), inSampleSize);
    }

    public static Bitmap getBitmap(File file, int inSampleSize) throws Throwable {
        InputStream fis = new FileInputStream(file);
        Bitmap bm = Utils.getBitmap(fis, inSampleSize);
        fis.close();
        return bm;
    }

    private static Bitmap getBitmap(InputStream is, int inSampleSize) {
        Options opt = new Options();
        opt.inPreferredConfig = Config.RGB_565;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        opt.inSampleSize = inSampleSize;
        return BitmapFactory.decodeStream(is, null, opt);
    }

    public static String getFileMime(String path) {
        String mime = URLConnection.getFileNameMap().getContentTypeFor(path);
        if (mime != null && mime.length() > 0) {
            return mime;
        }
        String lowerCaseFile = path.toLowerCase(Locale.getDefault());
        if (lowerCaseFile.endsWith("jpg") || lowerCaseFile.endsWith("jepg")) {
            return MediaType.IMAGE_JPEG;
        }
        if (lowerCaseFile.endsWith("png")) {
            return MediaType.IMAGE_PNG;
        }
        if (lowerCaseFile.endsWith("gif")) {
            return MediaType.IMAGE_GIF;
        }
        try {
            FileInputStream fis = new FileInputStream(path);
            mime = URLConnection.guessContentTypeFromStream(fis);
            fis.close();
        } catch (Throwable t) {
            t.printStackTrace();
        }
        if (mime == null || mime.length() <= 0) {
            return MediaType.APPLICATION_OCTET_STREAM;
        }
        return mime;
    }

    public static String getCachePath(Context context, String category) {
        String appDir = new StringBuilder(String.valueOf(context.getFilesDir().getAbsolutePath())).append("/mFramework/cache/").toString();
        DeviceHelper helper = new DeviceHelper(context);
        if (helper.getSdcardState()) {
            appDir = helper.getSdcardPath() + "/mFramework/" + helper.getPackageName() + "/cache/";
        }
        if (!TextUtils.isEmpty(category)) {
            appDir = new StringBuilder(String.valueOf(appDir)).append(category).append("/").toString();
        }
        File file = new File(appDir);
        if (!file.exists()) {
            file.mkdir();
        }
        return appDir;
    }

    public static int getScreenWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }
}
