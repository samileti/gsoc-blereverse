package com.whb.loease.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vc.cloudbalance.common.BaseActivity;
import com.vc.cloudbalance.common.Common;
import com.vc.cloudbalance.common.Constants;
import com.vc.cloudbalance.common.LanguageHelper;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.sqlite.AppConfigDAL;
import com.vc.cloudbalance.sqlite.MemberDAL;
import com.vc.cloudbalance.widget.View_AboutUs;
import com.vc.cloudbalance.widget.View_AboutUs_;
import com.vc.cloudbalance.widget.View_Themes_;
import com.vc.dialog.ListViewWindow;
import com.vc.dialog.ListViewWindow.OnSelectListener;
import com.whb.loease.bodytouch.C0181R;
import java.util.List;
import java.util.Locale;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity
public class MoreSettingActivity extends BaseActivity {
    View_AboutUs aboutUsView;
    @ViewById
    LinearLayout llAbout;
    @ViewById
    LinearLayout llGrowth;
    @ViewById
    LinearLayout llLanguage;
    @ViewById
    LinearLayout llSettingView;
    @ViewById
    LinearLayout llTop;
    @ViewById
    LinearLayout llUnit;
    @ViewById
    LinearLayout llVisibleView;
    List<MemberMDL> members;
    private OnSelectListener onLanguageSelectListener = new C02781();
    private OnSelectListener onUnitSelectListener = new C02792();
    @ViewById
    RelativeLayout rlRoot;
    MemberMDL thisMember;
    @ViewById
    TextView tvLanguage;
    @ViewById
    TextView tvThemeType;
    @ViewById
    TextView tvTitle;
    @ViewById
    TextView tvUnit;
    @ViewById
    TextView tvVersion;

    class C02781 implements OnSelectListener {
        C02781() {
        }

        public void onSelect(DialogInterface dialog, int which) {
            MoreSettingActivity.this.tvLanguage.setText(MoreSettingActivity.this.getResources().getStringArray(C0181R.array.language_array)[which]);
            SharedPreferences sharedPreferences = MoreSettingActivity.this.getSharedPreferences("AppConfigDAL", 0);
            Editor editor = sharedPreferences.edit();
            int LanguageVal = sharedPreferences.getInt(Constants.SQL_KEY_LANGUAGE_STRING, 0);
            Log.d("tag", Integer.toString(LanguageVal) + " " + Integer.toString(which));
            System.out.print(Integer.toString(LanguageVal) + " " + Integer.toString(which));
            if (which != LanguageVal - 1) {
                Resources resources = MoreSettingActivity.this.getResources();
                Configuration config = resources.getConfiguration();
                DisplayMetrics dm = resources.getDisplayMetrics();
                if (which == 0 || which == 1) {
                    config.locale = new Locale("zh", Common.getLanguageShortCodes()[which]);
                } else {
                    config.locale = new Locale(Common.getLanguageShortCodes()[which]);
                }
                Locale.setDefault(config.locale);
                resources.updateConfiguration(config, dm);
                LanguageHelper.SetLanguage(new StringBuilder(String.valueOf(which + 1)).toString());
                editor.putInt(Constants.SQL_KEY_LANGUAGE_STRING, which + 1);
                editor.commit();
                new AppConfigDAL(MoreSettingActivity.this.mContext).insert(Constants.SQL_KEY_LANGUAGECHANGE_STRING, "1");
                MoreSettingActivity.this.finish();
                MoreSettingActivity.this.startActivity(MoreSettingActivity.this.getIntent());
            }
        }
    }

    class C02792 implements OnSelectListener {
        C02792() {
        }

        public void onSelect(DialogInterface dialog, int which) {
            MoreSettingActivity.this.tvUnit.setText(new String[]{"Kg", "lb"}[which]);
            new AppConfigDAL(MoreSettingActivity.this.mContext).insert(Constants.SQL_KEY_WEIGHT_UNIT_STRING, new StringBuilder(String.valueOf(which)).toString());
            if (which == 0) {
                MoreSettingActivity.this.tvUnit.setText("Kg");
            } else if (which == 1) {
                MoreSettingActivity.this.tvUnit.setText("lb");
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initThemeView();
    }

    private void initThemeView() {
        switch (Common.getThemeType()) {
            case 1:
                setContentView(C0181R.layout.activity_moresetting);
                return;
            case 2:
                setContentView(C0181R.layout.activity_moresetting_greentheme);
                return;
            case 4:
                setContentView(C0181R.layout.activity_moresetting_greytheme);
                return;
            default:
                setContentView(C0181R.layout.activity_moresetting_bluetheme);
                return;
        }
    }

    protected void onResume() {
        System.gc();
        super.onResume();
    }

    protected void onStart() {
        super.onStart();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (this.aboutUsView == null) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.aboutUsView.getVisibility() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        this.aboutUsView.setVisibility(8);
        this.llSettingView.setVisibility(0);
        this.tvTitle.setText(getString(C0181R.string.more));
        return false;
    }

    public void goBack() {
        if (this.aboutUsView == null) {
            super.goBack();
        } else if (this.aboutUsView.getVisibility() == 0) {
            this.llSettingView.setVisibility(0);
            this.aboutUsView.setVisibility(8);
            this.tvTitle.setText(getString(C0181R.string.more));
        } else {
            super.goBack();
        }
    }

    @Click({2131492994})
    void selectLanguage() {
        new ListViewWindow(this.mContext, this.onLanguageSelectListener, getResources().getStringArray(C0181R.array.language_array), getString(C0181R.string.language), getString(C0181R.string.cancle)).show();
    }

    @Click({2131493000})
    void selectUnit() {
        new ListViewWindow(this.mContext, this.onUnitSelectListener, new String[]{"Kg", "lb"}, getString(C0181R.string.wt_unit), getString(C0181R.string.cancle)).show();
    }

    @Click({2131493002})
    void showAboutus() {
        this.llSettingView.setVisibility(8);
        this.tvTitle.setText(getString(C0181R.string.about));
        if (this.aboutUsView == null) {
            this.aboutUsView = View_AboutUs_.build(this);
            this.llVisibleView.addView(this.aboutUsView);
        }
        if (this.aboutUsView.getVisibility() != 0) {
            this.aboutUsView.setVisibility(0);
        }
    }

    @Click({2131492998})
    void showTheme() {
        PopupWindow mPopupWindow = new PopupWindow(-1, -1);
        View view = View_Themes_.build((Context) this, mPopupWindow);
        mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
        mPopupWindow.setFocusable(true);
        mPopupWindow.setContentView(view);
        mPopupWindow.showAsDropDown(this.llTop);
    }

    @AfterViews
    void init() {
        this.tvThemeType.setText("");
        this.members = new MemberDAL(this).SelectNotAdult();
        String val = "";
        int LanguageVal = getSharedPreferences("AppConfigDAL", 0).getInt(Constants.SQL_KEY_LANGUAGE_STRING, 0);
        String[] arrayList = getResources().getStringArray(C0181R.array.language_array);
        if (LanguageVal != 0) {
            this.tvLanguage.setText(arrayList[LanguageVal - 1]);
        }
        if (new AppConfigDAL(this.mContext).select(Constants.SQL_KEY_WEIGHT_UNIT_STRING).equals("1")) {
            this.tvUnit.setText("lb");
        } else {
            this.tvUnit.setText("Kg");
        }
        try {
            this.tvVersion.setText("LAICA Bodytouch " + this.mContext.getPackageManager().getPackageInfo(this.mContext.getPackageName(), 0).versionName);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
