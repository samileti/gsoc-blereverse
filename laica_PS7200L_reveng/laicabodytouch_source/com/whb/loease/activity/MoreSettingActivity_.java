package com.whb.loease.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.api.builder.ActivityIntentBuilder;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class MoreSettingActivity_ extends MoreSettingActivity implements HasViews, OnViewChangedListener {
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    class C01721 implements OnClickListener {
        C01721() {
        }

        public void onClick(View view) {
            MoreSettingActivity_.this.goBack();
        }
    }

    class C01732 implements OnClickListener {
        C01732() {
        }

        public void onClick(View view) {
            MoreSettingActivity_.this.showAboutus();
        }
    }

    class C01743 implements OnClickListener {
        C01743() {
        }

        public void onClick(View view) {
            MoreSettingActivity_.this.showTheme();
        }
    }

    class C01754 implements OnClickListener {
        C01754() {
        }

        public void onClick(View view) {
            MoreSettingActivity_.this.selectLanguage();
        }
    }

    class C01765 implements OnClickListener {
        C01765() {
        }

        public void onClick(View view) {
            MoreSettingActivity_.this.selectUnit();
        }
    }

    public static class IntentBuilder_ extends ActivityIntentBuilder<IntentBuilder_> {
        private Fragment fragmentSupport_;
        private android.app.Fragment fragment_;

        public IntentBuilder_(Context context) {
            super(context, MoreSettingActivity_.class);
        }

        public IntentBuilder_(android.app.Fragment fragment) {
            super(fragment.getActivity(), MoreSettingActivity_.class);
            this.fragment_ = fragment;
        }

        public IntentBuilder_(Fragment fragment) {
            super(fragment.getActivity(), MoreSettingActivity_.class);
            this.fragmentSupport_ = fragment;
        }

        public void startForResult(int requestCode) {
            if (this.fragmentSupport_ != null) {
                this.fragmentSupport_.startActivityForResult(this.intent, requestCode);
            } else if (this.fragment_ != null) {
                this.fragment_.startActivityForResult(this.intent, requestCode);
            } else {
                super.startForResult(requestCode);
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        init_(savedInstanceState);
        super.onCreate(savedInstanceState);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    private void init_(Bundle savedInstanceState) {
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        requestWindowFeature(1);
    }

    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        this.onViewChangedNotifier_.notifyViewChanged(this);
    }

    public void setContentView(View view, LayoutParams params) {
        super.setContentView(view, params);
        this.onViewChangedNotifier_.notifyViewChanged(this);
    }

    public void setContentView(View view) {
        super.setContentView(view);
        this.onViewChangedNotifier_.notifyViewChanged(this);
    }

    public static IntentBuilder_ intent(Context context) {
        return new IntentBuilder_(context);
    }

    public static IntentBuilder_ intent(android.app.Fragment fragment) {
        return new IntentBuilder_(fragment);
    }

    public static IntentBuilder_ intent(Fragment supportFragment) {
        return new IntentBuilder_(supportFragment);
    }

    public void onViewChanged(HasViews hasViews) {
        this.tvTitle = (TextView) hasViews.findViewById(C0181R.id.tvTitle);
        this.btnBack = (Button) hasViews.findViewById(C0181R.id.btnBack);
        this.llGrowth = (LinearLayout) hasViews.findViewById(C0181R.id.llGrowth);
        this.llUnit = (LinearLayout) hasViews.findViewById(C0181R.id.llUnit);
        this.rlRoot = (RelativeLayout) hasViews.findViewById(C0181R.id.rlRoot);
        this.llLanguage = (LinearLayout) hasViews.findViewById(C0181R.id.llLanguage);
        this.llSettingView = (LinearLayout) hasViews.findViewById(C0181R.id.llSettingView);
        this.tvUnit = (TextView) hasViews.findViewById(C0181R.id.tvUnit);
        this.llTop = (LinearLayout) hasViews.findViewById(C0181R.id.llTop);
        this.tvThemeType = (TextView) hasViews.findViewById(C0181R.id.tvThemeType);
        this.tvVersion = (TextView) hasViews.findViewById(C0181R.id.tvVersion);
        this.tvTitle = this.tvTitle;
        this.tvLanguage = (TextView) hasViews.findViewById(C0181R.id.tvLanguage);
        this.llVisibleView = (LinearLayout) hasViews.findViewById(C0181R.id.llVisibleView);
        this.llAbout = (LinearLayout) hasViews.findViewById(C0181R.id.llAbout);
        if (this.btnBack != null) {
            this.btnBack.setOnClickListener(new C01721());
        }
        if (this.llAbout != null) {
            this.llAbout.setOnClickListener(new C01732());
        }
        if (this.tvThemeType != null) {
            this.tvThemeType.setOnClickListener(new C01743());
        }
        if (this.llLanguage != null) {
            this.llLanguage.setOnClickListener(new C01754());
        }
        if (this.llUnit != null) {
            this.llUnit.setOnClickListener(new C01765());
        }
        InitBase();
        init();
    }
}
