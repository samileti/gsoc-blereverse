package com.whb.loease.activity;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.vc.cloudbalance.common.App;
import com.vc.cloudbalance.common.ApplicationMamager;
import com.vc.cloudbalance.common.BaseActivity;
import com.vc.cloudbalance.common.Common;
import com.vc.cloudbalance.common.Constants;
import com.vc.cloudbalance.common.DialogHelper;
import com.vc.cloudbalance.common.WeightUnitHelper;
import com.vc.cloudbalance.model.ActionMDL;
import com.vc.cloudbalance.model.BalanceDataMDL;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.sqlite.ActionDAL;
import com.vc.cloudbalance.sqlite.AppConfigDAL;
import com.vc.cloudbalance.sqlite.BalanceDataDAL;
import com.vc.cloudbalance.sqlite.MemberDAL;
import com.vc.cloudbalance.widget.BabyBalanceView;
import com.vc.cloudbalance.widget.BabyBalanceView_;
import com.vc.cloudbalance.widget.ViewGuestMemberInfo;
import com.vc.cloudbalance.widget.ViewGuestMemberInfo.OnFinishListener;
import com.vc.cloudbalance.widget.ViewGuestMemberInfo_;
import com.vc.cloudbalance.widget.View_MemberList;
import com.vc.cloudbalance.widget.View_MemberList.OnClickMemberListener;
import com.vc.cloudbalance.widget.View_MemberList_;
import com.vc.cloudbalance.widget.balanceCircleView;
import com.vc.util.ImageUtil;
import com.vc.util.ObjectHelper;
import com.whb.loease.bodytouch.C0181R;
import com.yohealth.api.btscale.YoHealthBtScaleHelper;
import com.yohealth.api.btscale.YoHealthBtScaleHelper.OnYoWeightListener;
import de.hdodenhof.circleimageview.CircleImageView;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity
public class MainActivity extends BaseActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private int age;
    Bitmap bitmap;
    YoHealthBtScaleHelper btScaleHelper;
    @ViewById
    CircleImageView circleImageView;
    private String currentBone;
    private int currentTheme = Common.getThemeType();
    private String currentUnit = "";
    private String currentWeight;
    private long exitTime = 0;
    private int height;
    @ViewById
    ImageView imgHead;
    byte[] imgStream;
    private boolean isHighPrecision = false;
    boolean isLoad = false;
    boolean isLockData = false;
    private boolean isOpenMemberListPopWindow;
    boolean isRed = false;
    boolean isRunning = false;
    boolean isUnlockData = false;
    private boolean isVersionTip = true;
    boolean isbigCircleLoad = false;
    @ViewById
    LinearLayout llMemberName;
    @ViewById
    LinearLayout llTop;
    private BabyBalanceView mBabyBalanceView;
    Handler mHandler = new C01451();
    private PopupWindow mPopupWindow;
    View_MemberList memberListContentView;
    PopupWindow memberListPopupWindow;
    List<MemberMDL> members;
    @ViewById
    RelativeLayout rlWtCircle;
    private int sex;
    @ViewById
    TableLayout tbDetailData;
    MemberMDL thisMember;
    @ViewById
    TextView tvBMIVal;
    @ViewById
    TextView tvBMIVal2;
    @ViewById
    TextView tvBoneVal;
    @ViewById
    TextView tvCalorieVal;
    @ViewById
    TextView tvFatTip;
    @ViewById
    TextView tvFatVal;
    @ViewById
    TextView tvMsg;
    @ViewById
    TextView tvMuscleVal;
    @ViewById
    TextView tvTargetWtVal;
    @ViewById
    TextView tvWaterVal;
    @ViewById
    TextView tvWeightVal;
    @ViewById
    TextView tvWtUnit1;
    @ViewById
    TextView tvWtUnit2;
    balanceCircleView wtCircleView;

    class C01451 extends Handler {
        C01451() {
        }

        public void handleMessage(Message msg) {
            boolean z = false;
            if (msg.what == 0) {
                if (((char) 1) > '(') {
                    MainActivity.this.isRunning = false;
                }
                if (MainActivity.this.isRed) {
                    MainActivity.this.tvMsg.setTextColor(MainActivity.this.getResources().getColor(C0181R.color.white));
                } else {
                    MainActivity.this.tvMsg.setTextColor(Common.getThemeColor(MainActivity.this.mContext));
                }
                MainActivity mainActivity = MainActivity.this;
                if (!MainActivity.this.isRed) {
                    z = true;
                }
                mainActivity.isRed = z;
            } else if (msg.what == 1) {
                MainActivity.this.tvMsg.setText("");
            }
            super.handleMessage(msg);
        }
    }

    class C01462 implements OnDismissListener {
        C01462() {
        }

        public void onDismiss() {
            float weight = MainActivity.this.mBabyBalanceView.getBabyWt();
            BabyBalanceView.lockedWeight = "";
            if (weight != 0.0f) {
                MainActivity.this.tbDetailData.setVisibility(4);
                MainActivity.this.tvWeightVal.setText(WeightUnitHelper.getConvertedWtVal(MainActivity.this.isHighPrecision, 2, Float.valueOf(weight)));
                MainActivity.this.currentWeight = new StringBuilder(String.valueOf(weight)).toString();
                MainActivity.this.tvBMIVal.setText("--");
                MainActivity.this.tvFatTip.setText("");
            }
            if (!MainActivity.this.thisMember.isGuest()) {
                MainActivity.this.wtCircleView.invalidate(weight, (float) ObjectHelper.Convert2Int(MainActivity.this.thisMember.getTargetweight()));
            }
            if (MainActivity.this.btScaleHelper != null) {
                MainActivity.this.btScaleHelper.start();
            }
            MainActivity.this.mBabyBalanceView.stopScaneData();
            MainActivity.this.mBabyBalanceView = null;
            System.gc();
        }
    }

    class C01473 implements OnDismissListener {
        C01473() {
        }

        public void onDismiss() {
            MainActivity.this.isOpenMemberListPopWindow = false;
        }
    }

    class C01486 implements DialogInterface.OnDismissListener {
        C01486() {
        }

        public void onDismiss(DialogInterface dialog) {
            MainActivity.this.isVersionTip = false;
        }
    }

    class C01497 implements OnGlobalLayoutListener {
        C01497() {
        }

        public void onGlobalLayout() {
            if (!MainActivity.this.isLoad) {
                LayoutParams lp = (LayoutParams) MainActivity.this.imgHead.getLayoutParams();
                lp.width = MainActivity.this.imgHead.getWidth();
                lp.height = MainActivity.this.imgHead.getHeight();
                MainActivity.this.imgHead.setLayoutParams(lp);
                LayoutParams lp2 = (LayoutParams) MainActivity.this.circleImageView.getLayoutParams();
                lp2.width = MainActivity.this.imgHead.getWidth();
                lp2.height = MainActivity.this.imgHead.getHeight();
                MainActivity.this.circleImageView.setLayoutParams(lp2);
                MainActivity.this.loadImage();
                MainActivity.this.isLoad = true;
            }
        }
    }

    class C01509 implements OnGlobalLayoutListener {
        C01509() {
        }

        public void onGlobalLayout() {
            if (!MainActivity.this.isbigCircleLoad) {
                MainActivity.this.isbigCircleLoad = true;
                int wtCircleWidth = MainActivity.this.rlWtCircle.getWidth();
                int strokeWidth = MainActivity.this.getResources().getDimensionPixelSize(C0181R.dimen.banlancecircle_thickstrokewidth);
                MainActivity.this.wtCircleView = new balanceCircleView(MainActivity.this.mContext, 0.0f, 0.0f, wtCircleWidth);
                MainActivity.this.rlWtCircle.addView(MainActivity.this.wtCircleView, new RelativeLayout.LayoutParams(wtCircleWidth + strokeWidth, wtCircleWidth + strokeWidth));
            }
        }
    }

    class C02744 implements OnClickMemberListener {
        C02744() {
        }

        public void memberSelected(MemberMDL member) {
            if (member != null) {
                MainActivity.this.memberListPopupWindow.dismiss();
                MainActivity.this.isOpenMemberListPopWindow = false;
                if (member.isGuest()) {
                    MainActivity.this.showGuestInfo(member);
                } else if (MainActivity.this.thisMember.getClientid() != member.getClientid()) {
                    MainActivity.this.clearDisplayData();
                    MainActivity.this.tvTitle.setText(member.getMembername());
                    MainActivity.this.tvTargetWtVal.setText(WeightUnitHelper.getConvertedWtVal(MainActivity.this.isHighPrecision, 1, member.getTargetweight()));
                    MainActivity.this.thisMember = member;
                    MainActivity.this.loadImage();
                }
            }
        }
    }

    class C02755 implements OnFinishListener {
        C02755() {
        }

        public void onConfirm(MemberMDL member) {
            if (MainActivity.this.thisMember.isGuest()) {
                MainActivity.this.thisMember = member;
                return;
            }
            MainActivity.this.thisMember = member;
            MainActivity.this.clearDisplayData();
            MainActivity.this.tvTitle.setText(MainActivity.this.mContext.getString(C0181R.string.guestMode));
            MainActivity.this.tvTargetWtVal.setText("");
            MainActivity.this.loadImage();
        }
    }

    class C02768 implements OnYoWeightListener {
        C02768() {
        }

        public void stableData(boolean isHighPrecision, int devicetype, int state, float weight, float bmi, float fatPercentage, float musclePercentage, float waterPercentage, float boneMass, float visceralFatPercentage, int bodyAge, int BMR) {
            if (MainActivity.this.isUnlockData && !MainActivity.this.isLockData) {
                MainActivity.this.isLockData = true;
                MainActivity.this.isUnlockData = false;
                MainActivity.this.isRunning = false;
                if (state == 1) {
                    MainActivity.this.tvMsg.setTextColor(MainActivity.this.getResources().getColor(C0181R.color.red));
                    MainActivity.this.tvMsg.setText(MainActivity.this.mContext.getString(C0181R.string.overWeight));
                    return;
                }
                if (state == 2) {
                    MainActivity.this.tvMsg.setTextColor(MainActivity.this.getResources().getColor(C0181R.color.red));
                } else if (state == 3) {
                    MainActivity.this.tvMsg.setText("");
                    MainActivity.this.tbDetailData.setVisibility(4);
                } else {
                    MainActivity.this.tvMsg.setText("");
                    if (MainActivity.this.age < 10 || devicetype == 0) {
                        MainActivity.this.tbDetailData.setVisibility(4);
                    } else if (MainActivity.this.tbDetailData.getVisibility() != 0) {
                        MainActivity.this.tbDetailData.setVisibility(0);
                    }
                }
                MainActivity.this.currentWeight = new StringBuilder(String.valueOf(weight)).toString();
                MainActivity.this.currentBone = new StringBuilder(String.valueOf(boneMass)).toString();
                MainActivity.this.tvFatVal.setText(new StringBuilder(String.valueOf(fatPercentage)).toString());
                MainActivity.this.tvWaterVal.setText(new StringBuilder(String.valueOf(waterPercentage)).toString());
                MainActivity.this.tvMuscleVal.setText(new StringBuilder(String.valueOf(musclePercentage)).toString());
                MainActivity.this.tvBoneVal.setText(WeightUnitHelper.getConvertedWtVal(1, Float.valueOf(boneMass)));
                MainActivity.this.tvWeightVal.setText(WeightUnitHelper.getConvertedWtVal(isHighPrecision, 2, Float.valueOf(weight)));
                MainActivity.this.tvCalorieVal.setText(new StringBuilder(String.valueOf(BMR)).toString());
                MainActivity.this.tvBMIVal2.setText(new StringBuilder(String.valueOf(bmi)).toString());
                MainActivity.this.tvBMIVal.setText(new StringBuilder(String.valueOf(bmi)).toString());
                if (((double) bmi) < 18.5d) {
                    MainActivity.this.tvFatTip.setText("(" + MainActivity.this.getString(C0181R.string.Underweight) + ")");
                } else if (((double) bmi) <= 24.9d) {
                    MainActivity.this.tvFatTip.setText("(" + MainActivity.this.getString(C0181R.string.Normal) + ")");
                } else if (((double) bmi) <= 29.9d) {
                    MainActivity.this.tvFatTip.setText("(" + MainActivity.this.getString(C0181R.string.Overweight) + ")");
                } else if (((double) bmi) > 29.9d) {
                    MainActivity.this.tvFatTip.setText("(" + MainActivity.this.getString(C0181R.string.Obese) + ")");
                }
                Common.playMp3(MainActivity.this.mContext);
                if (!MainActivity.this.thisMember.isGuest()) {
                    MainActivity.this.wtCircleView.invalidate(weight, ObjectHelper.Convert2Float(MainActivity.this.thisMember.getTargetweight()));
                    BalanceDataMDL balanceDataMDL = new BalanceDataMDL();
                    balanceDataMDL.setWeidate(new Date());
                    balanceDataMDL.setWeight(new StringBuilder(String.valueOf(weight)).toString());
                    balanceDataMDL.setBmi(new StringBuilder(String.valueOf(bmi)).toString());
                    balanceDataMDL.setFatpercent(new StringBuilder(String.valueOf(fatPercentage)).toString());
                    balanceDataMDL.setWater(new StringBuilder(String.valueOf(waterPercentage)).toString());
                    balanceDataMDL.setMuscle(new StringBuilder(String.valueOf(musclePercentage)).toString());
                    balanceDataMDL.setBone(new StringBuilder(String.valueOf(boneMass)).toString());
                    balanceDataMDL.setBasalmetabolism(new StringBuilder(String.valueOf(BMR)).toString());
                    balanceDataMDL.setMemberid(MainActivity.this.thisMember.getMemberid());
                    balanceDataMDL.setClientmemberid(MainActivity.this.thisMember.getClientid());
                    if (App.getApp(MainActivity.this.mContext).getUser() != null) {
                        balanceDataMDL.setUserid(App.getApp(MainActivity.this.mContext).getUser().getUserid());
                    }
                    if (new BalanceDataDAL(MainActivity.this.mContext).Insert(balanceDataMDL)) {
                        new ActionDAL(MainActivity.this.mContext).Insert(new ActionMDL(UUID.randomUUID().toString(), ActionMDL.AddBalanceData, new StringBuilder(String.valueOf(balanceDataMDL.getId())).toString()));
                        Common.AddBalanceDataThread(MainActivity.this.mContext);
                    }
                }
            }
        }

        public void realtimeData(boolean isHighPrecision2, int devicetype, float weight, float bmi) {
            if (weight > 183.0f) {
                MainActivity.this.clearDisplayData();
                MainActivity.this.tvWeightVal.setText("Err");
                return;
            }
            MainActivity.this.isLockData = false;
            MainActivity.this.isUnlockData = true;
            MainActivity.this.isHighPrecision = isHighPrecision2;
            MainActivity.this.tvWeightVal.setText(WeightUnitHelper.getConvertedWtVal(MainActivity.this.isHighPrecision, 2, Float.valueOf(weight)));
            MainActivity.this.currentWeight = new StringBuilder(String.valueOf(weight)).toString();
            MainActivity.this.tvBMIVal.setText(new StringBuilder(String.valueOf(bmi)).toString());
            if (!MainActivity.this.isRunning && !MainActivity.this.isLockData) {
                MainActivity.this.sendMemberInfo2BtHelper();
                MainActivity.this.currentBone = "0.0";
                MainActivity.this.isRunning = true;
                MainActivity.this.tvFatTip.setText("");
                MainActivity.this.tvFatVal.setText("0.0");
                MainActivity.this.tvWaterVal.setText("0.0");
                MainActivity.this.tvMuscleVal.setText("0.0");
                MainActivity.this.tvBoneVal.setText("0.0");
                MainActivity.this.tvBMIVal2.setText("0.0");
                MainActivity.this.tvCalorieVal.setText("0.0");
                if (MainActivity.this.age < 10 || devicetype == 0) {
                    MainActivity.this.tbDetailData.setVisibility(4);
                }
            }
        }

        public void error(String msg) {
            Log.e("error", msg);
        }

        public void getFactory(String factory) {
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initThemeView();
    }

    private void initThemeView() {
        switch (Common.getThemeType()) {
            case 1:
                setContentView(C0181R.layout.activity_main);
                return;
            case 2:
                setContentView(C0181R.layout.activity_main_greentheme);
                return;
            case 4:
                setContentView(C0181R.layout.activity_main_greytheme);
                return;
            default:
                setContentView(C0181R.layout.activity_main_bluetheme);
                return;
        }
    }

    protected void onResume() {
        super.onResume();
        if (this.currentTheme != Common.getThemeType()) {
            finish();
            startActivity(getIntent());
            return;
        }
        loadMembers();
        initLanguage();
        String clientId = new AppConfigDAL(this).select(Constants.SQL_KEY_LASTADULT_WTMEMBER_CLIENTID_STRING);
        if (TextUtils.isEmpty(clientId)) {
            this.thisMember = new MemberDAL(this.mContext).SelectLastMember();
            clearDisplayData();
        } else {
            this.thisMember = new MemberDAL(this.mContext).SelectByClientId(clientId);
        }
        if (this.thisMember != null) {
            this.tvTitle.setText(this.thisMember.getMembername());
            this.tvTargetWtVal.setText(WeightUnitHelper.getConvertedWtVal(true, 2, this.thisMember.getTargetweight()));
        } else {
            this.thisMember = App.guestMember();
            this.tvTitle.setText(this.mContext.getString(C0181R.string.guestMode));
            this.tvTargetWtVal.setText("");
        }
        if (!this.currentUnit.equals(WeightUnitHelper.getWeightUnitString())) {
            this.currentUnit = WeightUnitHelper.getWeightUnitString();
            if (this.currentWeight != null) {
                this.tvWeightVal.setText(WeightUnitHelper.getConvertedWtVal(this.isHighPrecision, 2, this.currentWeight));
            }
            if (this.currentBone != null) {
                this.tvBoneVal.setText(WeightUnitHelper.getConvertedWtVal(1, this.currentBone));
            }
            this.tvWtUnit1.setText(this.currentUnit);
            this.tvWtUnit2.setText(this.currentUnit);
        }
        imgHeadInit();
        System.gc();
        if (this.memberListPopupWindow != null) {
            this.memberListPopupWindow.dismiss();
        }
        if (this.isLoad) {
            loadImage();
        }
        if (this.btScaleHelper != null) {
            this.btScaleHelper.start();
        }
    }

    protected void onPause() {
        if (this.btScaleHelper != null) {
            this.btScaleHelper.stop();
        }
        if (!(this.thisMember == null || this.thisMember.isGuest())) {
            new AppConfigDAL(this).insert(Constants.SQL_KEY_LASTADULT_WTMEMBER_CLIENTID_STRING, this.thisMember.getClientid());
        }
        if (this.isHighPrecision) {
            new AppConfigDAL(this).insert(Constants.DEVICETYPE_STRING, "2");
        } else {
            new AppConfigDAL(this).insert(Constants.DEVICETYPE_STRING, "1");
        }
        super.onPause();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        if (System.currentTimeMillis() - this.exitTime > 2000) {
            Toast.makeText(getApplicationContext(), this.mContext.getString(C0181R.string.quitAppTip), 0).show();
            this.exitTime = System.currentTimeMillis();
            return false;
        }
        ApplicationMamager.getInstance().exit();
        return false;
    }

    public void onDestroy() {
        if (this.btScaleHelper != null) {
            this.btScaleHelper.stop();
        }
        super.onDestroy();
    }

    @Click({2131492946})
    void showDataViewActivity() {
        if (this.thisMember.isGuest()) {
            DialogHelper.showTost(this.mContext, getString(C0181R.string.notsupporguest));
            return;
        }
        Intent intent = DataViewActivity_.intent((Context) this).get();
        intent.putExtra("EXTRA_KEY_ID_STRING", this.thisMember.getClientid());
        startActivity(intent);
    }

    @Click({2131492950})
    void showTargetViewActivity() {
        if (this.thisMember.isGuest()) {
            DialogHelper.showTost(this.mContext, getString(C0181R.string.notsupporguest));
            return;
        }
        Intent intent = TargetViewActivity_.intent((Context) this).get();
        intent.putExtra("EXTRA_KEY_ID_STRING", this.thisMember.getClientid());
        startActivity(intent);
    }

    @Click({2131492954})
    void showChildBalanceActivity() {
        if (this.btScaleHelper != null) {
            this.btScaleHelper.stop();
        }
        if (this.mPopupWindow == null) {
            this.mPopupWindow = new PopupWindow(-1, -1);
            this.mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
            this.mPopupWindow.setFocusable(true);
        }
        if (this.mBabyBalanceView == null) {
            this.mBabyBalanceView = BabyBalanceView_.build(this, this.thisMember, this.mPopupWindow);
        }
        this.mPopupWindow.setContentView(this.mBabyBalanceView);
        this.mPopupWindow.showAsDropDown(this.llTop);
        this.mPopupWindow.setOnDismissListener(new C01462());
    }

    @Click({2131492958})
    void Experience() {
        startActivity(MoreSettingActivity_.intent((Context) this).get());
    }

    @Click({2131492880})
    void showSettingActivity() {
        if (this.thisMember.isGuest()) {
            showGuestInfo(this.thisMember);
            return;
        }
        Intent intent = MemberInfoActivity_.intent(this.mContext).get();
        intent.putExtra("EXTRA_KEY_ID_STRING", this.thisMember.getClientid());
        this.mContext.startActivity(intent);
    }

    @Click({2131492883})
    void showMemberList() {
        if (this.memberListPopupWindow == null) {
            this.memberListPopupWindow = new PopupWindow(this.memberListContentView, -2, 500, false);
            this.memberListPopupWindow.setBackgroundDrawable(new BitmapDrawable());
            this.memberListPopupWindow.setOutsideTouchable(true);
            this.memberListPopupWindow.setFocusable(true);
            this.memberListPopupWindow.setOnDismissListener(new C01473());
        }
        if (this.isOpenMemberListPopWindow) {
            this.memberListPopupWindow.dismiss();
            this.isOpenMemberListPopWindow = false;
            return;
        }
        this.memberListContentView.setMember(this.thisMember);
        this.memberListContentView.loadAllMembers();
        int[] location = new int[2];
        this.llMemberName.getLocationOnScreen(location);
        this.memberListPopupWindow.showAtLocation(this.llMemberName, 49, 0, (location[1] + this.llMemberName.getHeight()) + 10);
        this.isOpenMemberListPopWindow = true;
    }

    @AfterViews
    void init() {
        if (new AppConfigDAL(this).select(Constants.DEVICETYPE_STRING).equals("2")) {
            this.isHighPrecision = true;
        }
        String clientId = new AppConfigDAL(this).select(Constants.SQL_KEY_LASTADULT_WTMEMBER_CLIENTID_STRING);
        if (TextUtils.isEmpty(clientId)) {
            this.thisMember = new MemberDAL(this.mContext).SelectLastMember();
        } else {
            this.thisMember = new MemberDAL(this.mContext).SelectByClientId(clientId);
        }
        if (this.thisMember != null) {
            this.tvTitle.setText(this.thisMember.getMembername());
        } else {
            this.thisMember = App.guestMember();
            this.tvTitle.setText(this.mContext.getString(C0181R.string.guestMode));
        }
        drawBigCircle();
        this.tvTargetWtVal.setText(WeightUnitHelper.getConvertedWtVal(true, 2, this.thisMember.getTargetweight()));
        this.currentUnit = WeightUnitHelper.getWeightUnitString();
        this.tvWtUnit1.setText(this.currentUnit);
        this.tvWtUnit2.setText(this.currentUnit);
        if (systemVersionCheck()) {
            recvData();
        }
    }

    private void loadMembers() {
        if (this.memberListContentView == null) {
            this.memberListContentView = View_MemberList_.build(this.mContext, this.thisMember);
        }
        this.memberListContentView.setOnClickMemberListener(new C02744());
    }

    private void showGuestInfo(MemberMDL member) {
        PopupWindow popupWindow = new PopupWindow(-1, -1);
        popupWindow.setBackgroundDrawable(new ColorDrawable());
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        ViewGuestMemberInfo viewGuestMemberInfo = ViewGuestMemberInfo_.build(this.mContext, popupWindow, member);
        viewGuestMemberInfo.setOnFinishListener(new C02755());
        popupWindow.setContentView(viewGuestMemberInfo);
        popupWindow.showAtLocation(this.llTop, 1, 0, 0);
    }

    private void clearDisplayData() {
        if (this.thisMember != null) {
            this.tvTargetWtVal.setText(WeightUnitHelper.getConvertedWtVal(true, 2, this.thisMember.getTargetweight()));
        }
        this.currentWeight = "0.0";
        this.currentBone = "0.0";
        this.tvWeightVal.setText("0.0");
        this.tvFatVal.setText("0.0");
        this.tvWaterVal.setText("0.0");
        this.tvMuscleVal.setText("0.0");
        this.tvBoneVal.setText("0.0");
        this.tvBMIVal.setText("0.0");
        this.tvBMIVal2.setText("0.0");
        this.tvCalorieVal.setText("0.0");
        this.tvFatTip.setText("");
        this.tvWtUnit1.setText(this.currentUnit);
        this.tvWtUnit2.setText(this.currentUnit);
        if (this.wtCircleView != null) {
            this.wtCircleView.invalidate(0.0f, 0.0f);
        }
    }

    private boolean systemVersionCheck() {
        if (VERSION.SDK_INT >= 18 || !this.isVersionTip) {
            return true;
        }
        new Builder(this.mContext).setTitle(this.mContext.getString(C0181R.string.tip)).setMessage(this.mContext.getString(C0181R.string.notsupporSysterm)).setPositiveButton(this.mContext.getString(C0181R.string.confirm), null).show().setOnDismissListener(new C01486());
        return false;
    }

    private void imgHeadInit() {
        this.imgHead.getViewTreeObserver().addOnGlobalLayoutListener(new C01497());
    }

    private void initLanguage() {
        int LanguageVal = getSharedPreferences("AppConfigDAL", 0).getInt(Constants.SQL_KEY_LANGUAGE_STRING, 0);
        if (LanguageVal == 17) {
            ((TextView) findViewById(C0181R.id.textView12)).setTextSize(8.0f);
        }
        if (LanguageVal != 0) {
            try {
                Resources resources = getResources();
                Configuration config = resources.getConfiguration();
                DisplayMetrics dm = resources.getDisplayMetrics();
                String LanguageChange = new AppConfigDAL(this.mContext).select(Constants.SQL_KEY_LANGUAGECHANGE_STRING);
                boolean isChange = false;
                if (!config.locale.getLanguage().equals(Common.getLanguageShortCodes()[LanguageVal - 1])) {
                    if (LanguageVal != 1 && LanguageVal != 2) {
                        isChange = true;
                    } else if (!config.locale.getCountry().equals(Common.getLanguageShortCodes()[LanguageVal - 1])) {
                        isChange = true;
                    }
                }
                if (isChange || LanguageChange.equals("1")) {
                    if (LanguageVal == 1 || LanguageVal == 2) {
                        config.locale = new Locale("zh", Common.getLanguageShortCodes()[LanguageVal - 1]);
                    } else {
                        config.locale = new Locale(Common.getLanguageShortCodes()[LanguageVal - 1]);
                    }
                    resources.updateConfiguration(config, dm);
                    Locale.setDefault(config.locale);
                    new AppConfigDAL(this.mContext).insert(Constants.SQL_KEY_LANGUAGECHANGE_STRING, WeightUnitHelper.Kg);
                    finish();
                    startActivity(getIntent());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void loadImage() {
        if (this.thisMember.getClientImg() == null || this.thisMember.getClientImg().length <= 0) {
            this.imgHead.setVisibility(0);
            this.circleImageView.setVisibility(8);
            return;
        }
        this.imgStream = this.thisMember.getClientImg();
        this.bitmap = ImageUtil.decodeSampledBitmapFromByte(this.imgStream, this.imgHead.getWidth(), this.imgHead.getHeight());
        if (this.bitmap != null) {
            this.imgHead.setVisibility(8);
            this.circleImageView.setVisibility(0);
            this.circleImageView.setImageBitmap(this.bitmap);
        }
    }

    private void recvData() {
        this.height = ObjectHelper.Convert2Int(this.thisMember.getHeight());
        this.age = Common.GetAgeByBirthday(this.thisMember.getBirthday());
        if (this.age <= 0) {
            this.age = 25;
        } else if (this.age < 10) {
            this.tbDetailData.setVisibility(4);
        }
        this.sex = ObjectHelper.Convert2Int(this.thisMember.getSex());
        if (this.sex == 0) {
            this.sex = 1;
        } else {
            this.sex = 0;
        }
        this.btScaleHelper = new YoHealthBtScaleHelper(this, this.height, this.age, this.sex);
        this.btScaleHelper.setOnYoWeightListener(new C02768());
        this.btScaleHelper.init();
        this.btScaleHelper.start();
    }

    private void sendMemberInfo2BtHelper() {
        this.height = ObjectHelper.Convert2Int(this.thisMember.getHeight());
        this.age = Common.GetAgeByBirthday(this.thisMember.getBirthday());
        if (this.age <= 0) {
            this.age = 25;
        } else if (this.age < 10) {
            this.tbDetailData.setVisibility(4);
        }
        this.sex = ObjectHelper.Convert2Int(this.thisMember.getSex());
        if (this.sex == 0) {
            this.sex = 1;
        } else {
            this.sex = 0;
        }
        this.btScaleHelper.setMemberInfo(this.height, this.age, this.sex);
    }

    private void drawBigCircle() {
        this.rlWtCircle.getViewTreeObserver().addOnGlobalLayoutListener(new C01509());
    }
}
