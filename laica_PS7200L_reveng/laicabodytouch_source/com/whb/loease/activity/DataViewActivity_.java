package com.whb.loease.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.api.builder.ActivityIntentBuilder;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class DataViewActivity_ extends DataViewActivity implements HasViews, OnViewChangedListener {
    public static final String CLIENT_ID_EXTRA = "EXTRA_KEY_ID_STRING";
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    class C01441 implements OnClickListener {
        C01441() {
        }

        public void onClick(View view) {
            DataViewActivity_.this.goBack();
        }
    }

    public static class IntentBuilder_ extends ActivityIntentBuilder<IntentBuilder_> {
        private Fragment fragmentSupport_;
        private android.app.Fragment fragment_;

        public IntentBuilder_(Context context) {
            super(context, DataViewActivity_.class);
        }

        public IntentBuilder_(android.app.Fragment fragment) {
            super(fragment.getActivity(), DataViewActivity_.class);
            this.fragment_ = fragment;
        }

        public IntentBuilder_(Fragment fragment) {
            super(fragment.getActivity(), DataViewActivity_.class);
            this.fragmentSupport_ = fragment;
        }

        public void startForResult(int requestCode) {
            if (this.fragmentSupport_ != null) {
                this.fragmentSupport_.startActivityForResult(this.intent, requestCode);
            } else if (this.fragment_ != null) {
                this.fragment_.startActivityForResult(this.intent, requestCode);
            } else {
                super.startForResult(requestCode);
            }
        }

        public IntentBuilder_ ClientId(String ClientId) {
            return (IntentBuilder_) super.extra("EXTRA_KEY_ID_STRING", ClientId);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        init_(savedInstanceState);
        super.onCreate(savedInstanceState);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
        setContentView((int) C0181R.layout.activity_dataview);
    }

    private void init_(Bundle savedInstanceState) {
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        requestWindowFeature(1);
        injectExtras_();
    }

    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        this.onViewChangedNotifier_.notifyViewChanged(this);
    }

    public void setContentView(View view, LayoutParams params) {
        super.setContentView(view, params);
        this.onViewChangedNotifier_.notifyViewChanged(this);
    }

    public void setContentView(View view) {
        super.setContentView(view);
        this.onViewChangedNotifier_.notifyViewChanged(this);
    }

    public static IntentBuilder_ intent(Context context) {
        return new IntentBuilder_(context);
    }

    public static IntentBuilder_ intent(android.app.Fragment fragment) {
        return new IntentBuilder_(fragment);
    }

    public static IntentBuilder_ intent(Fragment supportFragment) {
        return new IntentBuilder_(supportFragment);
    }

    public void onViewChanged(HasViews hasViews) {
        this.tvTitle = (TextView) hasViews.findViewById(C0181R.id.tvTitle);
        this.btnBack = (Button) hasViews.findViewById(C0181R.id.btnBack);
        this.viewFlipper = (ViewFlipper) hasViews.findViewById(C0181R.id.viewFlipper);
        this.lineViewRight = hasViews.findViewById(C0181R.id.lineViewRight);
        this.lineViewLeft = hasViews.findViewById(C0181R.id.lineViewLeft);
        if (this.btnBack != null) {
            this.btnBack.setOnClickListener(new C01441());
        }
        InitBase();
        init();
    }

    private void injectExtras_() {
        Bundle extras_ = getIntent().getExtras();
        if (extras_ != null && extras_.containsKey("EXTRA_KEY_ID_STRING")) {
            this.ClientId = extras_.getString("EXTRA_KEY_ID_STRING");
        }
    }

    public void setIntent(Intent newIntent) {
        super.setIntent(newIntent);
        injectExtras_();
    }
}
