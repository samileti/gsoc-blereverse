package com.vc.cloudbalance.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.vc.cloudbalance.common.UIHelper;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(2130903059)
public class View_AboutUs extends LinearLayout {
    Context mContext;
    @ViewById
    TextView tvVersion1;
    @ViewById
    TextView tvVersion2;

    public View_AboutUs(Context context) {
        super(context);
        this.mContext = context;
    }

    public View_AboutUs(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    @AfterViews
    void init() {
        String version;
        UIHelper.SetText((int) C0181R.id.tvTitle, this.mContext.getString(C0181R.string.about), (View) this);
        try {
            version = this.mContext.getPackageManager().getPackageInfo(this.mContext.getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
            version = "1";
        }
        this.tvVersion1.setText(version.substring(0, 1));
        this.tvVersion2.setText(version.substring(1));
    }
}
