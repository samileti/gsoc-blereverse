package com.vc.cloudbalance.widget;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.vc.cloudbalance.common.App;
import com.vc.cloudbalance.common.Common;
import com.vc.cloudbalance.common.Constants;
import com.vc.cloudbalance.common.WeightUnitHelper;
import com.vc.cloudbalance.model.BalanceDataMDL;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.sqlite.AppConfigDAL;
import com.vc.cloudbalance.sqlite.BalanceDataDAL;
import com.vc.util.ObjectHelper;
import com.whb.loease.bodytouch.C0181R;
import com.yohealth.api.btscale.YoHealthBtScaleHelper;
import com.yohealth.api.btscale.YoHealthBtScaleHelper.OnYoWeightListener;
import java.util.Date;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup
public class BabyBalanceView extends LinearLayout {
    public static String BMI = "";
    private static final String TAG = BabyBalanceView.class.getSimpleName();
    public static String lockedWeight = "";
    private Byte balanceCount = Byte.valueOf((byte) 0);
    private String banlanceType = "2";
    YoHealthBtScaleHelper btScaleHelper;
    @ViewById
    Button btnCancel;
    @ViewById
    Button btnConfirm;
    private float firstWeightVal;
    @ViewById
    RadioButton imgBaby;
    @ViewById
    RadioButton imgMom;
    @ViewById
    RadioButton imgMomAndBB;
    private boolean isHighPrecision = false;
    boolean isLoad = false;
    boolean isLockData = false;
    boolean isRed = false;
    boolean isRunning = false;
    boolean isUnlockData = false;
    private boolean isVersionTip = false;
    private boolean isbigCircleLoad = false;
    @ViewById
    LinearLayout llMiddle;
    private Context mContext;
    Handler mHandler = new C00891();
    PopupWindow mPopupWindow;
    @ViewById
    RelativeLayout rlWtCircle;
    MemberMDL thisMember;
    @ViewById
    TextView tvBabyWtVal;
    @ViewById
    TextView tvMomBBWtVal;
    @ViewById
    TextView tvMomWtVal;
    @ViewById
    TextView tvMsg;
    @ViewById
    TextView tvTitle;
    @ViewById
    TextView tvWeightVal;
    @ViewById
    TextView tvWtUnit1;
    @ViewById
    TextView tvWtUnit2;
    @ViewById
    TextView tvWtUnit3;
    @ViewById
    TextView tvWtUnit4;
    protected balanceCircleView wtCircleView;

    class C00891 extends Handler {
        C00891() {
        }

        public void handleMessage(Message msg) {
            boolean z = false;
            if (msg.what == 0) {
                if (((char) 1) > '(') {
                    BabyBalanceView.this.isRunning = false;
                }
                if (BabyBalanceView.this.isRed) {
                    BabyBalanceView.this.tvMsg.setTextColor(BabyBalanceView.this.getResources().getColor(C0181R.color.white));
                } else {
                    BabyBalanceView.this.tvMsg.setTextColor(Common.getThemeColor(BabyBalanceView.this.mContext));
                }
                BabyBalanceView babyBalanceView = BabyBalanceView.this;
                if (!BabyBalanceView.this.isRed) {
                    z = true;
                }
                babyBalanceView.isRed = z;
            } else if (msg.what == 1) {
                BabyBalanceView.this.tvMsg.setText("");
            }
            super.handleMessage(msg);
        }
    }

    class C00902 implements OnDismissListener {
        C00902() {
        }

        public void onDismiss(DialogInterface dialog) {
            BabyBalanceView.this.isVersionTip = false;
        }
    }

    class C00913 implements OnGlobalLayoutListener {
        C00913() {
        }

        public void onGlobalLayout() {
            if (!BabyBalanceView.this.isbigCircleLoad) {
                BabyBalanceView.this.isbigCircleLoad = true;
                int wtCircleWidth = BabyBalanceView.this.rlWtCircle.getWidth();
                int strokeWidth = BabyBalanceView.this.getResources().getDimensionPixelSize(C0181R.dimen.banlancecircle_thickstrokewidth);
                BabyBalanceView.this.wtCircleView = new balanceCircleView(BabyBalanceView.this.mContext, 0.0f, 0.0f, wtCircleWidth);
                BabyBalanceView.this.rlWtCircle.addView(BabyBalanceView.this.wtCircleView, new LayoutParams(wtCircleWidth + strokeWidth, wtCircleWidth + strokeWidth));
            }
        }
    }

    public interface OnFinishListener {
        void onConfirm(float f);
    }

    class C02634 implements OnYoWeightListener {
        C02634() {
        }

        public void stableData(boolean isHighPrecision2, int devicetype, int state, float weight, float bmi, float fatPercentage, float musclePercentage, float waterPercentage, float boneMass, float visceralFatPercentage, int bodyAge, int BMR) {
            if (BabyBalanceView.this.isUnlockData && !BabyBalanceView.this.isLockData) {
                BabyBalanceView.this.isLockData = true;
                BabyBalanceView.this.isUnlockData = false;
                BabyBalanceView.this.isRunning = false;
                if (state == 1) {
                    BabyBalanceView.this.tvMsg.setTextColor(BabyBalanceView.this.getResources().getColor(C0181R.color.red));
                    BabyBalanceView.this.tvMsg.setText(BabyBalanceView.this.mContext.getString(C0181R.string.overWeight));
                    return;
                }
                if (state == 2) {
                    BabyBalanceView.this.tvMsg.setTextColor(BabyBalanceView.this.getResources().getColor(C0181R.color.red));
                } else {
                    BabyBalanceView.this.tvMsg.setText("");
                }
                Common.playMp3(BabyBalanceView.this.mContext);
                if (BabyBalanceView.this.balanceCount.byteValue() == (byte) 0) {
                    BabyBalanceView.this.firstWeightVal = weight;
                    BabyBalanceView.this.balanceCount = Byte.valueOf((byte) 1);
                    BabyBalanceView.this.tvMomWtVal.setText(WeightUnitHelper.getConvertedWtVal(BabyBalanceView.this.isHighPrecision, 2, Float.valueOf(weight)));
                    BabyBalanceView.this.imgMom.setChecked(true);
                } else if (BabyBalanceView.this.balanceCount.byteValue() == (byte) 1) {
                    BabyBalanceView.lockedWeight = ObjectHelper.Convert2MathCount(2, Float.valueOf(Math.abs(weight - BabyBalanceView.this.firstWeightVal)));
                    if (!BabyBalanceView.this.thisMember.isGuest()) {
                        BabyBalanceView.this.wtCircleView.invalidate(Math.abs(weight - BabyBalanceView.this.firstWeightVal), ObjectHelper.Convert2Float(BabyBalanceView.this.thisMember.getTargetweight()));
                    }
                    if (weight - BabyBalanceView.this.firstWeightVal < 0.0f) {
                        BabyBalanceView.this.tvMomWtVal.setText(WeightUnitHelper.getConvertedWtVal(BabyBalanceView.this.isHighPrecision, 2, Float.valueOf(weight)));
                        BabyBalanceView.this.tvMomBBWtVal.setText(WeightUnitHelper.getConvertedWtVal(BabyBalanceView.this.isHighPrecision, 2, Float.valueOf(BabyBalanceView.this.firstWeightVal)));
                    } else {
                        BabyBalanceView.this.tvMomBBWtVal.setText(WeightUnitHelper.getConvertedWtVal(BabyBalanceView.this.isHighPrecision, 2, Float.valueOf(weight)));
                        BabyBalanceView.this.tvMomWtVal.setText(WeightUnitHelper.getConvertedWtVal(BabyBalanceView.this.isHighPrecision, 2, Float.valueOf(BabyBalanceView.this.firstWeightVal)));
                    }
                    BabyBalanceView.this.tvBabyWtVal.setText(WeightUnitHelper.getConvertedWtVal(BabyBalanceView.this.isHighPrecision, 2, BabyBalanceView.lockedWeight));
                    BabyBalanceView.this.tvWeightVal.setText(WeightUnitHelper.getConvertedWtVal(BabyBalanceView.this.isHighPrecision, 2, BabyBalanceView.lockedWeight));
                    BabyBalanceView.this.balanceCount = Byte.valueOf((byte) 0);
                    BabyBalanceView.this.btnConfirm.setEnabled(true);
                    BabyBalanceView.this.btnCancel.setEnabled(true);
                    BabyBalanceView.this.imgMomAndBB.setChecked(true);
                    BabyBalanceView.this.imgBaby.setChecked(true);
                }
            }
        }

        public void realtimeData(boolean isHighPrecision2, int devicetype, float weight, float bmi) {
            BabyBalanceView.this.isHighPrecision = isHighPrecision2;
            if (weight > 183.0f) {
                BabyBalanceView.this.clearDisplayData();
                BabyBalanceView.this.tvWeightVal.setText("Err");
                return;
            }
            BabyBalanceView.this.isLockData = false;
            BabyBalanceView.this.isUnlockData = true;
            BabyBalanceView.this.tvWeightVal.setText(WeightUnitHelper.getConvertedWtVal(BabyBalanceView.this.isHighPrecision, 2, Float.valueOf(weight)));
            if (!BabyBalanceView.this.isRunning && !BabyBalanceView.this.isLockData) {
                if (BabyBalanceView.this.balanceCount.byteValue() == (byte) 0) {
                    BabyBalanceView.this.tvWeightVal.setText("0.00");
                    BabyBalanceView.this.tvMomWtVal.setText("0.00");
                    BabyBalanceView.this.tvMomBBWtVal.setText("0.00");
                    BabyBalanceView.this.tvBabyWtVal.setText("0.00");
                    BabyBalanceView.this.imgMom.setChecked(false);
                    BabyBalanceView.this.imgMomAndBB.setChecked(false);
                    BabyBalanceView.this.imgBaby.setChecked(false);
                }
                BabyBalanceView.this.btnConfirm.setEnabled(false);
                BabyBalanceView.this.btnCancel.setEnabled(false);
                BabyBalanceView.this.isRunning = true;
            }
        }

        public void error(String msg) {
            Log.e("error", msg);
        }

        public void getFactory(String factory) {
        }
    }

    public BabyBalanceView(Context context, MemberMDL member, PopupWindow popupWindow) {
        super(context);
        this.mContext = context;
        this.thisMember = member;
        this.mPopupWindow = popupWindow;
        initThemeView();
    }

    public BabyBalanceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initThemeView();
    }

    private void initThemeView() {
        switch (Common.getThemeType()) {
            case 1:
                inflate(getContext(), C0181R.layout.view_balance_baby, this);
                return;
            case 2:
                inflate(getContext(), C0181R.layout.view_balance_baby_greentheme, this);
                return;
            case 4:
                inflate(getContext(), C0181R.layout.view_balance_baby_greytheme, this);
                return;
            default:
                inflate(getContext(), C0181R.layout.view_balance_baby_bluetheme, this);
                return;
        }
    }

    @Click({2131493033})
    void notSaveData() {
        this.tvMsg.setText("");
        lockedWeight = "0.0";
        BMI = "";
        this.tvMomWtVal.setText("0.00");
        this.tvMomBBWtVal.setText("0.00");
        this.tvBabyWtVal.setText("0.00");
        this.tvWeightVal.setText("0.00");
        this.btnConfirm.setEnabled(false);
        this.btnCancel.setEnabled(false);
    }

    @Click({2131492873})
    void back() {
        lockedWeight = "0.0";
        BMI = "";
        stopScaneData();
        if (this.isHighPrecision) {
            new AppConfigDAL(this.mContext).insert(Constants.DEVICETYPE_STRING, "2");
        } else {
            new AppConfigDAL(this.mContext).insert(Constants.DEVICETYPE_STRING, "1");
        }
        if (this.mPopupWindow != null) {
            this.mPopupWindow.dismiss();
        }
    }

    @Click({2131493034})
    void saveData() {
        stopScaneData();
        if (!(this.thisMember.isGuest() || lockedWeight.equals("0.0"))) {
            BalanceDataMDL balanceDataMDL = new BalanceDataMDL();
            if (new BalanceDataDAL(this.mContext).SelectLastData(this.thisMember.getMemberid(), this.thisMember.getClientid()) == null) {
                BalanceDataMDL lastdataDataMDL = new BalanceDataMDL();
            }
            balanceDataMDL.setWeidate(new Date());
            balanceDataMDL.setWeight(lockedWeight);
            balanceDataMDL.setMemberid(this.thisMember.getMemberid());
            balanceDataMDL.setClientmemberid(this.thisMember.getClientid());
            if (App.getApp(this.mContext).getUser() != null) {
                balanceDataMDL.setUserid(App.getApp(this.mContext).getUser().getUserid());
            }
            new BalanceDataDAL(this.mContext).Insert(balanceDataMDL);
        }
        if (this.isHighPrecision) {
            new AppConfigDAL(this.mContext).insert(Constants.DEVICETYPE_STRING, "2");
        } else {
            new AppConfigDAL(this.mContext).insert(Constants.DEVICETYPE_STRING, "1");
        }
        if (this.mPopupWindow != null) {
            this.mPopupWindow.dismiss();
        }
    }

    public void stopScaneData() {
        if (this.btScaleHelper != null) {
            this.btScaleHelper.stop();
        }
    }

    @AfterViews
    void init() {
        if (new AppConfigDAL(this.mContext).select(Constants.DEVICETYPE_STRING).equals("2")) {
            this.isHighPrecision = true;
        }
        this.tvMsg.setVisibility(4);
        this.tvWtUnit1.setText(WeightUnitHelper.getWeightUnitString());
        this.tvWtUnit2.setText(WeightUnitHelper.getWeightUnitString());
        this.tvWtUnit3.setText(WeightUnitHelper.getWeightUnitString());
        this.tvWtUnit4.setText(WeightUnitHelper.getWeightUnitString());
        this.btnConfirm.setEnabled(false);
        this.btnCancel.setEnabled(false);
        int version = VERSION.SDK_INT;
        if (this.thisMember != null) {
            if (version >= 18 || !this.isVersionTip) {
                drawBigCircle();
                recvData();
                return;
            }
            new Builder(this.mContext).setTitle(null).setMessage(this.mContext.getString(C0181R.string.notsupporSysterm)).setPositiveButton(this.mContext.getString(C0181R.string.confirm), null).show().setOnDismissListener(new C00902());
        }
    }

    public float getBabyWt() {
        return ObjectHelper.Convert2Float(lockedWeight);
    }

    private void drawBigCircle() {
        this.rlWtCircle.getViewTreeObserver().addOnGlobalLayoutListener(new C00913());
    }

    private void recvData() {
        int sex;
        int height = ObjectHelper.Convert2Int(this.thisMember.getHeight());
        int age = Common.GetAgeByBirthday(this.thisMember.getBirthday());
        if (ObjectHelper.Convert2Int(this.thisMember.getSex()) == 0) {
            sex = 1;
        } else {
            sex = 0;
        }
        this.btScaleHelper = new YoHealthBtScaleHelper(this.mContext, height, age, sex);
        this.btScaleHelper.setOnYoWeightListener(new C02634());
        this.btScaleHelper.init();
        this.btScaleHelper.start();
    }

    private void clearDisplayData() {
        this.tvWeightVal.setText("0.00");
        this.tvMomWtVal.setText("0.00");
        this.tvMomBBWtVal.setText("0.00");
        this.tvBabyWtVal.setText("0.00");
        lockedWeight = "0.00";
        this.imgMom.setChecked(false);
        this.imgMomAndBB.setChecked(false);
        this.imgBaby.setChecked(false);
        this.btnConfirm.setEnabled(false);
        this.btnCancel.setEnabled(false);
    }
}
