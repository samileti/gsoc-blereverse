package com.vc.cloudbalance.widget;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Scroller;
import android.widget.TextView;
import com.vc.cloudbalance.common.Common;
import com.vc.cloudbalance.common.Constants;
import com.vc.cloudbalance.common.DialogHelper;
import com.vc.cloudbalance.common.WeightUnitHelper;
import com.vc.cloudbalance.model.BalanceDataMDL;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.sqlite.AppConfigDAL;
import com.vc.cloudbalance.sqlite.BalanceDataDAL;
import com.vc.cloudbalance.widget.LockableHorizontalScrollView.OnCustomTouchListenter;
import com.vc.util.ImageUtil;
import com.vc.util.ObjectHelper;
import com.whb.loease.activity.DataViewActivity;
import com.whb.loease.bodytouch.C0181R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.util.List;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(2130903073)
public class View_Item_Data extends LinearLayout {
    @ViewById
    View LineView;
    private Bitmap bitmap;
    @ViewById
    Button btnDelete;
    @ViewById
    CircleImageView circleImageView;
    BalanceDataMDL data;
    private byte[] imgStream;
    boolean isLoad;
    @ViewById
    LinearLayout llBack;
    @ViewById
    LockableHorizontalScrollView lockableHorizontalScrollView1;
    private Context mContext;
    private Scroller mScroller;
    MemberMDL member;
    int offset_x = 0;
    OnClickItemDataListenter onClickItemDataListenter;
    OnDeleteDataListenter onDeleteDataListenter;
    DataViewActivity parentView;
    @ViewById
    RelativeLayout rlFront;
    ScrollViewExtend scroll;
    @ViewById
    TextView tvDate;
    @ViewById
    TextView tvDay;
    @ViewById
    TextView tvRightVal;
    @ViewById
    TextView tvWeightVal;

    class C01131 implements OnClickListener {
        C01131() {
        }

        public void onClick(DialogInterface dialog, int which) {
            BalanceDataDAL dal = new BalanceDataDAL(View_Item_Data.this.mContext);
            List<BalanceDataMDL> old_datas = dal.SelectThisDateData(View_Item_Data.this.data.getMemberid(), View_Item_Data.this.data.getClientmemberid(), ObjectHelper.Convert2String(View_Item_Data.this.data.getWeidate(), "yyyy-MM-dd 00:00:00"), ObjectHelper.Convert2String(View_Item_Data.this.data.getWeidate(), "yyyy-MM-dd 23:59:59"));
            if (old_datas.size() > 0) {
                boolean isNeedUpload = false;
                if (((BalanceDataMDL) old_datas.get(0)).getId() == View_Item_Data.this.data.getId()) {
                    isNeedUpload = true;
                }
                if (dal.DelById(View_Item_Data.this.data.getId())) {
                    if (View_Item_Data.this.onDeleteDataListenter != null) {
                        View_Item_Data.this.onDeleteDataListenter.delete("");
                    }
                    if (isNeedUpload && old_datas.size() != 1) {
                        dal.setUnloadData((BalanceDataMDL) old_datas.get(1));
                        Common.SynBalanceDataThread(View_Item_Data.this.mContext);
                    }
                }
            }
        }
    }

    class C01142 implements OnClickListener {
        C01142() {
        }

        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    class C01153 implements OnGlobalLayoutListener {
        C01153() {
        }

        public void onGlobalLayout() {
            if (!View_Item_Data.this.isLoad || View_Item_Data.this.offset_x == 0) {
                View_Item_Data.this.offset_x = View_Item_Data.this.llBack.getWidth();
                View_Item_Data.this.lockableHorizontalScrollView1.setScroll_x(View_Item_Data.this.offset_x);
                View_Item_Data.this.isLoad = true;
            }
        }
    }

    class C01165 implements OnGlobalLayoutListener {
        C01165() {
        }

        public void onGlobalLayout() {
            if (!View_Item_Data.this.isLoad) {
                LayoutParams lp2 = (LayoutParams) View_Item_Data.this.circleImageView.getLayoutParams();
                lp2.width = View_Item_Data.this.circleImageView.getWidth();
                lp2.height = View_Item_Data.this.circleImageView.getHeight();
                View_Item_Data.this.circleImageView.setLayoutParams(lp2);
                View_Item_Data.this.isLoad = true;
            }
        }
    }

    public interface OnClickItemDataListenter {
        void click(BalanceDataMDL balanceDataMDL);
    }

    public interface OnDeleteDataListenter {
        void delete(String str);
    }

    class C02694 implements OnCustomTouchListenter {
        C02694() {
        }

        public void open() {
        }

        public void close() {
        }

        public void click() {
            if (View_Item_Data.this.data != null) {
                View_Item_Data.this.onClickItemDataListenter.click(View_Item_Data.this.data);
            }
        }

        public void prePage() {
            Log.e("prePage", "prePage");
            View_Item_Data.this.parentView.prePager();
        }

        public void touch() {
            View_Item_Data.this.scroll.IsTouchingChildren();
        }
    }

    public View_Item_Data(Context context, ScrollViewExtend scroll, DataViewActivity parentView) {
        super(context);
        this.mContext = context;
        this.scroll = scroll;
        this.parentView = parentView;
    }

    public View_Item_Data(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public void setOnDeleteDataListenter(OnDeleteDataListenter l) {
        this.onDeleteDataListenter = l;
    }

    public void setOnClickItemDataListenter(OnClickItemDataListenter l) {
        this.onClickItemDataListenter = l;
    }

    public void setData(BalanceDataMDL data, MemberMDL m) {
        this.member = m;
        this.data = data;
        this.tvDay.setText(ObjectHelper.Convert2String(data.getWeidate(), "yyyy/MM/dd"));
        boolean isHighPrecision = false;
        if (new AppConfigDAL(this.mContext).select(Constants.DEVICETYPE_STRING).equals("2")) {
            isHighPrecision = true;
        }
        this.tvWeightVal.setText(WeightUnitHelper.getConvertedWtVal(isHighPrecision, 2, data.getWeight()));
        this.tvRightVal.setText(ObjectHelper.Convert2MathCount(1, data.getBmi()));
        if (data.getClientImg() != null && data.getClientImg().length > 0) {
            this.imgStream = data.getClientImg();
            this.bitmap = ImageUtil.decodeSampledBitmapFromByte(this.imgStream, 96, 96);
            if (this.bitmap != null) {
                this.circleImageView.setVisibility(0);
                this.circleImageView.setImageBitmap(this.bitmap);
            }
        }
    }

    @Click({2131492988})
    void delete() {
        DialogHelper.showComfrimDialog(this.mContext, null, this.mContext.getString(C0181R.string.confirmDeletData), this.mContext.getString(C0181R.string.confirm), new C01131(), this.mContext.getString(C0181R.string.cancle), new C01142());
    }

    @AfterViews
    void init() {
        this.LineView.setBackgroundColor(Common.getThemeColor(this.mContext));
        this.btnDelete.setBackgroundColor(Common.getThemeColor(this.mContext));
        imgHeadInit();
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) this.rlFront.getLayoutParams();
        params.width = ((Activity) this.mContext).getWindowManager().getDefaultDisplay().getWidth();
        this.rlFront.setLayoutParams(params);
        this.llBack.getViewTreeObserver().addOnGlobalLayoutListener(new C01153());
        this.lockableHorizontalScrollView1.setOnCustomTouchListenter(new C02694());
    }

    private void imgHeadInit() {
        this.circleImageView.getViewTreeObserver().addOnGlobalLayoutListener(new C01165());
    }
}
