package com.vc.cloudbalance.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vc.cloudbalance.model.MemberMDL;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class View_MemberList_ extends View_MemberList implements HasViews, OnViewChangedListener {
    private boolean alreadyInflated_ = false;
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    class C01211 implements OnClickListener {
        C01211() {
        }

        public void onClick(View view) {
            View_MemberList_.this.enterGuestMode();
        }
    }

    class C01222 implements OnClickListener {
        C01222() {
        }

        public void onClick(View view) {
            View_MemberList_.this.showSettingActivity();
        }
    }

    public View_MemberList_(Context context) {
        super(context);
        init_();
    }

    public View_MemberList_(Context context, MemberMDL member) {
        super(context, member);
        init_();
    }

    public View_MemberList_(Context context, AttributeSet attrs) {
        super(context, attrs);
        init_();
    }

    public static View_MemberList build(Context context) {
        View_MemberList_ instance = new View_MemberList_(context);
        instance.onFinishInflate();
        return instance;
    }

    public void onFinishInflate() {
        if (!this.alreadyInflated_) {
            this.alreadyInflated_ = true;
            inflate(getContext(), C0181R.layout.view_memberlist, this);
            this.onViewChangedNotifier_.notifyViewChanged(this);
        }
        super.onFinishInflate();
    }

    private void init_() {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    public static View_MemberList build(Context context, MemberMDL member) {
        View_MemberList_ instance = new View_MemberList_(context, member);
        instance.onFinishInflate();
        return instance;
    }

    public static View_MemberList build(Context context, AttributeSet attrs) {
        View_MemberList_ instance = new View_MemberList_(context, attrs);
        instance.onFinishInflate();
        return instance;
    }

    public void onViewChanged(HasViews hasViews) {
        this.rlGuestMode = (RelativeLayout) hasViews.findViewById(C0181R.id.rlGuestMode);
        this.llNotMemberViews = (LinearLayout) hasViews.findViewById(C0181R.id.llNotMemberViews);
        this.llMemberList = (LinearLayout) hasViews.findViewById(C0181R.id.llMemberList);
        this.tvName = (TextView) hasViews.findViewById(C0181R.id.tvName);
        if (this.rlGuestMode != null) {
            this.rlGuestMode.setOnClickListener(new C01211());
        }
        View view = hasViews.findViewById(C0181R.id.rlAddMember);
        if (view != null) {
            view.setOnClickListener(new C01222());
        }
        init();
    }
}
