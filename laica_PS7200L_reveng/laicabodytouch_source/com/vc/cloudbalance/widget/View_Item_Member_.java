package com.vc.cloudbalance.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vc.cloudbalance.model.MemberMDL;
import com.whb.loease.bodytouch.C0181R;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class View_Item_Member_ extends View_Item_Member implements HasViews, OnViewChangedListener {
    private boolean alreadyInflated_ = false;
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    class C01191 implements OnClickListener {
        C01191() {
        }

        public void onClick(View view) {
            View_Item_Member_.this.showNewBalanceActivity();
        }
    }

    public View_Item_Member_(Context context) {
        super(context);
        init_();
    }

    public View_Item_Member_(Context context, MemberMDL member) {
        super(context, member);
        init_();
    }

    public View_Item_Member_(Context context, MemberMDL member, MemberMDL banlanceMember) {
        super(context, member, banlanceMember);
        init_();
    }

    public View_Item_Member_(Context context, AttributeSet attrs) {
        super(context, attrs);
        init_();
    }

    public static View_Item_Member build(Context context) {
        View_Item_Member_ instance = new View_Item_Member_(context);
        instance.onFinishInflate();
        return instance;
    }

    public void onFinishInflate() {
        if (!this.alreadyInflated_) {
            this.alreadyInflated_ = true;
            inflate(getContext(), C0181R.layout.view_item_member, this);
            this.onViewChangedNotifier_.notifyViewChanged(this);
        }
        super.onFinishInflate();
    }

    private void init_() {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    public static View_Item_Member build(Context context, MemberMDL member) {
        View_Item_Member_ instance = new View_Item_Member_(context, member);
        instance.onFinishInflate();
        return instance;
    }

    public static View_Item_Member build(Context context, MemberMDL member, MemberMDL banlanceMember) {
        View_Item_Member_ instance = new View_Item_Member_(context, member, banlanceMember);
        instance.onFinishInflate();
        return instance;
    }

    public static View_Item_Member build(Context context, AttributeSet attrs) {
        View_Item_Member_ instance = new View_Item_Member_(context, attrs);
        instance.onFinishInflate();
        return instance;
    }

    public void onViewChanged(HasViews hasViews) {
        this.tvName = (TextView) hasViews.findViewById(C0181R.id.tvName);
        this.rlItemMember = (RelativeLayout) hasViews.findViewById(C0181R.id.rlItemMember);
        if (this.rlItemMember != null) {
            this.rlItemMember.setOnClickListener(new C01191());
        }
        init();
    }
}
