package com.vc.cloudbalance.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.whb.loease.activity.DataViewActivity;
import com.whb.loease.bodytouch.C0181R;
import de.hdodenhof.circleimageview.CircleImageView;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class View_Item_Data_ extends View_Item_Data implements HasViews, OnViewChangedListener {
    private boolean alreadyInflated_ = false;
    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();

    class C01171 implements OnClickListener {
        C01171() {
        }

        public void onClick(View view) {
            View_Item_Data_.this.delete();
        }
    }

    public View_Item_Data_(Context context, ScrollViewExtend scroll, DataViewActivity parentView) {
        super(context, scroll, parentView);
        init_();
    }

    public View_Item_Data_(Context context, AttributeSet attrs) {
        super(context, attrs);
        init_();
    }

    public static View_Item_Data build(Context context, ScrollViewExtend scroll, DataViewActivity parentView) {
        View_Item_Data_ instance = new View_Item_Data_(context, scroll, parentView);
        instance.onFinishInflate();
        return instance;
    }

    public void onFinishInflate() {
        if (!this.alreadyInflated_) {
            this.alreadyInflated_ = true;
            inflate(getContext(), C0181R.layout.view_item_data, this);
            this.onViewChangedNotifier_.notifyViewChanged(this);
        }
        super.onFinishInflate();
    }

    private void init_() {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(this.onViewChangedNotifier_);
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    public static View_Item_Data build(Context context, AttributeSet attrs) {
        View_Item_Data_ instance = new View_Item_Data_(context, attrs);
        instance.onFinishInflate();
        return instance;
    }

    public void onViewChanged(HasViews hasViews) {
        this.rlFront = (RelativeLayout) hasViews.findViewById(C0181R.id.rlFront);
        this.llBack = (LinearLayout) hasViews.findViewById(C0181R.id.llBack);
        this.tvRightVal = (TextView) hasViews.findViewById(C0181R.id.tvRightVal);
        this.tvWeightVal = (TextView) hasViews.findViewById(C0181R.id.tvWeightVal);
        this.tvDate = (TextView) hasViews.findViewById(C0181R.id.tvDate);
        this.tvDay = (TextView) hasViews.findViewById(C0181R.id.tvDay);
        this.btnDelete = (Button) hasViews.findViewById(C0181R.id.btnDelete);
        this.lockableHorizontalScrollView1 = (LockableHorizontalScrollView) hasViews.findViewById(C0181R.id.lockableHorizontalScrollView1);
        this.circleImageView = (CircleImageView) hasViews.findViewById(C0181R.id.circleImageView);
        this.LineView = hasViews.findViewById(C0181R.id.LineView);
        if (this.btnDelete != null) {
            this.btnDelete.setOnClickListener(new C01171());
        }
        init();
    }
}
