package com.vc.cloudbalance.widget;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import com.vc.cloudbalance.common.Common;
import com.vc.cloudbalance.common.WeightUnitHelper;
import com.vc.cloudbalance.model.BalanceDataMDL;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.sqlite.BalanceDataDAL;
import com.vc.cloudbalance.widget.ScrollViewExtend.OnScrollViewBorderListener;
import com.vc.cloudbalance.widget.View_Item_Data.OnClickItemDataListenter;
import com.vc.cloudbalance.widget.View_Item_Data.OnDeleteDataListenter;
import com.vc.util.ObjectHelper;
import com.whb.loease.activity.DataViewActivity;
import com.whb.loease.bodytouch.C0181R;
import java.util.LinkedList;
import java.util.List;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup
public class View_DataList_Adult extends RelativeLayout {
    private int age = 0;
    List<BalanceDataMDL> balanceDatas;
    private int currentSelectedDataId;
    boolean finishLoadData = true;
    boolean isDelaySeconds = true;
    @ViewById
    LinearLayout llDataList;
    Context mContext;
    private MemberMDL member;
    int pageIndex = 1;
    View_Pager pager;
    DataViewActivity parentView;
    @ViewById
    ScrollViewExtend scrollView;
    private Handler setdataHandler = new C01101();
    @ViewById
    TableLayout tbDetail;
    @ViewById
    TextView tvBMIVal;
    @ViewById
    TextView tvBMRVal;
    @ViewById
    TextView tvBoneVal;
    @ViewById
    TextView tvFatVal;
    @ViewById
    TextView tvMuscleVal;
    @ViewById
    TextView tvWaterVal;
    @ViewById
    TextView tvWtUnit;

    class C01101 extends Handler {
        C01101() {
        }

        public void handleMessage(Message msg) {
            BalanceDataMDL mdl = msg.obj;
            if (mdl == null) {
                return;
            }
            if (ObjectHelper.Convert2Float(mdl.getFatpercent()) == 0.0f && ObjectHelper.Convert2Float(mdl.getMuscle()) == 0.0f) {
                View_DataList_Adult.this.tbDetail.setVisibility(8);
                return;
            }
            if (View_DataList_Adult.this.age >= 10) {
                View_DataList_Adult.this.tbDetail.setVisibility(0);
            }
            View_DataList_Adult.this.tvBMRVal.setText(ObjectHelper.Convert2String(mdl.getBasalmetabolism()));
            View_DataList_Adult.this.tvWaterVal.setText(ObjectHelper.Convert2String(mdl.getWater()));
            View_DataList_Adult.this.tvBoneVal.setText(WeightUnitHelper.getConvertedWtVal(1, mdl.getBone()) + WeightUnitHelper.getWeightUnitString());
            View_DataList_Adult.this.tvFatVal.setText(ObjectHelper.Convert2String(mdl.getFatpercent()));
            View_DataList_Adult.this.tvMuscleVal.setText(ObjectHelper.Convert2String(mdl.getMuscle()));
            View_DataList_Adult.this.tvBMIVal.setText(ObjectHelper.Convert2String(mdl.getBmi()));
        }
    }

    class C01112 implements OnClickListener {
        C01112() {
        }

        public void onClick(View v) {
            View_DataList_Adult.this.pager.setLoading();
            View_DataList_Adult view_DataList_Adult = View_DataList_Adult.this;
            view_DataList_Adult.pageIndex++;
            new LoadDataTask().execute(new Object[]{""});
        }
    }

    class LoadDataTask extends AsyncTask<Object, Object, List<View>> {

        class C02671 implements OnDeleteDataListenter {
            C02671() {
            }

            public void delete(String id) {
                View_DataList_Adult.this.loadData();
            }
        }

        class C02682 implements OnClickItemDataListenter {
            C02682() {
            }

            public void click(BalanceDataMDL mdl) {
                if (View_DataList_Adult.this.currentSelectedDataId != mdl.getId()) {
                    if (View_DataList_Adult.this.llDataList.findViewById(View_DataList_Adult.this.currentSelectedDataId) != null) {
                        View_DataList_Adult.this.llDataList.findViewById(View_DataList_Adult.this.currentSelectedDataId).setBackgroundColor(View_DataList_Adult.this.mContext.getResources().getColor(C0181R.color.nocolor));
                    }
                    View_DataList_Adult.this.currentSelectedDataId = mdl.getId();
                }
                View_DataList_Adult.this.llDataList.findViewById(mdl.getId()).setBackgroundColor(View_DataList_Adult.this.getResources().getColor(C0181R.color.lightgrey));
                if (View_DataList_Adult.this.age >= 10) {
                    View_DataList_Adult.this.setData(mdl);
                }
            }
        }

        LoadDataTask() {
        }

        protected void onPostExecute(List<View> result) {
            if (View_DataList_Adult.this.pageIndex == 1) {
                View_DataList_Adult.this.llDataList.removeAllViews();
            }
            View_DataList_Adult.this.llDataList.removeView(View_DataList_Adult.this.pager);
            for (View view : result) {
                View_DataList_Adult.this.llDataList.addView(view);
            }
            View_DataList_Adult.this.finishLoadData = true;
            if (result.size() == 10) {
                View_DataList_Adult.this.llDataList.addView(View_DataList_Adult.this.pager);
            }
            View_DataList_Adult.this.pager.setHideLoading();
            super.onPostExecute(result);
        }

        protected List<View> doInBackground(Object... params) {
            if (TextUtils.isEmpty(View_DataList_Adult.this.member.getMemberid())) {
                View_DataList_Adult.this.balanceDatas = new BalanceDataDAL(View_DataList_Adult.this.mContext).SelectByClientMemberId(View_DataList_Adult.this.member.getClientid(), View_DataList_Adult.this.pageIndex);
            } else {
                View_DataList_Adult.this.balanceDatas = new BalanceDataDAL(View_DataList_Adult.this.mContext).SelectByMemberId(View_DataList_Adult.this.member.getMemberid(), View_DataList_Adult.this.pageIndex);
            }
            List<View> views = new LinkedList();
            int count = View_DataList_Adult.this.balanceDatas.size();
            if (count > 20) {
                count = 20;
            }
            int i = 0;
            while (i < count) {
                View_Item_Data v = View_Item_Data_.build(View_DataList_Adult.this.mContext, View_DataList_Adult.this.scrollView, View_DataList_Adult.this.parentView);
                v.setOnDeleteDataListenter(new C02671());
                v.setOnClickItemDataListenter(new C02682());
                v.setId(((BalanceDataMDL) View_DataList_Adult.this.balanceDatas.get(i)).getId());
                v.setData((BalanceDataMDL) View_DataList_Adult.this.balanceDatas.get(i), View_DataList_Adult.this.member);
                if (View_DataList_Adult.this.pageIndex == 1 && i == 0) {
                    v.setBackgroundColor(View_DataList_Adult.this.getResources().getColor(C0181R.color.lightgrey));
                    View_DataList_Adult.this.setData((BalanceDataMDL) View_DataList_Adult.this.balanceDatas.get(i));
                    View_DataList_Adult.this.currentSelectedDataId = ((BalanceDataMDL) View_DataList_Adult.this.balanceDatas.get(i)).getId();
                }
                views.add(v);
                i++;
            }
            return views;
        }
    }

    class C02663 implements OnScrollViewBorderListener {

        class C01121 implements Runnable {
            C01121() {
            }

            public void run() {
                View_DataList_Adult.this.isDelaySeconds = false;
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                View_DataList_Adult.this.isDelaySeconds = true;
            }
        }

        C02663() {
        }

        public void onTop() {
        }

        public void onBottom() {
            if (View_DataList_Adult.this.finishLoadData) {
                View_DataList_Adult.this.finishLoadData = false;
                View_DataList_Adult view_DataList_Adult = View_DataList_Adult.this;
                view_DataList_Adult.pageIndex++;
                new Thread(new C01121()).start();
                new LoadDataTask().execute(new Object[]{""});
            }
        }
    }

    public View_DataList_Adult(Context context, MemberMDL m, DataViewActivity parentView) {
        super(context);
        initThemeView();
        this.mContext = context;
        this.member = m;
        this.parentView = parentView;
    }

    public View_DataList_Adult(Context context, AttributeSet attrs) {
        super(context, attrs);
        initThemeView();
        this.mContext = context;
    }

    private void initThemeView() {
        switch (Common.getThemeType()) {
            case 1:
                inflate(getContext(), C0181R.layout.view_datalist_adult, this);
                return;
            case 2:
                inflate(getContext(), C0181R.layout.view_datalist_adult_geentheme, this);
                return;
            case 4:
                inflate(getContext(), C0181R.layout.view_datalist_adult_greytheme, this);
                return;
            default:
                inflate(getContext(), C0181R.layout.view_datalist_adult_bluetheme, this);
                return;
        }
    }

    @AfterViews
    void init() {
        this.age = Common.GetAgeByBirthday(this.member.getBirthday());
        if (this.age < 10) {
            this.tbDetail.setVisibility(8);
        }
        this.tvWtUnit.setText("(" + WeightUnitHelper.getWeightUnitString() + ")");
        this.pager = View_Pager_.build(this.mContext);
        this.pager.setOnClickListener(new C01112());
        this.scrollView.setOnScrollViewBorderListener(new C02663());
    }

    public void removeData() {
        this.llDataList.removeAllViews();
    }

    public void loadData() {
        this.pageIndex = 1;
        new LoadDataTask().execute(new Object[]{""});
    }

    private void setData(BalanceDataMDL mdl) {
        Message msg = new Message();
        msg.obj = mdl;
        this.setdataHandler.sendMessage(msg);
    }

    public void setGestureDetector(GestureDetector gestureDetector) {
        this.scrollView.setGestureDetector(gestureDetector);
    }
}
