package com.vc.cloudbalance.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.vc.cloudbalance.common.Common;
import com.whb.loease.bodytouch.C0181R;

public class balanceCircleView extends View {
    private int currentWtVal = 0;
    private boolean isfirstDraw = true;
    private Thread mThread = new C01281();
    private Paint f4p = new Paint();
    private final int startAngle = 270;
    private int strokeThickWidth = getResources().getDimensionPixelSize(C0181R.dimen.banlancecircle_thickstrokewidth);
    private final int sweepAngle = 360;
    private int targetWtVal = 0;
    private float thickAngleDraw = 0.0f;
    private float thinArcAngleRate = 1.0f;
    private int wtCircleWidth = getResources().getDimensionPixelSize(C0181R.dimen.banlancecirclesize);

    class C01281 extends Thread {
        C01281() {
        }

        public void run() {
            float thickAngle = (1.0f - balanceCircleView.this.thinArcAngleRate) * 360.0f;
            balanceCircleView.this.thickAngleDraw = 1.0f;
            while (balanceCircleView.this.thickAngleDraw <= thickAngle) {
                Thread.currentThread();
                try {
                    balanceCircleView.this.postInvalidate();
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                balanceCircleView com_vc_cloudbalance_widget_balanceCircleView = balanceCircleView.this;
                com_vc_cloudbalance_widget_balanceCircleView.thickAngleDraw = com_vc_cloudbalance_widget_balanceCircleView.thickAngleDraw + 1.0f;
            }
        }
    }

    class C01292 extends Thread {
        C01292() {
        }

        public void run() {
            float thickAngle = (1.0f - balanceCircleView.this.thinArcAngleRate) * 360.0f;
            balanceCircleView.this.thickAngleDraw = 0.0f;
            while (balanceCircleView.this.thickAngleDraw < thickAngle) {
                Thread.currentThread();
                try {
                    balanceCircleView.this.postInvalidate();
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                balanceCircleView com_vc_cloudbalance_widget_balanceCircleView = balanceCircleView.this;
                com_vc_cloudbalance_widget_balanceCircleView.thickAngleDraw = com_vc_cloudbalance_widget_balanceCircleView.thickAngleDraw + 1.0f;
            }
        }
    }

    public balanceCircleView(Context context, float currentWtVal, float targetWtVal, int circleWith) {
        super(context);
        this.currentWtVal = (int) currentWtVal;
        this.targetWtVal = (int) targetWtVal;
        this.wtCircleWidth = circleWith;
        if (this.targetWtVal == 0) {
            this.thinArcAngleRate = 1.0f;
        } else {
            this.thinArcAngleRate = Math.abs(currentWtVal - targetWtVal) / targetWtVal;
        }
        this.f4p.setColor(Common.getThemeColor(context));
        this.f4p.setStyle(Style.STROKE);
        this.f4p.setAntiAlias(true);
    }

    public balanceCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setPaintColor(int color) {
        this.f4p.setColor(color);
    }

    private void drawThread() {
        new C01292().start();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.isfirstDraw) {
            drawThread();
            this.isfirstDraw = false;
        }
        int maxStrokeWidth = this.strokeThickWidth;
        int strokeThinWidth = getResources().getDimensionPixelSize(C0181R.dimen.banlancecircle_thinstrokewidth);
        RectF oval1 = new RectF((float) ((maxStrokeWidth / 2) + strokeThinWidth), (float) ((maxStrokeWidth / 2) + strokeThinWidth), (float) ((this.wtCircleWidth + (maxStrokeWidth / 2)) - strokeThinWidth), (float) ((this.wtCircleWidth + (maxStrokeWidth / 2)) - strokeThinWidth));
        this.f4p.setStrokeWidth((float) maxStrokeWidth);
        canvas.drawArc(oval1, 270.0f, this.thickAngleDraw, false, this.f4p);
    }

    public void invalidate(float currentWtVal, float targetWtVal) {
        this.currentWtVal = (int) currentWtVal;
        this.targetWtVal = (int) targetWtVal;
        if (this.targetWtVal == 0 || this.currentWtVal == 0) {
            this.thinArcAngleRate = 1.0f;
        } else {
            this.thinArcAngleRate = Math.abs(currentWtVal - targetWtVal) / targetWtVal;
        }
        drawThread();
    }
}
