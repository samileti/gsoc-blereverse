package com.vc.cloudbalance.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.vc.cloudbalance.common.Common;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.widget.View_MemberList.OnClickMemberListener;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(2130903074)
public class View_Item_Member extends LinearLayout {
    MemberMDL banlanceMember;
    boolean isLoad;
    private Context mContext;
    OnClickMemberListener mOnClickMemberListener;
    MemberMDL member;
    int offset_x = 0;
    @ViewById
    RelativeLayout rlItemMember;
    @ViewById
    TextView tvName;

    public View_Item_Member(Context context) {
        super(context);
        this.mContext = context;
    }

    public View_Item_Member(Context context, MemberMDL member) {
        super(context);
        this.mContext = context;
        this.member = member;
    }

    public View_Item_Member(Context context, MemberMDL member, MemberMDL banlanceMember) {
        super(context);
        this.mContext = context;
        this.member = member;
        this.banlanceMember = banlanceMember;
    }

    public View_Item_Member(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public void setOnClickMemberListener(OnClickMemberListener mOnClickMemberListener) {
        this.mOnClickMemberListener = mOnClickMemberListener;
    }

    @Click({2131493089})
    void showNewBalanceActivity() {
        if (this.mOnClickMemberListener != null) {
            this.mOnClickMemberListener.memberSelected(this.member);
        }
    }

    @AfterViews
    void init() {
        setMember(this.member);
    }

    public MemberMDL getMember() {
        return this.member;
    }

    public void setMember(MemberMDL member) {
        if (member != null) {
            this.tvName.setText(member.getMembername());
        }
    }

    public void drawSolidDot() {
        View SolidDot = new View(this.mContext) {
            protected void onDraw(Canvas canvas) {
                Paint p = new Paint();
                p.setColor(Common.getThemeColor(View_Item_Member.this.mContext));
                canvas.drawCircle(10.0f, 10.0f, 10.0f, p);
                p.setAntiAlias(true);
                super.onDraw(canvas);
            }
        };
        LayoutParams lParams = new LayoutParams(30, 30);
        lParams.setMargins(50, 0, 0, 0);
        lParams.addRule(5);
        lParams.addRule(15, -1);
        SolidDot.setLayoutParams(lParams);
        this.rlItemMember.addView(SolidDot);
    }
}
