package com.vc.cloudbalance.webservice;

import android.content.Context;
import com.vc.net.RequestParams;
import org.json.JSONObject;

public class UserWS extends BaseWS {
    public UserWS(Context context) {
        super(context);
    }

    public JSONObject registerUser(String userid, String username, String password) {
        try {
            String url = GetMethodURL("registerUser");
            RequestParams params = getParams();
            params.put("userid", (Object) userid);
            params.put("username", (Object) username);
            params.put("password", (Object) password);
            return getAsyncHttpClient().postToJson(url, params);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject UserLogin(String logintype, String parm1, String parm2) {
        try {
            String url = GetMethodURL("UserLogin");
            RequestParams params = getParams();
            params.put("logintype", (Object) logintype);
            params.put("parm1", (Object) parm1);
            params.put("parm2", (Object) parm2);
            return getAsyncHttpClient().postToJson(url, params);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject UserBang(String userid, String bangtype, String parm1, String parm2) {
        try {
            String url = GetMethodURL("UserBang");
            RequestParams params = getParams();
            params.put("userid", (Object) userid);
            params.put("bangtype", (Object) bangtype);
            params.put("parm1", (Object) parm1);
            params.put("parm2", (Object) parm2);
            return getAsyncHttpClient().postToJson(url, params);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject UserJieBang(String userid, String bangtype) {
        try {
            String url = GetMethodURL("UserJieBang");
            RequestParams params = getParams();
            params.put("userid", (Object) userid);
            params.put("bangtype", (Object) bangtype);
            return getAsyncHttpClient().postToJson(url, params);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject getFilever() {
        try {
            return getAsyncHttpClient().postToJson(GetMethodURL("getFilever"), getParams());
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject sendFeedBack(String userid, String feedBackStr) {
        try {
            String url = GetMethodURL("submitAdvice");
            RequestParams params = getParams();
            params.put("userid", (Object) userid);
            params.put("advice", (Object) feedBackStr);
            return getAsyncHttpClient().postToJson(url, params);
        } catch (Exception e) {
            return null;
        }
    }
}
