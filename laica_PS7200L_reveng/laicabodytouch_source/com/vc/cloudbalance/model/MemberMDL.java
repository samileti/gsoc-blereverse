package com.vc.cloudbalance.model;

public class MemberMDL {
    private String birthday;
    private String city;
    private byte[] clientImg;
    private String clientid;
    private boolean guest = false;
    private String height;
    private String hoby;
    private String iconfile;
    private String memberid;
    private String membername;
    private String modeltype;
    private String mood;
    private String sex;
    private String targetFinishTime;
    private String targetStartTime;
    private String targetweight;
    private int upload;
    private String userid;
    private String waist;

    public boolean isGuest() {
        return this.guest;
    }

    public void setGuest(boolean guest) {
        this.guest = guest;
    }

    public byte[] getClientImg() {
        return this.clientImg;
    }

    public void setClientImg(byte[] clientImg) {
        this.clientImg = clientImg;
    }

    public String getClientid() {
        return this.clientid;
    }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }

    public String getUserid() {
        return this.userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getMemberid() {
        return this.memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getMembername() {
        return this.membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public String getIconfile() {
        return this.iconfile;
    }

    public void setIconfile(String iconfile) {
        this.iconfile = iconfile;
    }

    public String getBirthday() {
        return this.birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getHeight() {
        return this.height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWaist() {
        return this.waist;
    }

    public void setWaist(String waist) {
        this.waist = waist;
    }

    public String getSex() {
        return this.sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTargetweight() {
        return this.targetweight;
    }

    public void setTargetweight(String targetweight) {
        this.targetweight = targetweight;
    }

    public String getModeltype() {
        return this.modeltype;
    }

    public String getHoby() {
        return this.hoby;
    }

    public void setHoby(String hoby) {
        this.hoby = hoby;
    }

    public void setMood(String mood) {
        this.mood = mood;
    }

    public String getMood() {
        return this.mood;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTargetFinishTime() {
        return this.targetFinishTime;
    }

    public void setTargetFinishTime(String targetFinishTime) {
        this.targetFinishTime = targetFinishTime;
    }

    public String getTargetStartTime() {
        return this.targetStartTime;
    }

    public void setTargetStartTime(String targetStartTime) {
        this.targetStartTime = targetStartTime;
    }

    public void setModeltype(String modeltype) {
        this.modeltype = modeltype;
    }

    public int getUpload() {
        return this.upload;
    }

    public void setUpload(int upload) {
        this.upload = upload;
    }
}
