package com.vc.cloudbalance.model;

import com.vc.cloudbalance.common.WeightUnitHelper;
import com.vc.util.ObjectHelper;
import java.util.Date;

public class BalanceDataMDL {
    private String babyGrowth;
    private String basalmetabolism;
    private String bmi;
    private String bone;
    private String changeHeight;
    private String changeWeight;
    private byte[] clientImg;
    private String clientmemberid;
    private String dataid;
    private String fatpercent;
    private String haveimg;
    private String height;
    private int id;
    private String innerfat;
    private String memberid;
    private String muscle;
    private String picurl;
    private int upload;
    private String userid;
    private String water;
    private String weidate;
    private String weight;

    public String getPicurl() {
        return this.picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

    public String getHaveimg() {
        return this.haveimg;
    }

    public void setHaveimg(String haveimg) {
        this.haveimg = haveimg;
    }

    public String getHeight() {
        return this.height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public byte[] getClientImg() {
        return this.clientImg;
    }

    public void setClientImg(byte[] clientImg) {
        this.clientImg = clientImg;
    }

    public String getVal(int Mode) {
        if (Mode == 4) {
            return getBone();
        }
        if (Mode == 2) {
            return getFatpercent();
        }
        if (Mode == 3) {
            return getMuscle();
        }
        if (Mode == 5) {
            return getWater();
        }
        if (Mode == 1) {
            return getWeight();
        }
        if (Mode == 6) {
            return getBmi();
        }
        if (Mode == 7) {
            return getHeight();
        }
        if (Mode == 8) {
            return getBasalmetabolism();
        }
        return WeightUnitHelper.Kg;
    }

    public String getDataid() {
        return this.dataid;
    }

    public void setDataid(String dataid) {
        this.dataid = dataid;
    }

    public String getClientmemberid() {
        return this.clientmemberid;
    }

    public void setClientmemberid(String clientmemberid) {
        this.clientmemberid = clientmemberid;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMemberid() {
        return this.memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public String getUserid() {
        return this.userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Date getWeidate() {
        return ObjectHelper.Convert2Date(this.weidate, "yyyy-MM-dd HH:mm:ss");
    }

    public String getWeidateString() {
        return this.weidate;
    }

    public void setWeidate(Date weidate) {
        this.weidate = ObjectHelper.Convert2String(weidate, "yyyy-MM-dd HH:mm:ss");
    }

    public void setWeidateString(String weidate) {
        this.weidate = weidate;
    }

    public String getWeight() {
        return this.weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBmi() {
        return this.bmi;
    }

    public void setBmi(String bmi) {
        this.bmi = bmi;
    }

    public String getFatpercent() {
        return this.fatpercent;
    }

    public void setFatpercent(String fatpercent) {
        this.fatpercent = fatpercent;
    }

    public String getMuscle() {
        return this.muscle;
    }

    public void setMuscle(String muscle) {
        this.muscle = muscle;
    }

    public String getBone() {
        return this.bone;
    }

    public void setBone(String bone) {
        this.bone = bone;
    }

    public String getWater() {
        return this.water;
    }

    public void setWater(String water) {
        this.water = water;
    }

    public String getBasalmetabolism() {
        return this.basalmetabolism;
    }

    public void setBasalmetabolism(String basalmetabolism) {
        this.basalmetabolism = basalmetabolism;
    }

    public String getInnerfat() {
        return this.innerfat;
    }

    public void setInnerfat(String innerfat) {
        this.innerfat = innerfat;
    }

    public int getUpload() {
        return this.upload;
    }

    public void setUpload(int upload) {
        this.upload = upload;
    }

    public String getBabyGrowth() {
        return this.babyGrowth;
    }

    public void setBabyGrowth(String babyGrowth) {
        this.babyGrowth = babyGrowth;
    }

    public String getchangeHeight() {
        return this.changeHeight;
    }

    public void setchangeHeight(String changeHeight) {
        this.changeHeight = changeHeight;
    }

    public String getchangeWeight() {
        return this.changeWeight;
    }

    public void setchangeWeight(String changeWeight) {
        this.changeWeight = changeWeight;
    }
}
