package com.vc.cloudbalance.model;

public class UserMDL {
    private String qqaccesstoken;
    private String qqname;
    private String qqopenid;
    private String userid;
    private String username;
    private String wbaccesstoken;
    private String wbname;
    private String wbopenid;

    public String getUserid() {
        return this.userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getQqopenid() {
        return this.qqopenid;
    }

    public void setQqopenid(String qqopenid) {
        this.qqopenid = qqopenid;
    }

    public String getQqname() {
        return this.qqname;
    }

    public void setQqname(String qqname) {
        this.qqname = qqname;
    }

    public String getWbopenid() {
        return this.wbopenid;
    }

    public void setWbopenid(String wbopenid) {
        this.wbopenid = wbopenid;
    }

    public String getQqaccesstoken() {
        return this.qqaccesstoken;
    }

    public void setQqaccesstoken(String qqaccesstoken) {
        this.qqaccesstoken = qqaccesstoken;
    }

    public String getWbaccesstoken() {
        return this.wbaccesstoken;
    }

    public void setWbaccesstoken(String wbaccesstoken) {
        this.wbaccesstoken = wbaccesstoken;
    }

    public String getWbname() {
        return this.wbname;
    }

    public void setWbname(String wbname) {
        this.wbname = wbname;
    }
}
