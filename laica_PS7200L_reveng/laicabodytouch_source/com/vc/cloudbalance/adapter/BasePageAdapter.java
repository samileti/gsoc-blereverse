package com.vc.cloudbalance.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

public class BasePageAdapter extends PagerAdapter {
    private Context mContext;
    private List<View> mList;

    public BasePageAdapter(Context c, List<View> vs) {
        this.mContext = c;
        this.mList = vs;
    }

    public int getCount() {
        if (this.mList.size() <= 3) {
            return this.mList.size();
        }
        return Integer.MAX_VALUE;
    }

    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    public void finishUpdate(ViewGroup container) {
    }

    public List<View> getDatas() {
        return this.mList;
    }

    public void destroyItem(View arg0, int arg1, Object arg2) {
    }

    public void finishUpdate(View arg0) {
    }

    public Object instantiateItem(View arg0, int arg1) {
        try {
            if (((View) this.mList.get(arg1 % this.mList.size())).getParent() == null) {
                ((ViewPager) arg0).addView((View) this.mList.get(arg1 % this.mList.size()));
            }
        } catch (Exception e) {
        }
        return this.mList.get(arg1 % this.mList.size());
    }

    public void restoreState(Parcelable arg0, ClassLoader arg1) {
    }

    public Parcelable saveState() {
        return null;
    }

    public void startUpdate(View arg0) {
    }
}
