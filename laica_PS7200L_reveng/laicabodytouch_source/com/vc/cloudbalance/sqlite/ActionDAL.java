package com.vc.cloudbalance.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.vc.cloudbalance.common.App;
import com.vc.cloudbalance.common.DatabaseHelper;
import com.vc.cloudbalance.model.ActionMDL;
import com.vc.util.LogUtils;
import java.util.LinkedList;
import java.util.List;

public class ActionDAL {
    Context context;
    SQLiteDatabase mDb = null;
    DatabaseHelper mDbHelper = null;
    String selectPara = " id,action,data,locked";

    public ActionDAL(Context c) {
        this.context = c;
        this.mDbHelper = DatabaseHelper.getInstance(c);
        this.mDb = this.mDbHelper.getDatabase();
    }

    public List<ActionMDL> SelectByAction(String action) {
        try {
            List<ActionMDL> datas;
            synchronized (App.threadDBLock) {
                String sql = "select  " + this.selectPara + " from Action where action=? and locked=0";
                Cursor cursor = this.mDb.rawQuery(sql, new String[]{action});
                datas = new LinkedList();
                while (cursor.moveToNext()) {
                    datas.add(convert(cursor));
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return null;
        }
    }

    public List<ActionMDL> Select() {
        try {
            List<ActionMDL> datas;
            synchronized (App.threadDBLock) {
                Cursor cursor = this.mDb.rawQuery("select  " + this.selectPara + " from Action where locked=0 ", new String[0]);
                datas = new LinkedList();
                while (cursor.moveToNext()) {
                    datas.add(convert(cursor));
                }
                cursor.close();
            }
            return datas;
        } catch (Exception e) {
            return null;
        }
    }

    public ActionMDL Select(String action, String data) {
        try {
            ActionMDL i;
            synchronized (App.threadDBLock) {
                String sql = "select  " + this.selectPara + " from Action where action=? and data=? and  locked=0 ";
                Cursor cursor = this.mDb.rawQuery(sql, new String[]{action, data});
                i = null;
                if (cursor.moveToNext()) {
                    i = convert(cursor);
                }
                cursor.close();
            }
            return i;
        } catch (Exception e) {
            return null;
        }
    }

    private ActionMDL convert(Cursor cursor) {
        ActionMDL i = new ActionMDL();
        try {
            i.setId(cursor.getString(0));
            i.setAction(cursor.getString(1));
            i.setData(cursor.getString(2));
        } catch (Exception e) {
        }
        return i;
    }

    public boolean Locked(String id, int lock) {
        try {
            String sql = "update Action set locked=? where id=?";
            try {
                this.mDb.execSQL(sql, new Object[]{Integer.valueOf(lock), id});
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }

    public boolean Del(String id) {
        try {
            this.mDb.execSQL("delete from Action where id=" + id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean Insert(ActionMDL data) {
        try {
            String sql = "insert into Action (" + this.selectPara + ") values (?,?,?,0)";
            try {
                this.mDb.execSQL(sql, new Object[]{data.getId(), data.getAction(), data.getData()});
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }
}
