package com.vc.cloudbalance.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.vc.cloudbalance.common.App;
import com.vc.cloudbalance.common.DatabaseHelper;
import com.vc.cloudbalance.model.UserMDL;
import com.vc.util.LogUtils;

public class UserDAL {
    Context context;
    SQLiteDatabase mDb = null;
    DatabaseHelper mDbHelper = null;
    String selectPara = " userid,username,qqopenid,qqtoken,qqname,wbopenid,wbtoken,wbname ";

    public UserDAL(Context c) {
        this.context = c;
        this.mDbHelper = DatabaseHelper.getInstance(c);
        this.mDb = this.mDbHelper.getDatabase();
    }

    public UserMDL Select() {
        try {
            UserMDL i;
            synchronized (App.threadDBLock) {
                Cursor cursor = this.mDb.rawQuery("select " + this.selectPara + " from User limit 1 ", new String[0]);
                i = null;
                if (cursor.moveToNext()) {
                    i = convert(cursor);
                }
                cursor.close();
            }
            return i;
        } catch (Exception e) {
            return null;
        }
    }

    private UserMDL convert(Cursor cursor) {
        UserMDL i = new UserMDL();
        try {
            i.setUserid(cursor.getString(0));
            i.setUsername(cursor.getString(1));
            i.setQqopenid(cursor.getString(2));
            i.setQqaccesstoken(cursor.getString(3));
            i.setQqname(cursor.getString(4));
            i.setWbopenid(cursor.getString(5));
            i.setWbaccesstoken(cursor.getString(6));
            i.setWbname(cursor.getString(7));
        } catch (Exception e) {
        }
        return i;
    }

    public boolean Del() {
        try {
            synchronized (App.threadDBLock) {
                try {
                    this.mDb.execSQL("delete from User");
                } catch (Exception e) {
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }

    public boolean Insert(UserMDL data) {
        try {
            synchronized (App.threadDBLock) {
                String sql = "insert into User (" + this.selectPara + ") values (?,?,?,?,?,?,?,?)";
                this.mDb.beginTransaction();
                try {
                    this.mDb.execSQL("delete from User");
                    this.mDb.execSQL(sql, new Object[]{data.getUserid(), data.getUsername(), data.getQqopenid(), data.getQqaccesstoken(), data.getQqname(), data.getWbopenid(), data.getWbaccesstoken(), data.getWbname()});
                    this.mDb.setTransactionSuccessful();
                    this.mDb.endTransaction();
                } catch (Exception e) {
                    this.mDb.endTransaction();
                    return false;
                } catch (Throwable th) {
                    this.mDb.endTransaction();
                }
            }
            return true;
        } catch (Exception e2) {
            LogUtils.m3e(e2.toString());
            return false;
        }
    }
}
