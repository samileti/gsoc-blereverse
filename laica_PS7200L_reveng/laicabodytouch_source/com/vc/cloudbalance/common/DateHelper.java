package com.vc.cloudbalance.common;

import com.whb.loease.bodytouch.C0181R;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateHelper {
    public static List<Date> dateToWeek(Date mdate, int index) {
        int b = mdate.getDay();
        List<Date> list = new ArrayList();
        Long fTime = Long.valueOf(mdate.getTime() - ((long) ((b * 24) * 3600000)));
        for (int a = 1; a <= 7; a++) {
            Date fdate = new Date();
            fdate.setTime(fTime.longValue() + ((long) ((a * 24) * 3600000)));
            Calendar cal = Calendar.getInstance();
            cal.setTime(fdate);
            if (index != 0) {
                cal.add(6, index * 7);
            }
            list.add(a - 1, cal.getTime());
        }
        return list;
    }

    public static List<Date> dateToMonth(Date mdate, int index) {
        List<Date> list = new ArrayList();
        Calendar cal = Calendar.getInstance();
        cal.setTime(mdate);
        cal.add(2, index);
        cal.add(2, 1);
        cal.set(5, 1);
        cal.add(5, -1);
        int lastDay = cal.getTime().getDate();
        cal.set(5, 1);
        for (int i = cal.getTime().getDate(); i <= lastDay; i++) {
            Calendar c = Calendar.getInstance();
            c.set(cal.get(1), cal.get(2), i);
            list.add(c.getTime());
        }
        return list;
    }

    public static List<Date> dateToSeason(Date mdate, int index) {
        List<Date> list = new ArrayList();
        Calendar cal = Calendar.getInstance();
        cal.setTime(mdate);
        cal.add(2, index * 3);
        int month = cal.get(2) + 1;
        if (month == 1 || month == 2 || month == 3) {
            cal.set(2, 0);
        } else if (month == 4 || month == 5 || month == 6) {
            cal.set(2, 3);
        } else if (month == 7 || month == 8 || month == 9) {
            cal.set(2, 6);
        } else if (month == 10 || month == 11 || month == 12) {
            cal.set(2, 9);
        }
        for (int j = 0; j < 3; j++) {
            cal.add(2, 1);
            cal.set(5, 1);
            cal.add(5, -1);
            int lastDay = cal.getTime().getDate();
            for (int i = 1; i <= lastDay; i++) {
                Calendar c = Calendar.getInstance();
                c.set(cal.get(1), cal.get(2), i);
                list.add(c.getTime());
            }
            cal.add(2, 1);
        }
        return list;
    }

    public static List<Date> dateToYear(Date mdate, int index) {
        List<Date> list = new ArrayList();
        Calendar cal = Calendar.getInstance();
        cal.setTime(mdate);
        cal.add(1, index);
        for (int i = 0; i <= 11; i++) {
            Calendar c = Calendar.getInstance();
            c.set(cal.get(1), i, 1);
            int maxdaymonth = c.getActualMaximum(5);
            for (int j = 1; j <= maxdaymonth; j++) {
                Calendar cj = Calendar.getInstance();
                c.set(cal.get(1), i, j);
                list.add(c.getTime());
            }
        }
        return list;
    }

    public static int getDayYear(int year, int month, int day) {
        int sum = 0;
        for (int i = 1; i < month; i++) {
            switch (i) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    sum += 31;
                    break;
                case 2:
                    int i2;
                    if (year % 4 == 0) {
                        i2 = 1;
                    } else {
                        i2 = 0;
                    }
                    int i3 = (year % 100 != 0 ? 1 : 0) & i2;
                    if (year % 400 == 0) {
                        i2 = 1;
                    } else {
                        i2 = 0;
                    }
                    if ((i2 | i3) == 0) {
                        sum += 28;
                        break;
                    }
                    sum += 29;
                    break;
                case 4:
                case 6:
                case 9:
                case C0181R.styleable.SwipeListView_swipeDrawableUnchecked /*11*/:
                    sum += 30;
                    break;
                default:
                    break;
            }
        }
        return sum + day;
    }
}
