package com.vc.cloudbalance.common;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import java.io.File;
import java.io.IOException;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String Action_CREATE = "CREATE TABLE [Action] ([id] NVARCHAR2,[action] NVARCHAR2, [data] NVARCHAR2,[locked] INTEGER);";
    private static final String AppConfig_CREATE = "CREATE TABLE [AppConfig] ([KeyName] NVARCHAR2, [Value] NVARCHAR2);";
    private static final String BalanceData_CREATE = "CREATE TABLE [BalanceData] ([id] integer PRIMARY KEY autoincrement,[memberid] NVARCHAR2, [userid] NVARCHAR2,[weidate] NVARCHAR2,[weight] NVARCHAR2,[bmi] NVARCHAR2,[fatpercent] NVARCHAR2,[muscle] NVARCHAR2,[bone] NVARCHAR2,[water] NVARCHAR2,[basalmetabolism] NVARCHAR2,[innerfat] NVARCHAR2,[upload] INTEGER,[clientmemberid] NVARCHAR2,[dataid] NVARCHAR2,[clientImg] BLOB,[height] NVARCHAR2,[haveimg] NVARCHAR2,[imgurl] NVARCHAR2,[babygrowth] NVARCHAR2,[changeHeight] NVARCHAR2,[changeWeight] NVARCHAR2);";
    public static final String DATABASE_NAME = "bodytouch";
    public static final int DATABASE_VERSION = 1;
    private static final String Member_CREATE = "CREATE TABLE [Member] ([clientid] NVARCHAR2,[memberid] NVARCHAR2, [membername] NVARCHAR2,[iconfile] NVARCHAR2,[birthday] NVARCHAR2,[height] NVARCHAR2,[waist] NVARCHAR2,[sex] NVARCHAR2,[targetweight] NVARCHAR2,[modeltype] NVARCHAR2,[userid] NVARCHAR2,[upload] INTEGER,[clientImg] BLOB,[targetStartTime] NVARCHAR2,[targetFinishTime] NVARCHAR2,[mood] NVARCHAR2);";
    private static final String User_CREATE = "CREATE TABLE [User] ([userid] NVARCHAR2, [username] NVARCHAR2,[qqopenid] NVARCHAR2,[qqtoken] NVARCHAR2,[qqname] NVARCHAR2,[wbopenid] NVARCHAR2,[wbtoken] NVARCHAR2,[wbname] NVARCHAR2);";
    private static DatabaseHelper instance;
    private static SQLiteDatabase mDB;
    private File f3f = new File(this.filePath + "bodytouch.wd");
    String filePath = (Environment.getExternalStorageDirectory() + "/.LOEASE/.bodytouch/.database/");
    private File path = new File(this.filePath);

    public static synchronized DatabaseHelper getInstance(Context c) {
        DatabaseHelper databaseHelper;
        synchronized (DatabaseHelper.class) {
            if (c == null) {
                c = App.getInstance();
            }
            if (instance == null) {
                instance = new DatabaseHelper(c);
            }
            databaseHelper = instance;
        }
        return databaseHelper;
    }

    public synchronized SQLiteDatabase getDatabase() {
        if (mDB == null) {
            if (!this.path.exists()) {
                this.path.mkdirs();
            }
            if (this.f3f.exists()) {
                mDB = SQLiteDatabase.openOrCreateDatabase(this.f3f, null);
            } else {
                try {
                    this.f3f.createNewFile();
                    mDB = SQLiteDatabase.openOrCreateDatabase(this.f3f, null);
                    mDB.execSQL(AppConfig_CREATE);
                    mDB.execSQL(User_CREATE);
                    mDB.execSQL(Member_CREATE);
                    mDB.execSQL(Action_CREATE);
                    mDB.execSQL(BalanceData_CREATE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return mDB;
    }

    public DatabaseHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DatabaseHelper(Context c) {
        super(c, DATABASE_NAME, null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(AppConfig_CREATE);
        db.execSQL(User_CREATE);
        db.execSQL(Member_CREATE);
        db.execSQL(Action_CREATE);
        db.execSQL(BalanceData_CREATE);
        init(db);
    }

    private void init(SQLiteDatabase db) {
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
