package com.vc.cloudbalance.common;

import android.app.Application;
import android.content.Context;
import com.vc.cloudbalance.model.MemberMDL;
import com.vc.cloudbalance.model.UserMDL;
import com.vc.cloudbalance.sqlite.UserDAL;
import java.util.Calendar;

public class App extends Application {
    private static App instance;
    public static String packageName = "data/data/com.whb.loease.bodytouch";
    public static Object threadDBLock = "";
    private UserMDL User;
    private boolean firstLoad = true;

    public boolean isFirstLoad() {
        return this.firstLoad;
    }

    public static MemberMDL guestMember() {
        MemberMDL member = new MemberMDL();
        member.setGuest(true);
        member.setSex("1");
        member.setBirthday(new StringBuilder(String.valueOf(Calendar.getInstance().get(1) - 25)).append("-").append(1).append("-").append(1).toString());
        member.setHeight("170");
        return member;
    }

    public void setFirstLoad(boolean firstLoad) {
        this.firstLoad = firstLoad;
    }

    public UserMDL getUser() {
        if (this.User == null) {
            this.User = new UserDAL(getApplicationContext()).Select();
        }
        return this.User;
    }

    public void setUser(UserMDL user) {
        this.User = user;
    }

    public static App getApp(Context mContext) {
        return (App) mContext.getApplicationContext();
    }

    public static App getInstance() {
        return instance;
    }

    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
