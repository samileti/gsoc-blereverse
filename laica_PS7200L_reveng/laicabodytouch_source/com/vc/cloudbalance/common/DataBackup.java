package com.vc.cloudbalance.common;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

public class DataBackup {
    private String APP_PATH = App.packageName;
    private String BACKUP_DATABASES;
    private String BACKUP_PATH;
    private String BACKUP_SHARED_PREFS;
    private String DATABASES = (this.APP_PATH + "/databases");
    private String SHARED_PREFS = (this.APP_PATH + "/shared_prefs");
    private Context mContext;

    public DataBackup(Context context) {
        this.mContext = context;
        if ("mounted".equals(Environment.getExternalStorageState())) {
            this.BACKUP_PATH = "/sdcard/LOEASE/backup";
        } else {
            this.BACKUP_PATH = "/LOEASE/backup/";
        }
        this.BACKUP_DATABASES = this.BACKUP_PATH + "/database";
        this.BACKUP_SHARED_PREFS = this.BACKUP_PATH + "/shared_prefs";
    }

    public boolean doBackup() {
        return backupDB() && backupSharePrefs();
    }

    private boolean backupDB() {
        return copyDir(this.DATABASES, this.BACKUP_DATABASES, "备份数据库文件成功:" + this.BACKUP_DATABASES, "备份数据库文件失败");
    }

    private boolean backupSharePrefs() {
        return copyDir(this.DATABASES, this.BACKUP_DATABASES, "备份配置文件成功:" + this.BACKUP_SHARED_PREFS, "备份配置文件失败");
    }

    public boolean doRestore() {
        return restoreDB() && restoreSharePrefs();
    }

    private boolean restoreDB() {
        return copyDir(this.BACKUP_DATABASES, this.DATABASES, "恢复数据库文件成功", "恢复数据库文件失败");
    }

    private boolean restoreSharePrefs() {
        return copyDir(this.BACKUP_SHARED_PREFS, this.SHARED_PREFS, "恢复配置文件成功", "恢复配置文件失败");
    }

    private final void showToast(String msg) {
        Toast.makeText(this.mContext, msg, 0).show();
    }

    private final boolean copyDir(String srcDir, String destDir, String successMsg, String failedMsg) {
        copyFolder(srcDir, destDir);
        showToast(successMsg);
        return true;
    }

    public void copyFile(String oldPath, String newPath) {
        int bytesum = 0;
        try {
            File oldfile = new File(oldPath);
            if (oldfile.exists() && oldfile.isFile() && oldfile.canRead() && oldfile.exists()) {
                InputStream inStream = new FileInputStream(oldPath);
                FileOutputStream fs = new FileOutputStream(newPath);
                byte[] buffer = new byte[1444];
                while (true) {
                    int byteread = inStream.read(buffer);
                    if (byteread == -1) {
                        inStream.close();
                        return;
                    }
                    bytesum += byteread;
                    System.out.println(bytesum);
                    fs.write(buffer, 0, byteread);
                }
            }
        } catch (Exception e) {
            System.out.println("复制单个文件操作出错");
            e.printStackTrace();
        }
    }

    public void copyFolder(String oldPath, String newPath) {
        try {
            new File(newPath).mkdirs();
            String[] file = new File(oldPath).list();
            for (int i = 0; i < file.length; i++) {
                File temp;
                if (oldPath.endsWith(File.separator)) {
                    temp = new File(new StringBuilder(String.valueOf(oldPath)).append(file[i]).toString());
                } else {
                    temp = new File(new StringBuilder(String.valueOf(oldPath)).append(File.separator).append(file[i]).toString());
                }
                if (temp.isFile()) {
                    FileInputStream input = new FileInputStream(temp);
                    FileOutputStream output = new FileOutputStream(new StringBuilder(String.valueOf(newPath)).append("/").append(temp.getName().toString()).toString());
                    byte[] b = new byte[5120];
                    while (true) {
                        int len = input.read(b);
                        if (len == -1) {
                            break;
                        }
                        output.write(b, 0, len);
                    }
                    output.flush();
                    output.close();
                    input.close();
                }
                if (temp.isDirectory()) {
                    copyFolder(new StringBuilder(String.valueOf(oldPath)).append("/").append(file[i]).toString(), new StringBuilder(String.valueOf(newPath)).append("/").append(file[i]).toString());
                }
            }
        } catch (Exception e) {
            System.out.println("复制整个文件夹内容操作出错");
            e.printStackTrace();
        }
    }
}
