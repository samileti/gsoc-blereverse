package com.vc.dialog;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import java.util.List;

public class ListViewWindow {
    String[] arrayString;
    Button btn_cancel;
    String cancel;
    OnItemClickListener clickListener;
    List<String> datas;
    Dialog dialog;
    ListView lvHP;
    private Context mContext;
    private OnSelectListener onSelectListener;
    String title;

    class C01301 implements OnClickListener {
        C01301() {
        }

        public void onClick(DialogInterface dialog, int which) {
            if (ListViewWindow.this.onSelectListener != null) {
                ListViewWindow.this.onSelectListener.onSelect(dialog, which);
            }
        }
    }

    class C01312 implements OnClickListener {
        C01312() {
        }

        public void onClick(DialogInterface dialog, int which) {
        }
    }

    public interface OnSelectListener {
        void onSelect(DialogInterface dialogInterface, int i);
    }

    public Dialog GetDialog() {
        return this.dialog;
    }

    public ListViewWindow(Context context, OnSelectListener clickListener, List<String> items, String title, String cancel) {
        this.mContext = context;
        this.datas = items;
        this.arrayString = new String[items.size()];
        this.arrayString = (String[]) items.toArray(this.arrayString);
        this.onSelectListener = clickListener;
        this.title = title;
        this.cancel = cancel;
        init();
    }

    public ListViewWindow(Context context, OnSelectListener clickListener, List<String> items, String title) {
        this.mContext = context;
        this.datas = items;
        this.arrayString = new String[items.size()];
        this.arrayString = (String[]) items.toArray(this.arrayString);
        this.onSelectListener = clickListener;
        this.title = title;
        this.cancel = "取锟斤拷";
        init();
    }

    public ListViewWindow(Context context, OnSelectListener clickListener, String[] items, String title) {
        this.mContext = context;
        this.arrayString = items;
        this.onSelectListener = clickListener;
        this.title = title;
        init();
    }

    public ListViewWindow(Context context, OnSelectListener clickListener, String[] items, String title, String cancel) {
        this.mContext = context;
        this.arrayString = items;
        this.onSelectListener = clickListener;
        this.title = title;
        this.cancel = cancel;
        init();
    }

    private void init() {
        this.dialog = new Builder(this.mContext).setTitle(this.title).setItems(this.arrayString, new C01301()).setNegativeButton(this.cancel, new C01312()).create();
    }

    public void setOnSelectListener(OnSelectListener l) {
        this.onSelectListener = l;
    }

    public void show() {
        this.dialog.show();
    }

    public void dismiss() {
        this.dialog.dismiss();
    }
}
