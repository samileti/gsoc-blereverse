package com.vc.image;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class SimpleHttpDownloader implements Downloader {
    private static final int IO_BUFFER_SIZE = 8192;
    private static final String TAG = "BitmapDownloader";

    public class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0;
            while (totalBytesSkipped < n) {
                long bytesSkipped = this.in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0) {
                    if (read() < 0) {
                        break;
                    }
                    bytesSkipped = 1;
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

    public boolean downloadToLocalStreamByUrl(String urlString, OutputStream outputStream) {
        IOException e;
        Throwable th;
        HttpURLConnection urlConnection = null;
        BufferedOutputStream out = null;
        FlushedInputStream in = null;
        Log.e("downloadToLocalStreamByUrl", urlString);
        try {
            urlConnection = (HttpURLConnection) new URL(urlString).openConnection();
            FlushedInputStream in2 = new FlushedInputStream(new BufferedInputStream(urlConnection.getInputStream(), 8192));
            try {
                BufferedOutputStream out2 = new BufferedOutputStream(outputStream, 8192);
                while (true) {
                    try {
                        int b = in2.read();
                        if (b == -1) {
                            break;
                        }
                        out2.write(b);
                    } catch (IOException e2) {
                        e = e2;
                        in = in2;
                        out = out2;
                    } catch (Throwable th2) {
                        th = th2;
                        in = in2;
                        out = out2;
                    }
                }
                Log.e("downloadToLocalStreamByUrl2", urlString);
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (out2 != null) {
                    try {
                        out2.close();
                    } catch (IOException e3) {
                    }
                }
                if (in2 != null) {
                    in2.close();
                }
                in = in2;
                out = out2;
                return true;
            } catch (IOException e4) {
                e = e4;
                in = in2;
                try {
                    Log.e(TAG, "Error in downloadBitmap - " + urlString + " : " + e);
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e5) {
                            return false;
                        }
                    }
                    if (in != null) {
                        in.close();
                    }
                    return false;
                } catch (Throwable th3) {
                    th = th3;
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e6) {
                            throw th;
                        }
                    }
                    if (in != null) {
                        in.close();
                    }
                    throw th;
                }
            } catch (Throwable th4) {
                th = th4;
                in = in2;
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
                throw th;
            }
        } catch (IOException e7) {
            e = e7;
            Log.e(TAG, "Error in downloadBitmap - " + urlString + " : " + e);
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (out != null) {
                out.close();
            }
            if (in != null) {
                in.close();
            }
            return false;
        }
    }
}
