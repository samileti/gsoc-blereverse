package com.vc.image;

import android.graphics.Bitmap;
import android.util.Log;
import com.vc.image.LruDiskCache.Editor;
import com.vc.image.LruDiskCache.Snapshot;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;

public class BitmapProcess {
    private static final int DEFAULT_CACHE_SIZE = 20971520;
    private static final int DISK_CACHE_INDEX = 0;
    private static final String TAG = "BitmapProcess";
    private int cacheSize;
    private Downloader downloader;
    private final Object mHttpDiskCacheLock = new Object();
    private boolean mHttpDiskCacheStarting = true;
    private File mOriginalCacheDir;
    private LruDiskCache mOriginalDiskCache;
    private boolean neverCalculate = false;

    public BitmapProcess(Downloader downloader, String filePath, int cacheSize) {
        this.mOriginalCacheDir = new File(new StringBuilder(String.valueOf(filePath)).append("/original").toString());
        this.downloader = downloader;
        if (cacheSize <= 0) {
            cacheSize = DEFAULT_CACHE_SIZE;
        }
        this.cacheSize = cacheSize;
    }

    public void configCalculateBitmap(boolean neverCalculate) {
        this.neverCalculate = neverCalculate;
    }

    public Bitmap processBitmap(String data, BitmapDisplayConfig config, boolean calculate) {
        Log.e("processBitmap", data);
        String key = FileNameGenerator.generator(data);
        FileDescriptor fileDescriptor = null;
        FileInputStream fileInputStream = null;
        synchronized (this.mHttpDiskCacheLock) {
            while (this.mHttpDiskCacheStarting) {
                try {
                    this.mHttpDiskCacheLock.wait();
                } catch (InterruptedException e) {
                }
            }
            if (this.mOriginalDiskCache != null) {
                try {
                    Snapshot snapshot = this.mOriginalDiskCache.get(key);
                    if (config.getIsUseCache()) {
                        snapshot = null;
                    }
                    if (snapshot == null) {
                        Editor editor = this.mOriginalDiskCache.edit(key);
                        if (editor != null) {
                            if (this.downloader.downloadToLocalStreamByUrl(data, editor.newOutputStream(0))) {
                                editor.commit();
                            } else {
                                editor.abort();
                            }
                        }
                        snapshot = this.mOriginalDiskCache.get(key);
                    }
                    if (snapshot != null) {
                        fileInputStream = (FileInputStream) snapshot.getInputStream(0);
                        fileDescriptor = fileInputStream.getFD();
                    }
                    if (fileDescriptor == null && fileInputStream != null) {
                        try {
                            fileInputStream.close();
                        } catch (IOException e2) {
                        }
                    }
                } catch (IOException e3) {
                    Log.e(TAG, "processBitmap - " + e3);
                    if (null == null && fileInputStream != null) {
                        try {
                            fileInputStream.close();
                        } catch (IOException e4) {
                        }
                    }
                } catch (IllegalStateException e5) {
                    Log.e(TAG, "processBitmap - " + e5);
                    if (null == null && fileInputStream != null) {
                        try {
                            fileInputStream.close();
                        } catch (IOException e6) {
                        }
                    }
                } catch (Throwable th) {
                    if (null == null && fileInputStream != null) {
                        try {
                            fileInputStream.close();
                        } catch (IOException e7) {
                        }
                    }
                }
            }
        }
        Bitmap bitmap = null;
        if (fileDescriptor != null) {
            if (this.neverCalculate || !calculate) {
                if (config.getScale() == 0) {
                    bitmap = BitmapDecoder.decodeSampledBitmapFromDescriptor(fileDescriptor);
                } else {
                    bitmap = BitmapDecoder.decodeSampledBitmapFromDescriptor(fileDescriptor, config.getScale());
                }
            } else if (config.getScale() == 0) {
                bitmap = BitmapDecoder.decodeSampledBitmapFromDescriptor(fileDescriptor, config.getBitmapWidth(), config.getBitmapHeight());
            } else {
                bitmap = BitmapDecoder.decodeSampledBitmapFromDescriptor(fileDescriptor, config.getScale());
            }
        }
        if (fileInputStream != null) {
            try {
                fileInputStream.close();
            } catch (IOException e8) {
            }
        }
        return bitmap;
    }

    public Bitmap processBitmap(String data, BitmapDisplayConfig config) {
        return processBitmap(data, config, true);
    }

    public void initHttpDiskCache() {
        if (!this.mOriginalCacheDir.exists()) {
            this.mOriginalCacheDir.mkdirs();
        }
        synchronized (this.mHttpDiskCacheLock) {
            if (ImageUtils.getUsableSpace(this.mOriginalCacheDir) > ((long) this.cacheSize)) {
                try {
                    this.mOriginalDiskCache = LruDiskCache.open(this.mOriginalCacheDir, 1, 1, (long) this.cacheSize);
                } catch (IOException e) {
                    this.mOriginalDiskCache = null;
                }
            }
            this.mHttpDiskCacheStarting = false;
            this.mHttpDiskCacheLock.notifyAll();
        }
    }

    public void clearCacheInternal() {
        synchronized (this.mHttpDiskCacheLock) {
            if (!(this.mOriginalDiskCache == null || this.mOriginalDiskCache.isClosed())) {
                try {
                    this.mOriginalDiskCache.delete();
                } catch (IOException e) {
                    Log.e(TAG, "clearCacheInternal - " + e);
                }
                this.mOriginalDiskCache = null;
                this.mHttpDiskCacheStarting = true;
                initHttpDiskCache();
            }
        }
    }

    public void flushCacheInternal() {
        synchronized (this.mHttpDiskCacheLock) {
            if (this.mOriginalDiskCache != null) {
                try {
                    this.mOriginalDiskCache.flush();
                } catch (IOException e) {
                    Log.e(TAG, "flush - " + e);
                }
            }
        }
    }

    public void closeCacheInternal() {
        synchronized (this.mHttpDiskCacheLock) {
            if (this.mOriginalDiskCache != null) {
                try {
                    if (!this.mOriginalDiskCache.isClosed()) {
                        this.mOriginalDiskCache.close();
                        this.mOriginalDiskCache = null;
                    }
                } catch (IOException e) {
                    Log.e(TAG, "closeCacheInternal - " + e);
                }
            }
        }
    }
}
