package com.vc.image;

import android.graphics.Bitmap;
import android.view.animation.Animation;

public class BitmapDisplayConfig {
    private Animation animation;
    private int animationType;
    private int bitmapHeight;
    private int bitmapWidth;
    private boolean hasAnimation = false;
    private boolean isCache = true;
    private Bitmap loadfailBitmap;
    private Bitmap loadingBitmap;
    private int scale = 0;
    private boolean showLoading = true;
    private boolean useMemoryCache = false;

    public class AnimationType {
        public static final int fadeIn = 1;
        public static final int userDefined = 0;
    }

    public void setShowLoading(boolean showLoading) {
        this.showLoading = showLoading;
    }

    public boolean getShowLoading() {
        return this.showLoading;
    }

    public boolean getIsUseCache() {
        return this.isCache;
    }

    public void setIsUseCache(boolean isCache) {
        this.isCache = isCache;
    }

    public boolean getIsUseMemoryCache() {
        return this.useMemoryCache;
    }

    public void setIsUseMemoryCache(boolean isUse) {
        this.useMemoryCache = isUse;
    }

    public int getScale() {
        return this.scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    public boolean getHasAnimation() {
        return this.hasAnimation;
    }

    public void setHasAnimation(boolean hasAnimation) {
        this.hasAnimation = hasAnimation;
    }

    public int getBitmapWidth() {
        return this.bitmapWidth;
    }

    public void setBitmapWidth(int bitmapWidth) {
        this.bitmapWidth = bitmapWidth;
    }

    public int getBitmapHeight() {
        return this.bitmapHeight;
    }

    public void setBitmapHeight(int bitmapHeight) {
        this.bitmapHeight = bitmapHeight;
    }

    public Animation getAnimation() {
        return this.animation;
    }

    public void setAnimation(Animation animation) {
        this.animation = animation;
    }

    public int getAnimationType() {
        return this.animationType;
    }

    public void setAnimationType(int animationType) {
        this.animationType = animationType;
    }

    public Bitmap getLoadingBitmap() {
        return this.loadingBitmap;
    }

    public void setLoadingBitmap(Bitmap loadingBitmap) {
        this.loadingBitmap = loadingBitmap;
    }

    public Bitmap getLoadfailBitmap() {
        return this.loadfailBitmap;
    }

    public void setLoadfailBitmap(Bitmap loadfailBitmap) {
        this.loadfailBitmap = loadfailBitmap;
    }
}
