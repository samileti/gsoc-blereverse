package com.vc.image;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

@SuppressLint({"FloatMath"})
public class ImageViewTouchBase extends ImageView {
    static final int DRAG = 1;
    static final int NONE = 0;
    static final int ROTATE = 3;
    static final int ZOOM = 2;
    static final int ZOOM_OR_ROTATE = 4;
    protected float MAX_SCALE = 8.0f;
    float dist = 1.0f;
    float imageH;
    float imageW;
    PointF lastClickPos = new PointF();
    long lastClickTime = 0;
    Context mContext;
    protected boolean mRotateEnabled = false;
    protected boolean mScaleEnabled = true;
    protected boolean mTouchEnabled = true;
    Matrix matrix = new Matrix();
    PointF mid = new PointF();
    float minScale = 1.0f;
    int mode = 0;
    public boolean onBottomSide = false;
    public boolean onLeftSide = false;
    public boolean onRightSide = false;
    public boolean onTopSide = false;
    PointF pA = new PointF();
    PointF pB = new PointF();
    float rotatedImageH;
    float rotatedImageW;
    double rotation = 0.0d;
    Matrix savedMatrix = new Matrix();
    float viewH;
    float viewW;

    public ImageViewTouchBase(Context context) {
        super(context);
        this.mContext = context;
        init(null);
    }

    public ImageViewTouchBase(Context context, float maxscale) {
        super(context);
        this.mContext = context;
        this.MAX_SCALE = maxscale;
        init(null);
    }

    public void setScaleEnabled(boolean e) {
        this.mScaleEnabled = e;
        this.mRotateEnabled = e;
        this.mTouchEnabled = e;
    }

    public boolean getScaleEnabled() {
        return this.mScaleEnabled;
    }

    public ImageViewTouchBase(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init(attrs);
    }

    public ImageViewTouchBase(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        setScaleType(ScaleType.MATRIX);
    }

    public void setBaseScaleType(ScaleType type) {
        setScaleType(type);
        invalidate();
    }

    public void resetScale() {
    }

    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        setImageWidthHeight();
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        setImageWidthHeight();
    }

    public void setImageResource(int resId) {
        super.setImageResource(resId);
        setImageWidthHeight();
    }

    private void setImageWidthHeight() {
        Drawable d = getDrawable();
        if (d != null) {
            float intrinsicWidth = (float) d.getIntrinsicWidth();
            this.rotatedImageW = intrinsicWidth;
            this.imageW = intrinsicWidth;
            intrinsicWidth = (float) d.getIntrinsicHeight();
            this.rotatedImageH = intrinsicWidth;
            this.imageH = intrinsicWidth;
            initImage();
        }
    }

    public boolean pagerCanScroll() {
        if (this.mode != 0) {
            return false;
        }
        float[] p = new float[9];
        this.matrix.getValues(p);
        float curScale = Math.abs(p[0]) + Math.abs(p[1]);
        if (curScale < this.minScale || curScale > 1.072f) {
            return false;
        }
        return true;
    }

    private void checkSiding() {
        if (!pagerCanScroll()) {
            float[] m = new float[9];
            this.matrix.getValues(m);
            float matrixX = m[2];
            float matrixY = m[5];
            float curScale = Math.abs(m[0]) + Math.abs(m[1]);
            float scaleWidth = (float) Math.round(this.imageW * curScale);
            float scaleHeight = (float) Math.round(this.imageH * curScale);
            this.onBottomSide = false;
            this.onTopSide = false;
            this.onRightSide = false;
            this.onLeftSide = false;
            if ((-matrixX) < 10.0f) {
                this.onLeftSide = true;
            }
            if ((scaleWidth >= this.viewW && (matrixX + scaleWidth) - this.viewW < 10.0f) || (scaleWidth <= this.viewW && (-matrixX) + scaleWidth <= this.viewW)) {
                this.onRightSide = true;
            }
            if ((-matrixY) < 10.0f) {
                this.onTopSide = true;
            }
            if (Math.abs(((-matrixY) + this.viewH) - scaleHeight) < 10.0f) {
                this.onBottomSide = true;
            }
        }
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.viewW = (float) w;
        this.viewH = (float) h;
        if (oldw == 0) {
            initImage();
            return;
        }
        fixScale();
        fixTranslation();
        setImageMatrix(this.matrix);
    }

    private void initImage() {
        if (this.viewW > 0.0f && this.viewH > 0.0f && this.imageW > 0.0f && this.imageH > 0.0f) {
            this.mode = 0;
            this.matrix.setScale(0.0f, 0.0f);
            fixScale();
            fixTranslation();
            setImageMatrix(this.matrix);
        }
    }

    private void fixScale() {
        float[] p = new float[9];
        this.matrix.getValues(p);
        float curScale = Math.abs(p[0]) + Math.abs(p[1]);
        float minScale = Math.min(this.viewW / this.rotatedImageW, this.viewH / this.rotatedImageH);
        if (curScale >= minScale) {
            return;
        }
        if (curScale > 0.0f) {
            double scale = (double) (minScale / curScale);
            p[0] = (float) (((double) p[0]) * scale);
            p[1] = (float) (((double) p[1]) * scale);
            p[3] = (float) (((double) p[3]) * scale);
            p[4] = (float) (((double) p[4]) * scale);
            this.matrix.setValues(p);
            return;
        }
        this.matrix.setScale(minScale, minScale);
    }

    private float maxPostScale() {
        float[] p = new float[9];
        this.matrix.getValues(p);
        return Math.max(Math.min(this.viewW / this.rotatedImageW, this.viewH / this.rotatedImageH), this.MAX_SCALE) / (Math.abs(p[0]) + Math.abs(p[1]));
    }

    private void fixTranslation() {
        RectF rect = new RectF(0.0f, 0.0f, this.imageW, this.imageH);
        this.matrix.mapRect(rect);
        float height = rect.height();
        float width = rect.width();
        float deltaX = 0.0f;
        float deltaY = 0.0f;
        if (width < this.viewW) {
            deltaX = ((this.viewW - width) / 2.0f) - rect.left;
        } else if (rect.left > 0.0f) {
            deltaX = -rect.left;
        } else if (rect.right < this.viewW) {
            deltaX = this.viewW - rect.right;
        }
        if (height < this.viewH) {
            deltaY = ((this.viewH - height) / 2.0f) - rect.top;
        } else if (rect.top > 0.0f) {
            deltaY = -rect.top;
        } else if (rect.bottom < this.viewH) {
            deltaY = this.viewH - rect.bottom;
        }
        this.matrix.postTranslate(deltaX, deltaY);
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & MotionEventCompat.ACTION_MASK) {
            case 0:
                this.savedMatrix.set(this.matrix);
                this.pA.set(event.getX(), event.getY());
                this.pB.set(event.getX(), event.getY());
                this.mode = 1;
                break;
            case 1:
            case 6:
                if (this.mode == 1) {
                    if (spacing(this.pA.x, this.pA.y, this.pB.x, this.pB.y) < 50.0f) {
                        long now = System.currentTimeMillis();
                        if (now - this.lastClickTime < 500) {
                            if (spacing(this.pA.x, this.pA.y, this.lastClickPos.x, this.lastClickPos.y) < 50.0f) {
                                doubleClick(this.pA.x, this.pA.y);
                                now = 0;
                            }
                        }
                        this.lastClickPos.set(this.pA);
                        this.lastClickTime = now;
                    }
                } else if (this.mode == 3) {
                    int level = (int) Math.floor((this.rotation + 0.7853981633974483d) / 1.5707963267948966d);
                    if (level == 4) {
                        level = 0;
                    }
                    this.matrix.set(this.savedMatrix);
                    this.matrix.postRotate((float) (level * 90), this.mid.x, this.mid.y);
                    if (level == 1 || level == 3) {
                        float tmp = this.rotatedImageW;
                        this.rotatedImageW = this.rotatedImageH;
                        this.rotatedImageH = tmp;
                        fixScale();
                    }
                    fixTranslation();
                    setImageMatrix(this.matrix);
                }
                this.mode = 0;
                break;
            case 2:
                PointF pointF;
                double a;
                double b;
                double c;
                if (this.mode == 4) {
                    pointF = new PointF((event.getX(1) - event.getX(0)) + this.pA.x, (event.getY(1) - event.getY(0)) + this.pA.y);
                    a = (double) spacing(this.pB.x, this.pB.y, pointF.x, pointF.y);
                    b = (double) spacing(this.pA.x, this.pA.y, pointF.x, pointF.y);
                    c = (double) spacing(this.pA.x, this.pA.y, this.pB.x, this.pB.y);
                    if (a >= 10.0d) {
                        double angleB = Math.acos((((a * a) + (c * c)) - (b * b)) / ((2.0d * a) * c));
                        if (angleB <= 0.7853981633974483d || angleB >= 3.0d * 0.7853981633974483d) {
                            this.mode = 2;
                        } else {
                            this.mode = 3;
                            this.rotation = 0.0d;
                        }
                    }
                }
                if (this.mode != 1) {
                    if (this.mode != 2) {
                        if (this.mode == 3 && this.mRotateEnabled) {
                            pointF = new PointF((event.getX(1) - event.getX(0)) + this.pA.x, (event.getY(1) - event.getY(0)) + this.pA.y);
                            a = (double) spacing(this.pB.x, this.pB.y, pointF.x, pointF.y);
                            b = (double) spacing(this.pA.x, this.pA.y, pointF.x, pointF.y);
                            c = (double) spacing(this.pA.x, this.pA.y, this.pB.x, this.pB.y);
                            if (b > 10.0d) {
                                double angleA = Math.acos((((b * b) + (c * c)) - (a * a)) / ((2.0d * b) * c));
                                if (((((double) pointF.x) * ((double) (this.pB.y - this.pA.y))) + (((double) pointF.y) * ((double) (this.pA.x - this.pB.x)))) + ((double) ((this.pB.x * this.pA.y) - (this.pA.x * this.pB.y))) > 0.0d) {
                                    angleA = 6.283185307179586d - angleA;
                                }
                                this.rotation = angleA;
                                this.matrix.set(this.savedMatrix);
                                this.matrix.postRotate((float) ((this.rotation * 180.0d) / 3.141592653589793d), this.mid.x, this.mid.y);
                                setImageMatrix(this.matrix);
                                break;
                            }
                        }
                    }
                    float newDist = spacing(event.getX(0), event.getY(0), event.getX(1), event.getY(1));
                    if (newDist > 10.0f && this.mScaleEnabled) {
                        this.matrix.set(this.savedMatrix);
                        float tScale = Math.min(newDist / this.dist, maxPostScale());
                        this.matrix.postScale(tScale, tScale, this.mid.x, this.mid.y);
                        fixScale();
                        fixTranslation();
                        setImageMatrix(this.matrix);
                        break;
                    }
                }
                this.matrix.set(this.savedMatrix);
                this.pB.set(event.getX(), event.getY());
                this.matrix.postTranslate(event.getX() - this.pA.x, event.getY() - this.pA.y);
                fixTranslation();
                setImageMatrix(this.matrix);
                break;
                break;
            case 5:
                if (event.getActionIndex() <= 1) {
                    this.dist = spacing(event.getX(0), event.getY(0), event.getX(1), event.getY(1));
                    if (this.dist > 10.0f) {
                        this.savedMatrix.set(this.matrix);
                        this.pA.set(event.getX(0), event.getY(0));
                        this.pB.set(event.getX(1), event.getY(1));
                        this.mid.set((event.getX(0) + event.getX(1)) / 2.0f, (event.getY(0) + event.getY(1)) / 2.0f);
                        this.mode = 4;
                        break;
                    }
                }
                break;
        }
        checkSiding();
        return this.mTouchEnabled;
    }

    public void ZoomTo(float to) {
        if (this.mScaleEnabled) {
            this.matrix.postScale(to, to, this.mid.x, this.mid.y);
            fixScale();
            fixTranslation();
            setImageMatrix(this.matrix);
        }
    }

    private float spacing(float x1, float y1, float x2, float y2) {
        float x = x1 - x2;
        float y = y1 - y2;
        return FloatMath.sqrt((x * x) + (y * y));
    }

    private void doubleClick(float x, float y) {
        float[] p = new float[9];
        this.matrix.getValues(p);
        float curScale = Math.abs(p[0]) + Math.abs(p[1]);
        float minScale = Math.min(this.viewW / this.rotatedImageW, this.viewH / this.rotatedImageH);
        float toScale;
        if (((double) curScale) <= ((double) minScale) + 0.01d) {
            toScale = Math.max(minScale, this.MAX_SCALE) / curScale;
            this.matrix.postScale(toScale, toScale, x, y);
        } else {
            toScale = minScale / curScale;
            this.matrix.postScale(toScale, toScale, x, y);
            fixTranslation();
        }
        setImageMatrix(this.matrix);
    }
}
