package com.vc.image;

import java.io.OutputStream;

public interface Downloader {
    boolean downloadToLocalStreamByUrl(String str, OutputStream outputStream);
}
