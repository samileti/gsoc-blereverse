package com.vc.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import com.vc.cloudbalance.common.WeightUnitHelper;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;
import org.json.JSONObject;

public class ObjectHelper {
    public static String Convert2String(Date date, String type) {
        if (date == null) {
            return "";
        }
        return new SimpleDateFormat(type).format(date);
    }

    public static String trim(String str) {
        if (str == null) {
            return "";
        }
        return str.trim();
    }

    public static String Convert2MathCount(int count, Object obj) {
        if (obj == null) {
            return "";
        }
        try {
            return String.format("%." + count + "f", new Object[]{Float.valueOf(obj.toString())}).replace(",", ".");
        } catch (Exception e) {
            return obj.toString();
        }
    }

    public static String getDIF(int count, Object value1, Object value2) {
        if (value1 == null || value2 == null) {
            return "";
        }
        try {
            return String.format("%." + count + "f", new Object[]{Float.valueOf(Float.valueOf(value1.toString()).floatValue() - Float.valueOf(value2.toString()).floatValue())});
        } catch (Exception e) {
            return "";
        }
    }

    public static String FormatDateString(int year, int month, int dayOfMonth) {
        String strmonth = "";
        String dayofmonthstr = "";
        if (month + 1 < 10) {
            strmonth = new StringBuilder(WeightUnitHelper.Kg).append(month + 1).toString();
        } else {
            strmonth = new StringBuilder(String.valueOf(month + 1)).toString();
        }
        if (dayOfMonth < 10) {
            dayofmonthstr = new StringBuilder(WeightUnitHelper.Kg).append(dayOfMonth).toString();
        } else {
            dayofmonthstr = new StringBuilder(String.valueOf(dayOfMonth)).toString();
        }
        return new StringBuilder(String.valueOf(year)).append("-").append(strmonth).append("-").append(dayofmonthstr).toString();
    }

    public static String Convert2String(Object value) {
        if (value == null) {
            return "";
        }
        try {
            return String.valueOf(value);
        } catch (Exception e) {
            return "";
        }
    }

    public static JSONObject Convert2JsonObject(Object value) {
        try {
            return new JSONObject(value.toString());
        } catch (Exception e) {
            return new JSONObject();
        }
    }

    public static long Convert2Long(Object object) {
        try {
            long l = Long.parseLong(object.toString());
            return Long.parseLong(object.toString());
        } catch (Exception e) {
            return 0;
        }
    }

    public static double Convert2Double(Object object) {
        try {
            return Double.parseDouble(object.toString());
        } catch (Exception e) {
            return 0.0d;
        }
    }

    public static Date Convert2Date(String dateString) {
        return Convert2Date(dateString, "yyyy-MM-dd HH:mm:ss");
    }

    public static Date Convert2Date(String dateString, String type) {
        if (dateString == null || dateString.trim().equals("") || dateString.trim().equals("null")) {
            return null;
        }
        DateFormat df = new SimpleDateFormat(type);
        Date date = new Date();
        try {
            return df.parse(dateString);
        } catch (ParseException e) {
            Log.e("StringToDate", new StringBuilder(String.valueOf(dateString)).append("    ").append(e).toString());
            e.printStackTrace();
            return date;
        }
    }

    public static int Convert2Int(Object obj) {
        try {
            return Integer.valueOf(obj.toString().trim()).intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public static float Convert2Float(String val) {
        try {
            return Float.valueOf(val).floatValue();
        } catch (Exception e) {
            return 0.0f;
        }
    }

    public static String GetBundleString(Activity activity, String key) {
        try {
            Bundle bundle = activity.getIntent().getExtras();
            if (bundle == null) {
                return "";
            }
            String value = bundle.getString(key);
            if (value == null || value.equals("")) {
                return "";
            }
            return value;
        } catch (Exception e) {
            return "";
        }
    }

    public static int GetBundleInt(Activity activity, String key) {
        int i = 0;
        try {
            Bundle bundle = activity.getIntent().getExtras();
            if (bundle != null) {
                i = bundle.getInt(key, 0);
            }
        } catch (Exception e) {
        }
        return i;
    }

    public static String GetBundleString(Intent intent, String key) {
        try {
            Bundle bundle = intent.getExtras();
            if (bundle == null) {
                return "";
            }
            String value = bundle.getString(key);
            if (value == null || value.equals("")) {
                return "";
            }
            return value;
        } catch (Exception e) {
            return "";
        }
    }

    public static int GetBundleInt(Intent intent, String key) {
        int i = 0;
        try {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                i = bundle.getInt(key, 0);
            }
        } catch (Exception e) {
        }
        return i;
    }

    public static int GetReciprocalDays(String smdate) {
        try {
            return daysBetween(new SimpleDateFormat("yyyy-MM-dd").format(new Date()), smdate);
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static int daysBetween(String smdate, String bdate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(smdate));
        long time1 = cal.getTimeInMillis();
        cal.setTime(sdf.parse(bdate));
        return Integer.parseInt(String.valueOf((cal.getTimeInMillis() - time1) / 86400000));
    }

    public static int secondsBetween(String smdate, String bdate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(smdate));
        long time1 = cal.getTimeInMillis();
        cal.setTime(sdf.parse(bdate));
        return Integer.parseInt(String.valueOf((cal.getTimeInMillis() - time1) / 1000));
    }

    public static boolean isIP(String ip) {
        return Pattern.compile("(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])").matcher(ip).matches();
    }

    public static boolean isNumeric(String str) {
        try {
            Integer.valueOf(str.toString());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isEmpty(EditText et) {
        try {
            return et.getText().toString().trim().equals("");
        } catch (Exception e) {
            return true;
        }
    }

    public static boolean isMobileNO(String mobiles) {
        return Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$").matcher(mobiles).matches();
    }

    public static boolean isEmail(String email) {
        return Pattern.compile("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$").matcher(email).matches();
    }
}
