package com.vc.util;

import android.util.Log;

public class LogUtils {
    private static final boolean DEBUG = true;
    public static final String TAG = "VC";

    public static void m3e(String message) {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        if (elements.length < 4) {
            Log.e(TAG, "Stack to shallow");
            return;
        }
        String fullClassName = elements[3].getClassName();
        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        String methodName = elements[3].getMethodName();
        Log.e(TAG, new StringBuilder(String.valueOf(message)).append(" at ").append(className).append(".").append(methodName).append("():").append(elements[3].getLineNumber()).toString());
    }

    public static void m2d(String message) {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        if (elements.length < 3) {
            Log.e(TAG, "Stack to shallow");
            return;
        }
        String fullClassName = elements[3].getClassName();
        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        String methodName = elements[3].getMethodName();
        Log.d(TAG, new StringBuilder(String.valueOf(message)).append(" at ").append(className).append(".").append(methodName).append("():").append(elements[3].getLineNumber()).toString());
    }

    public static void m4i(String message) {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        if (elements.length < 3) {
            Log.e(TAG, "Stack to shallow");
            return;
        }
        String fullClassName = elements[3].getClassName();
        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        String methodName = elements[3].getMethodName();
        Log.i(TAG, new StringBuilder(String.valueOf(message)).append(" at ").append(className).append(".").append(methodName).append("():").append(elements[3].getLineNumber()).toString());
    }

    public static void m6w(String message) {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        if (elements.length < 3) {
            Log.e(TAG, "Stack to shallow");
            return;
        }
        String fullClassName = elements[3].getClassName();
        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        String methodName = elements[3].getMethodName();
        Log.w(TAG, new StringBuilder(String.valueOf(message)).append(" at ").append(className).append(".").append(methodName).append("():").append(elements[3].getLineNumber()).toString());
    }

    public static void m5v(String message) {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        if (elements.length < 3) {
            Log.e(TAG, "Stack to shallow");
            return;
        }
        String fullClassName = elements[3].getClassName();
        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        String methodName = elements[3].getMethodName();
        Log.v(TAG, new StringBuilder(String.valueOf(message)).append(" at ").append(className).append(".").append(methodName).append("():").append(elements[3].getLineNumber()).toString());
    }
}
