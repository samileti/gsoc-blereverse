package com.vc.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.provider.MediaStore.Images.Media;
import android.provider.MediaStore.Images.Thumbnails;
import android.support.v4.media.TransportMediator;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.text.format.Formatter;
import android.view.View;
import android.view.View.MeasureSpec;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ImageUtil {
    public static final int BOTTOM = 4;
    public static final int LEFT = 0;
    public static final int RIGHT = 1;
    public static final int TOP = 3;

    public enum ScalingLogic {
        CROP,
        FIT
    }

    public static boolean isSDCardExist() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static Bitmap compBitmap(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(CompressFormat.JPEG, 100, baos);
        if (baos.toByteArray().length / 1024 > 1024) {
            baos.reset();
            image.compress(CompressFormat.JPEG, 50, baos);
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
        Options newOpts = new Options();
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        int be = 1;
        if (w > h && ((float) w) > 400.0f) {
            be = (int) (((float) newOpts.outWidth) / 400.0f);
        } else if (w < h && ((float) h) > 400.0f) {
            be = (int) (((float) newOpts.outHeight) / 400.0f);
        }
        if (be <= 0) {
            be = 1;
        }
        newOpts.inSampleSize = be;
        return compressImage(BitmapFactory.decodeStream(new ByteArrayInputStream(baos.toByteArray()), null, newOpts));
    }

    public static Bitmap compressImage(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(CompressFormat.JPEG, 100, baos);
        int options = 100;
        while (baos.toByteArray().length / 1024 > 100) {
            baos.reset();
            image.compress(CompressFormat.JPEG, options, baos);
            options -= 10;
        }
        return BitmapFactory.decodeStream(new ByteArrayInputStream(baos.toByteArray()), null, null);
    }

    public static void writeFile(byte[] data, String fileName) {
        IOException e;
        Throwable th;
        File f = new File(fileName);
        FileOutputStream fout = null;
        try {
            if (!f.exists()) {
                f.createNewFile();
            }
            FileOutputStream fout2 = new FileOutputStream(f);
            try {
                fout2.write(data);
                fout2.close();
                if (fout2 != null) {
                    try {
                        fout2.close();
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                fout = fout2;
            } catch (IOException e3) {
                e = e3;
                fout = fout2;
                try {
                    e.printStackTrace();
                    if (fout != null) {
                        try {
                            fout.close();
                        } catch (Exception e22) {
                            e22.printStackTrace();
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (fout != null) {
                        try {
                            fout.close();
                        } catch (Exception e222) {
                            e222.printStackTrace();
                        }
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                fout = fout2;
                if (fout != null) {
                    fout.close();
                }
                throw th;
            }
        } catch (IOException e4) {
            e = e4;
            e.printStackTrace();
            if (fout != null) {
                fout.close();
            }
        }
    }

    public static Bitmap decodeFile(File f) {
        try {
            Options o = new Options();
            o.inJustDecodeBounds = true;
            FileInputStream fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();
            int scale = 1;
            if (o.outHeight > 1000 || o.outWidth > 1000) {
                scale = (int) Math.pow(2.0d, (double) ((int) Math.round(Math.log(((double) 1000) / ((double) Math.max(o.outHeight, o.outWidth))) / Math.log(0.5d))));
            }
            Options o2 = new Options();
            o2.inSampleSize = scale;
            fis = new FileInputStream(f);
            Bitmap b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();
            return b;
        } catch (IOException e) {
            return null;
        }
    }

    public static Bitmap decodeFile(File f, int scale) {
        try {
            Options o = new Options();
            o.inJustDecodeBounds = true;
            FileInputStream fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();
            Options o2 = new Options();
            o2.inSampleSize = scale;
            fis = new FileInputStream(f);
            Bitmap b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();
            return b;
        } catch (IOException e) {
            return null;
        }
    }

    public static int calculateSampleSize(int srcWidth, int srcHeight, int dstWidth, int dstHeight, ScalingLogic scalingLogic) {
        if (scalingLogic == ScalingLogic.FIT) {
            if (((float) srcWidth) / ((float) srcHeight) > ((float) dstWidth) / ((float) dstHeight)) {
                return srcWidth / dstWidth;
            }
            return srcHeight / dstHeight;
        } else if (((float) srcWidth) / ((float) srcHeight) > ((float) dstWidth) / ((float) dstHeight)) {
            return srcHeight / dstHeight;
        } else {
            return srcWidth / dstWidth;
        }
    }

    public static Bitmap decodeSampledBitmapFromByte(byte[] res, int reqWidth, int reqHeight) {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(res, 0, res.length, options);
        if (reqHeight == -1) {
            options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, options.outWidth, options.outHeight, ScalingLogic.CROP);
        } else {
            options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, reqWidth, reqHeight, ScalingLogic.CROP);
        }
        options.inJustDecodeBounds = false;
        return getUnErrorBitmap(res, options);
    }

    private static Bitmap getUnErrorBitmap(byte[] res, Options options) {
        try {
            return BitmapFactory.decodeByteArray(res, 0, res.length, options);
        } catch (OutOfMemoryError e) {
            options.inSampleSize++;
            return getUnErrorBitmap(res, options);
        }
    }

    public static String getBitmapWH(Bitmap bitmap) {
        byte[] datas = Bitmap2Bytes(bitmap);
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(datas, 0, datas.length, options);
        return options.outWidth + "X" + options.outHeight;
    }

    public static Bitmap CompressBitmapByQuality(Bitmap bitmap, int quality) {
        return BitmapFactory.decodeStream(Bitmap2InputStream(bitmap, quality));
    }

    public static Bitmap CompressBitmapBySize(Bitmap bitmap, int quality) {
        InputStream is = Bitmap2InputStream(bitmap);
        Options options = new Options();
        options.inJustDecodeBounds = false;
        options.inSampleSize = quality;
        return BitmapFactory.decodeStream(is, null, options);
    }

    public static InputStream Bitmap2InputStream(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(CompressFormat.PNG, 100, baos);
        return new ByteArrayInputStream(baos.toByteArray());
    }

    public static InputStream Bitmap2InputStream(Bitmap bm, int quality) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(CompressFormat.JPEG, quality, baos);
        return new ByteArrayInputStream(baos.toByteArray());
    }

    public static Bitmap InputStream2Bitmap(InputStream is) {
        return BitmapFactory.decodeStream(is);
    }

    public static byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    public static String getBitmapWH(Context context, int res) {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(), res, options);
        return options.outWidth + "X" + options.outHeight;
    }

    public static String getBitmapSize(Context context, int res) {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(), res, options);
        int size = 0;
        if (options.inPreferredConfig == Config.ARGB_8888) {
            size = (options.outHeight * options.outWidth) * 4;
        } else if (options.inPreferredConfig == Config.ARGB_4444) {
            size = (options.outHeight * options.outWidth) * 2;
        } else if (options.inPreferredConfig == Config.ALPHA_8) {
            size = options.outHeight * options.outWidth;
        }
        return Formatter.formatFileSize(context, (long) size);
    }

    public static String getBitmapSize(Context context, Bitmap bitmap) {
        return Formatter.formatFileSize(context, (long) (bitmap.getRowBytes() * bitmap.getHeight()));
    }

    public static Bitmap zoomBitmap(Bitmap bitmap, int w, int h) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) w) / ((float) width), ((float) h) / ((float) height));
        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Config config;
        int width = drawable.getIntrinsicWidth();
        int height = drawable.getIntrinsicHeight();
        if (drawable.getOpacity() != -1) {
            config = Config.ARGB_8888;
        } else {
            config = Config.RGB_565;
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, config);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, width, height);
        drawable.draw(canvas);
        return bitmap;
    }

    public static Drawable bitmapToDrawable(Bitmap bmp) {
        return new BitmapDrawable(bmp);
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, float roundPx) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-12434878);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public static Bitmap createReflectionImageWithOrigin(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.preScale(1.0f, -1.0f);
        Bitmap reflectionImage = Bitmap.createBitmap(bitmap, 0, height / 2, width, height / 2, matrix, false);
        Bitmap bitmapWithReflection = Bitmap.createBitmap(width, (height / 2) + height, Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmapWithReflection);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, null);
        canvas.drawRect(0.0f, (float) height, (float) width, (float) (height + 4), new Paint());
        canvas.drawBitmap(reflectionImage, 0.0f, (float) (height + 4), null);
        Paint paint = new Paint();
        paint.setShader(new LinearGradient(0.0f, (float) bitmap.getHeight(), 0.0f, (float) (bitmapWithReflection.getHeight() + 4), 1895825407, ViewCompat.MEASURED_SIZE_MASK, TileMode.CLAMP));
        paint.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
        canvas.drawRect(0.0f, (float) height, (float) width, (float) (bitmapWithReflection.getHeight() + 4), paint);
        return bitmapWithReflection;
    }

    public static byte[] bitmapToByte(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    public static Bitmap byteToBitmap(byte[] buffer) {
        return BitmapFactory.decodeByteArray(buffer, 0, buffer.length);
    }

    public static Bitmap byteToBitmap(byte[] buffer, int inSampleSize) {
        Options options = new Options();
        options.inSampleSize = inSampleSize;
        return BitmapFactory.decodeByteArray(buffer, 0, buffer.length, options);
    }

    private static Bitmap overlay(Bitmap bmp, int overRef, Resources res) {
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, Config.RGB_565);
        Bitmap overlay = BitmapFactory.decodeResource(res, overRef);
        int w = overlay.getWidth();
        int h = overlay.getHeight();
        float scaleX = (((float) width) * 1.0f) / ((float) w);
        float scaleY = (((float) height) * 1.0f) / ((float) h);
        Matrix matrix = new Matrix();
        matrix.postScale(scaleX, scaleY);
        Bitmap overlayCopy = Bitmap.createBitmap(overlay, 0, 0, w, h, matrix, true);
        int[] srcPixels = new int[(width * height)];
        int[] layPixels = new int[(width * height)];
        bmp.getPixels(srcPixels, 0, width, 0, 0, width, height);
        overlayCopy.getPixels(layPixels, 0, width, 0, 0, width, height);
        for (int i = 0; i < height; i++) {
            for (int k = 0; k < width; k++) {
                int pos = (i * width) + k;
                int pixColor = srcPixels[pos];
                int layColor = layPixels[pos];
                int pixR = Color.red(pixColor);
                int pixG = Color.green(pixColor);
                int pixB = Color.blue(pixColor);
                int pixA = Color.alpha(pixColor);
                int layR = Color.red(layColor);
                int layG = Color.green(layColor);
                int newG = (int) ((((float) pixG) * 0.5f) + (((float) layG) * 0.5f));
                int newB = (int) ((((float) pixB) * 0.5f) + (((float) Color.blue(layColor)) * 0.5f));
                int layA = (int) ((((float) pixA) * 0.5f) + (((float) Color.alpha(layColor)) * 0.5f));
                srcPixels[pos] = Color.argb(Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, layA)), Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, (int) ((((float) pixR) * 0.5f) + (((float) layR) * 0.5f)))), Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newG)), Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newB)));
            }
        }
        bitmap.setPixels(srcPixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    public static Bitmap doodle(Bitmap src, Bitmap watermark) {
        Bitmap newb = Bitmap.createBitmap(src.getWidth(), src.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(newb);
        canvas.drawBitmap(src, 0.0f, 0.0f, null);
        canvas.drawBitmap(watermark, (float) ((src.getWidth() - watermark.getWidth()) / 2), (float) ((src.getHeight() - watermark.getHeight()) / 2), null);
        canvas.save(31);
        canvas.restore();
        watermark.recycle();
        return newb;
    }

    private static Bitmap oldRemeber(Bitmap bmp) {
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, Config.RGB_565);
        int[] pixels = new int[(width * height)];
        bmp.getPixels(pixels, 0, width, 0, 0, width, height);
        for (int i = 0; i < height; i++) {
            for (int k = 0; k < width; k++) {
                int pixColor = pixels[(width * i) + k];
                int pixR = Color.red(pixColor);
                int pixG = Color.green(pixColor);
                int pixB = Color.blue(pixColor);
                int newR = (int) (((0.393d * ((double) pixR)) + (0.769d * ((double) pixG))) + (0.189d * ((double) pixB)));
                int newG = (int) (((0.349d * ((double) pixR)) + (0.686d * ((double) pixG))) + (0.168d * ((double) pixB)));
                int newB = (int) (((0.272d * ((double) pixR)) + (0.534d * ((double) pixG))) + (0.131d * ((double) pixB)));
                pixels[(width * i) + k] = Color.argb(MotionEventCompat.ACTION_MASK, newR > 255 ? MotionEventCompat.ACTION_MASK : newR, newG > 255 ? MotionEventCompat.ACTION_MASK : newG, newB > MotionEventCompat.ACTION_MASK ? MotionEventCompat.ACTION_MASK : newB);
            }
        }
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        long end = System.currentTimeMillis();
        return bitmap;
    }

    private static Bitmap blurImage(Bitmap bmp) {
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, Config.RGB_565);
        int newR = 0;
        int newG = 0;
        int newB = 0;
        int[][] colors = (int[][]) Array.newInstance(Integer.TYPE, new int[]{9, 3});
        int length = width - 1;
        for (int i = 1; i < length; i++) {
            int len = height - 1;
            for (int k = 1; k < len; k++) {
                int m;
                for (m = 0; m < 9; m++) {
                    int s = 0;
                    int p = 0;
                    switch (m) {
                        case 0:
                            s = i - 1;
                            p = k - 1;
                            break;
                        case 1:
                            s = i;
                            p = k - 1;
                            break;
                        case 2:
                            s = i + 1;
                            p = k - 1;
                            break;
                        case 3:
                            s = i + 1;
                            p = k;
                            break;
                        case 4:
                            s = i + 1;
                            p = k + 1;
                            break;
                        case 5:
                            s = i;
                            p = k + 1;
                            break;
                        case 6:
                            s = i - 1;
                            p = k + 1;
                            break;
                        case 7:
                            s = i - 1;
                            p = k;
                            break;
                        case 8:
                            s = i;
                            p = k;
                            break;
                        default:
                            break;
                    }
                    int pixColor = bmp.getPixel(s, p);
                    colors[m][0] = Color.red(pixColor);
                    colors[m][1] = Color.green(pixColor);
                    colors[m][2] = Color.blue(pixColor);
                }
                for (m = 0; m < 9; m++) {
                    newR += colors[m][0];
                    newG += colors[m][1];
                    newB += colors[m][2];
                }
                newR = (int) (((float) newR) / 9.0f);
                newG = (int) (((float) newG) / 9.0f);
                newB = (int) (((float) newB) / 9.0f);
                bitmap.setPixel(i, k, Color.argb(MotionEventCompat.ACTION_MASK, Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newR)), Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newG)), Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newB))));
                newR = 0;
                newG = 0;
                newB = 0;
            }
        }
        return bitmap;
    }

    private static Bitmap blurImageAmeliorate(Bitmap bmp) {
        int[] iArr = new int[9];
        iArr = new int[]{1, 2, 1, 2, 4, 2, 1, 2, 1};
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, Config.RGB_565);
        int newR = 0;
        int newG = 0;
        int newB = 0;
        int[] pixels = new int[(width * height)];
        bmp.getPixels(pixels, 0, width, 0, 0, width, height);
        int length = height - 1;
        for (int i = 1; i < length; i++) {
            int len = width - 1;
            for (int k = 1; k < len; k++) {
                int idx = 0;
                for (int m = -1; m <= 1; m++) {
                    for (int n = -1; n <= 1; n++) {
                        int pixColor = pixels[(((i + m) * width) + k) + n];
                        int pixR = Color.red(pixColor);
                        int pixG = Color.green(pixColor);
                        newR += iArr[idx] * pixR;
                        newG += iArr[idx] * pixG;
                        newB += iArr[idx] * Color.blue(pixColor);
                        idx++;
                    }
                }
                newG /= 16;
                newB /= 16;
                pixels[(i * width) + k] = Color.argb(MotionEventCompat.ACTION_MASK, Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newR / 16)), Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newG)), Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newB)));
                newR = 0;
                newG = 0;
                newB = 0;
            }
        }
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        long end = System.currentTimeMillis();
        return bitmap;
    }

    private static Bitmap sharpenImageAmeliorate(Bitmap bmp) {
        int[] iArr = new int[9];
        iArr = new int[]{-1, -1, -1, -1, 9, -1, -1, -1, -1};
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, Config.RGB_565);
        int newR = 0;
        int newG = 0;
        int newB = 0;
        int[] pixels = new int[(width * height)];
        bmp.getPixels(pixels, 0, width, 0, 0, width, height);
        int length = height - 1;
        for (int i = 1; i < length; i++) {
            int len = width - 1;
            for (int k = 1; k < len; k++) {
                int idx = 0;
                for (int m = -1; m <= 1; m++) {
                    for (int n = -1; n <= 1; n++) {
                        int pixColor = pixels[(((i + n) * width) + k) + m];
                        int pixR = Color.red(pixColor);
                        int pixG = Color.green(pixColor);
                        newR += (int) (((float) (iArr[idx] * pixR)) * 0.3f);
                        newG += (int) (((float) (iArr[idx] * pixG)) * 0.3f);
                        newB += (int) (((float) (iArr[idx] * Color.blue(pixColor))) * 0.3f);
                        idx++;
                    }
                }
                pixels[(i * width) + k] = Color.argb(MotionEventCompat.ACTION_MASK, Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newR)), Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newG)), Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newB)));
                newR = 0;
                newG = 0;
                newB = 0;
            }
        }
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        long end = System.currentTimeMillis();
        return bitmap;
    }

    private static Bitmap emboss(Bitmap bmp) {
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, Config.RGB_565);
        int[] pixels = new int[(width * height)];
        bmp.getPixels(pixels, 0, width, 0, 0, width, height);
        int length = height - 1;
        for (int i = 1; i < length; i++) {
            int len = width - 1;
            for (int k = 1; k < len; k++) {
                int pos = (i * width) + k;
                int pixColor = pixels[pos];
                int pixR = Color.red(pixColor);
                int pixG = Color.green(pixColor);
                int pixB = Color.blue(pixColor);
                pixColor = pixels[pos + 1];
                pixels[pos] = Color.argb(MotionEventCompat.ACTION_MASK, Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, (Color.red(pixColor) - pixR) + TransportMediator.KEYCODE_MEDIA_PAUSE)), Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, (Color.green(pixColor) - pixG) + TransportMediator.KEYCODE_MEDIA_PAUSE)), Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, (Color.blue(pixColor) - pixB) + TransportMediator.KEYCODE_MEDIA_PAUSE)));
            }
        }
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private static Bitmap film(Bitmap bmp) {
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, Config.RGB_565);
        int[] pixels = new int[(width * height)];
        bmp.getPixels(pixels, 0, width, 0, 0, width, height);
        int length = height - 1;
        for (int i = 1; i < length; i++) {
            int len = width - 1;
            for (int k = 1; k < len; k++) {
                int pos = (i * width) + k;
                int pixColor = pixels[pos];
                int pixR = Color.red(pixColor);
                int newG = 255 - Color.green(pixColor);
                int newB = 255 - Color.blue(pixColor);
                pixels[pos] = Color.argb(MotionEventCompat.ACTION_MASK, Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, 255 - pixR)), Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newG)), Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newB)));
            }
        }
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    public static Bitmap sunshine(Bitmap bmp) {
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height, Config.RGB_565);
        int centerX = width / 2;
        int centerY = height / 2;
        int radius = Math.min(centerX, centerY);
        int[] pixels = new int[(width * height)];
        bmp.getPixels(pixels, 0, width, 0, 0, width, height);
        int length = height - 1;
        for (int i = 1; i < length; i++) {
            int len = width - 1;
            for (int k = 1; k < len; k++) {
                int pos = (i * width) + k;
                int pixColor = pixels[pos];
                int pixR = Color.red(pixColor);
                int pixG = Color.green(pixColor);
                int pixB = Color.blue(pixColor);
                int newR = pixR;
                int newG = pixG;
                int newB = pixB;
                int distance = (int) (Math.pow((double) (centerY - i), 2.0d) + Math.pow((double) (centerX - k), 2.0d));
                if (distance < radius * radius) {
                    int result = (int) (150.0d * (1.0d - (Math.sqrt((double) distance) / ((double) radius))));
                    newR = pixR + result;
                    newG = pixG + result;
                    newB = pixB + result;
                }
                pixels[pos] = Color.argb(MotionEventCompat.ACTION_MASK, Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newR)), Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newG)), Math.min(MotionEventCompat.ACTION_MASK, Math.max(0, newB)));
            }
        }
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    public static Drawable chroma(Drawable drawable, int sature) {
        drawable.mutate();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation((float) sature);
        drawable.setColorFilter(new ColorMatrixColorFilter(cm));
        return drawable;
    }

    public static boolean bitmapToFile(String photoPath, File aFile, int newWidth, int newHeight) {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(photoPath, options);
        options.inJustDecodeBounds = false;
        options.inSampleSize = reckonThumbnail(options.outWidth, options.outHeight, newWidth, newHeight);
        bitmap = BitmapFactory.decodeFile(photoPath, options);
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(CompressFormat.JPEG, 100, baos);
            byte[] photoBytes = baos.toByteArray();
            if (aFile.exists()) {
                aFile.delete();
            }
            aFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(aFile);
            fos.write(photoBytes);
            fos.flush();
            fos.close();
            return true;
        } catch (Exception e1) {
            e1.printStackTrace();
            if (aFile.exists()) {
                aFile.delete();
            }
            return false;
        }
    }

    public static void savePNG_After(Bitmap bitmap, String name) {
        try {
            FileOutputStream out = new FileOutputStream(new File(name));
            if (bitmap.compress(CompressFormat.PNG, 100, out)) {
                out.flush();
                out.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public static void saveJPGE_After(Bitmap bitmap, String path, String filename) {
        try {
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            FileOutputStream out = new FileOutputStream(new File(new StringBuilder(String.valueOf(path)).append(filename).toString()));
            if (bitmap.compress(CompressFormat.JPEG, 100, out)) {
                out.flush();
                out.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public static int reckonThumbnail(int oldWidth, int oldHeight, int newWidth, int newHeight) {
        int be;
        if ((oldHeight > newHeight && oldWidth > newWidth) || (oldHeight <= newHeight && oldWidth > newWidth)) {
            be = (int) (((float) oldWidth) / ((float) newWidth));
            if (be <= 1) {
                return 1;
            }
            return be;
        } else if (oldHeight <= newHeight || oldWidth > newWidth) {
            return 1;
        } else {
            be = (int) (((float) oldHeight) / ((float) newHeight));
            if (be <= 1) {
                return 1;
            }
            return be;
        }
    }

    public static Bitmap createBitmapForWatermark(Bitmap src, Bitmap watermark) {
        if (src == null) {
            return null;
        }
        int w = src.getWidth();
        int h = src.getHeight();
        int ww = watermark.getWidth();
        int wh = watermark.getHeight();
        Bitmap newb = Bitmap.createBitmap(w, h, Config.ARGB_8888);
        Canvas cv = new Canvas(newb);
        cv.drawBitmap(src, 0.0f, 0.0f, null);
        cv.drawBitmap(watermark, (float) ((w - ww) + 5), (float) ((h - wh) + 5), null);
        cv.save(31);
        cv.restore();
        return newb;
    }

    public static Bitmap getViewBitmap(View v) {
        v.clearFocus();
        v.setPressed(false);
        boolean willNotCache = v.willNotCacheDrawing();
        v.setWillNotCacheDrawing(false);
        int color = v.getDrawingCacheBackgroundColor();
        v.setDrawingCacheBackgroundColor(0);
        if (color != 0) {
            v.destroyDrawingCache();
        }
        v.buildDrawingCache();
        Bitmap cacheBitmap = v.getDrawingCache();
        if (cacheBitmap == null) {
            return null;
        }
        Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);
        v.destroyDrawingCache();
        v.setWillNotCacheDrawing(willNotCache);
        v.setDrawingCacheBackgroundColor(color);
        return bitmap;
    }

    public static Bitmap potoMix(int direction, Bitmap... bitmaps) {
        if (bitmaps.length <= 0) {
            return null;
        }
        if (bitmaps.length == 1) {
            return bitmaps[0];
        }
        Bitmap newBitmap = bitmaps[0];
        for (int i = 1; i < bitmaps.length; i++) {
            newBitmap = createBitmapForFotoMix(newBitmap, bitmaps[i], direction);
        }
        return newBitmap;
    }

    private static Bitmap createBitmapForFotoMix(Bitmap first, Bitmap second, int direction) {
        if (first == null) {
            return null;
        }
        if (second == null) {
            return first;
        }
        int fw = first.getWidth();
        int fh = first.getHeight();
        int sw = second.getWidth();
        int sh = second.getHeight();
        Bitmap newBitmap = null;
        int i;
        Canvas canvas;
        if (direction == 0) {
            i = fw + sw;
            if (fh <= sh) {
                fh = sh;
            }
            newBitmap = Bitmap.createBitmap(i, fh, Config.ARGB_8888);
            canvas = new Canvas(newBitmap);
            canvas.drawBitmap(first, (float) sw, 0.0f, null);
            canvas.drawBitmap(second, 0.0f, 0.0f, null);
        } else if (direction == 1) {
            i = fw + sw;
            if (fh <= sh) {
                fh = sh;
            }
            newBitmap = Bitmap.createBitmap(i, fh, Config.ARGB_8888);
            canvas = new Canvas(newBitmap);
            canvas.drawBitmap(first, 0.0f, 0.0f, null);
            canvas.drawBitmap(second, (float) fw, 0.0f, null);
        } else if (direction == 3) {
            if (sw <= fw) {
                sw = fw;
            }
            newBitmap = Bitmap.createBitmap(sw, fh + sh, Config.ARGB_8888);
            canvas = new Canvas(newBitmap);
            canvas.drawBitmap(first, 0.0f, (float) sh, null);
            canvas.drawBitmap(second, 0.0f, 0.0f, null);
        } else if (direction == 4) {
            if (sw <= fw) {
                sw = fw;
            }
            newBitmap = Bitmap.createBitmap(sw, fh + sh, Config.ARGB_8888);
            canvas = new Canvas(newBitmap);
            canvas.drawBitmap(first, 0.0f, 0.0f, null);
            canvas.drawBitmap(second, 0.0f, (float) fh, null);
        }
        return newBitmap;
    }

    public static Bitmap decodeBitmap(String path, int displayWidth, int displayHeight) {
        Options op = new Options();
        op.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(path, op);
        int wRatio = (int) Math.ceil((double) (((float) op.outWidth) / ((float) displayWidth)));
        int hRatio = (int) Math.ceil((double) (((float) op.outHeight) / ((float) displayHeight)));
        if (wRatio > 1 && hRatio > 1) {
            if (wRatio > hRatio) {
                op.inSampleSize = wRatio;
            } else {
                op.inSampleSize = hRatio;
            }
        }
        op.inJustDecodeBounds = false;
        return Bitmap.createScaledBitmap(BitmapFactory.decodeFile(path, op), displayWidth, displayHeight, true);
    }

    public static Bitmap decodeBitmap(String path, int maxImageSize) {
        Options op = new Options();
        op.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(path, op);
        int scale = 1;
        if (op.outWidth > maxImageSize || op.outHeight > maxImageSize) {
            scale = (int) Math.pow(2.0d, (double) ((int) Math.round(Math.log(((double) maxImageSize) / ((double) Math.max(op.outWidth, op.outHeight))) / Math.log(0.5d))));
        }
        op.inJustDecodeBounds = false;
        op.inSampleSize = scale;
        return BitmapFactory.decodeFile(path, op);
    }

    public static Cursor queryThumbnails(Activity context) {
        return context.managedQuery(Thumbnails.EXTERNAL_CONTENT_URI, new String[]{"_data", "_id", "image_id"}, null, null, "image_id ASC");
    }

    public static Cursor queryThumbnails(Activity context, String selection, String[] selectionArgs) {
        return context.managedQuery(Thumbnails.EXTERNAL_CONTENT_URI, new String[]{"_data", "_id", "image_id"}, selection, selectionArgs, "image_id ASC");
    }

    public static Bitmap queryThumbnailById(Activity context, int thumbId) {
        Cursor cursor = queryThumbnails(context, "_id = ?", new String[]{new StringBuilder(String.valueOf(thumbId)).toString()});
        if (cursor.moveToFirst()) {
            String path = cursor.getString(cursor.getColumnIndexOrThrow("_data"));
            cursor.close();
            return decodeBitmap(path, 100, 100);
        }
        cursor.close();
        return null;
    }

    public static Bitmap[] queryThumbnailsByIds(Activity context, Integer[] thumbIds) {
        Bitmap[] bitmaps = new Bitmap[thumbIds.length];
        for (int i = 0; i < bitmaps.length; i++) {
            bitmaps[i] = queryThumbnailById(context, thumbIds[i].intValue());
        }
        return bitmaps;
    }

    public static List<Bitmap> queryThumbnailList(Activity context) {
        List<Bitmap> bitmaps = new ArrayList();
        Cursor cursor = queryThumbnails(context);
        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            bitmaps.add(decodeBitmap(cursor.getString(cursor.getColumnIndexOrThrow("_data")), 100, 100));
        }
        cursor.close();
        return bitmaps;
    }

    public static List<Bitmap> queryThumbnailListByIds(Activity context, int[] thumbIds) {
        List<Bitmap> bitmaps = new ArrayList();
        for (int queryThumbnailById : thumbIds) {
            bitmaps.add(queryThumbnailById(context, queryThumbnailById));
        }
        return bitmaps;
    }

    public static Cursor queryImages(Activity context) {
        return context.managedQuery(Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "_data", "_display_name"}, null, null, "bucket_display_name");
    }

    public static Cursor queryImages(Activity context, String selection, String[] selectionArgs) {
        return context.managedQuery(Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "_data", "_display_name"}, selection, selectionArgs, "bucket_display_name");
    }

    public static Bitmap queryImageById(Activity context, int imageId) {
        Cursor cursor = queryImages(context, "_id=?", new String[]{new StringBuilder(String.valueOf(imageId)).toString()});
        if (cursor.moveToFirst()) {
            String path = cursor.getString(cursor.getColumnIndexOrThrow("_data"));
            cursor.close();
            return decodeBitmap(path, 220);
        }
        cursor.close();
        return null;
    }

    public static Bitmap queryImageById(Context context, String name) {
        return BitmapFactory.decodeResource(context.getResources(), context.getResources().getIdentifier(name, "drawable", context.getApplicationInfo().packageName));
    }

    public static Bitmap queryImageByThumbnailId(Activity context, Integer thumbId) {
        Cursor cursor = queryThumbnails(context, "_id = ?", new String[]{thumbId});
        if (cursor.moveToFirst()) {
            int imageId = cursor.getInt(cursor.getColumnIndexOrThrow("image_id"));
            cursor.close();
            return queryImageById(context, imageId);
        }
        cursor.close();
        return null;
    }

    public static Bitmap ajustImage(Bitmap bitemap) {
        ColorMatrix cMatrix = new ColorMatrix();
        cMatrix.setScale(2.0f, 2.0f, 2.0f, 2.0f);
        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cMatrix));
        Bitmap bmp = Bitmap.createBitmap(bitemap.getWidth(), bitemap.getHeight(), bitemap.getConfig());
        new Canvas(bmp).drawBitmap(bitemap, 0.0f, 0.0f, paint);
        return bmp;
    }

    public static Bitmap getBitmapFromView(View view, int width, int height) {
        view.measure(MeasureSpec.makeMeasureSpec(width, 1073741824), MeasureSpec.makeMeasureSpec(height, 1073741824));
        view.layout(0, 0, width, height);
        Bitmap bitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        view.draw(new Canvas(bitmap));
        return bitmap;
    }

    public Bitmap readBitmap(Context context, int resId) {
        Options opts = new Options();
        opts.inPreferredConfig = Config.RGB_565;
        opts.inPurgeable = true;
        opts.inInputShareable = true;
        return BitmapFactory.decodeStream(context.getResources().openRawResource(resId), null, opts);
    }

    public static Bitmap GetBitmapByFileName(Context context, String fileName) {
        try {
            return BitmapFactory.decodeResource(context.getResources(), context.getResources().getIdentifier(fileName, "drawable", context.getPackageName()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap getBitmapFromView(View view) {
        Bitmap bitmap = null;
        try {
            view.destroyDrawingCache();
            view.measure(MeasureSpec.makeMeasureSpec(0, 0), MeasureSpec.makeMeasureSpec(0, 0));
            view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
            view.setDrawingCacheEnabled(true);
            bitmap = view.getDrawingCache(true);
        } catch (Exception e) {
        }
        return bitmap;
    }
}
