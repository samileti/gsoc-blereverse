package com.vc.pulltorefresh;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.vc.pulltorefresh.PullToRefreshBase.Mode;
import com.vc.pulltorefresh.PullToRefreshBase.OnRefreshListener;

public class PullToRefreshWebView extends PullToRefreshBase<WebView> {
    private static final OnRefreshListener<WebView> defaultOnRefreshListener = null;
    private final WebChromeClient defaultWebChromeClient;

    @TargetApi(9)
    final class InternalWebViewSDK9 extends WebView {
        static final int OVERSCROLL_FUZZY_THRESHOLD = 2;
        static final float OVERSCROLL_SCALE_FACTOR = 1.5f;
        final /* synthetic */ PullToRefreshWebView this$0;

        public InternalWebViewSDK9(PullToRefreshWebView pullToRefreshWebView, Context context, AttributeSet attributeSet) {
            throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n");
        }

        protected boolean overScrollBy(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, boolean z) {
            throw new Error("Unresolved compilation problem: \n");
        }

        private int getScrollRange() {
            throw new Error("Unresolved compilation problem: \n");
        }
    }

    public PullToRefreshWebView(Context context) {
        throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n");
    }

    public PullToRefreshWebView(Context context, AttributeSet attributeSet) {
        throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n");
    }

    public PullToRefreshWebView(Context context, Mode mode) {
        throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n");
    }

    protected WebView createRefreshableView(Context context, AttributeSet attributeSet) {
        throw new Error("Unresolved compilation problem: \n\tR cannot be resolved to a variable\n");
    }

    protected boolean isReadyForPullDown() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected boolean isReadyForPullUp() {
        throw new Error("Unresolved compilation problem: \n");
    }
}
