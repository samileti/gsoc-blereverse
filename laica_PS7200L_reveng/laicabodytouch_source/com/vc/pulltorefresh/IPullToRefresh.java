package com.vc.pulltorefresh;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.animation.Interpolator;
import com.vc.pulltorefresh.PullToRefreshBase.Mode;
import com.vc.pulltorefresh.PullToRefreshBase.OnRefreshListener;
import com.vc.pulltorefresh.PullToRefreshBase.OnRefreshListener2;

public interface IPullToRefresh<T extends View> {
    Mode getCurrentMode();

    boolean getFilterTouchEvents();

    Mode getMode();

    int getPageIndex();

    T getRefreshableView();

    boolean getShowViewWhileRefreshing();

    boolean hasPullFromTop();

    boolean isDisableScrollingWhileRefreshing();

    boolean isPullToRefreshEnabled();

    boolean isPullToRefreshOverScrollEnabled();

    boolean isRefreshing();

    void onRefreshComplete();

    void setDisableScrollingWhileRefreshing(boolean z);

    void setFilterTouchEvents(boolean z);

    void setLastUpdatedLabel(CharSequence charSequence);

    void setLoadingDrawable(Drawable drawable);

    void setLoadingDrawable(Drawable drawable, Mode mode);

    void setMode(Mode mode);

    void setOnRefreshListener(OnRefreshListener2<T> onRefreshListener2);

    void setOnRefreshListener(OnRefreshListener<T> onRefreshListener);

    void setPageIndex(int i);

    void setPullLabel(String str);

    void setPullLabel(String str, Mode mode);

    void setPullToRefreshEnabled(boolean z);

    void setPullToRefreshOverScrollEnabled(boolean z);

    void setRefreshing();

    void setRefreshing(boolean z);

    void setRefreshingLabel(String str);

    void setRefreshingLabel(String str, Mode mode);

    void setReleaseLabel(String str);

    void setReleaseLabel(String str, Mode mode);

    void setScrollAnimationInterpolator(Interpolator interpolator);

    void setShowViewWhileRefreshing(boolean z);
}
