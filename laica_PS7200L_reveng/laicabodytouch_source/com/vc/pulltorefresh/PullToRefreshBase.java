package com.vc.pulltorefresh;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public abstract class PullToRefreshBase<T extends View> extends LinearLayout implements IPullToRefresh<T> {
    static final boolean DEBUG = false;
    static final Mode DEFAULT_MODE = null;
    static final float FRICTION = 2.0f;
    static final String LOG_TAG = "PullToRefresh";
    static final int MANUAL_REFRESHING = 3;
    static final int PULL_TO_REFRESH = 0;
    static final int REFRESHING = 2;
    static final int RELEASE_TO_REFRESH = 1;
    public static final int SMOOTH_SCROLL_DURATION_MS = 200;
    public static final int SMOOTH_SCROLL_LONG_DURATION_MS = 325;
    static final String STATE_CURRENT_MODE = "ptr_current_mode";
    static final String STATE_DISABLE_SCROLLING_REFRESHING = "ptr_disable_scrolling";
    static final String STATE_MODE = "ptr_mode";
    static final String STATE_SHOW_REFRESHING_VIEW = "ptr_show_refreshing_view";
    static final String STATE_STATE = "ptr_state";
    static final String STATE_SUPER = "ptr_super";
    private OnClickListener footOnClickListener;
    private Mode mCurrentMode;
    private SmoothScrollRunnable mCurrentSmoothScrollRunnable;
    private boolean mDisableScrollingWhileRefreshing;
    private boolean mFilterTouchEvents;
    private FootLoadingLayout mFooterLayout;
    private int mHeaderHeight;
    private LoadingLayout mHeaderLayout;
    private float mInitialMotionY;
    private boolean mIsBeingDragged;
    private float mLastMotionX;
    private float mLastMotionY;
    private Mode mMode;
    private OnRefreshListener<T> mOnRefreshListener;
    private OnRefreshListener2<T> mOnRefreshListener2;
    private boolean mOverScrollEnabled;
    T mRefreshableView;
    private FrameLayout mRefreshableViewWrapper;
    private Interpolator mScrollAnimationInterpolator;
    private boolean mShowViewWhileRefreshing;
    private int mState;
    private int mTouchSlop;
    private int pageIndex;

    // jadx: inconsistent code
    public enum Mode {
        public static final Mode BOTH = null;
        public static final Mode DISABLED = null;
        public static final Mode PULL_DOWN_TO_REFRESH = null;
        public static final Mode PULL_UP_TO_REFRESH = null;
        private int mIntValue;

        private Mode(String str, int i, int i2) {
            throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n");
        }

        public static Mode mapIntToMode(int i) {
            throw new Error("Unresolved compilation problem: \n");
        }

        boolean canPullDown() {
            throw new Error("Unresolved compilation problem: \n");
        }

        boolean canPullUp() {
            throw new Error("Unresolved compilation problem: \n");
        }

        int getIntValue() {
            throw new Error("Unresolved compilation problem: \n");
        }
    }

    public interface OnLastItemVisibleListener {
        void onLastItemVisible();

        static {
            throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n");
        }
    }

    public interface OnRefreshListener2<V extends View> {
        void onPullDownToRefresh(PullToRefreshBase pullToRefreshBase);

        void onPullUpToRefresh(PullToRefreshBase pullToRefreshBase, int i);

        static {
            throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n");
        }
    }

    public interface OnRefreshListener<V extends View> {
        void onRefresh(PullToRefreshBase pullToRefreshBase);

        static {
            throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n");
        }
    }

    final class SmoothScrollRunnable implements Runnable {
        static final int ANIMATION_DELAY = 10;
        private boolean mContinueRunning;
        private int mCurrentY;
        private final long mDuration;
        private final Interpolator mInterpolator;
        private final int mScrollFromY;
        private final int mScrollToY;
        private long mStartTime;
        final /* synthetic */ PullToRefreshBase this$0;

        public SmoothScrollRunnable(PullToRefreshBase pullToRefreshBase, int i, int i2, long j) {
            throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n");
        }

        public void run() {
            throw new Error("Unresolved compilation problem: \n");
        }

        public void stop() {
            throw new Error("Unresolved compilation problem: \n");
        }
    }

    protected abstract T createRefreshableView(Context context, AttributeSet attributeSet);

    protected abstract boolean isReadyForPullDown();

    protected abstract boolean isReadyForPullUp();

    public PullToRefreshBase(Context context) {
        throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n");
    }

    public PullToRefreshBase(Context context, AttributeSet attributeSet) {
        throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n");
    }

    public PullToRefreshBase(Context context, Mode mode) {
        throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n");
    }

    public void addView(View view, int i, LayoutParams layoutParams) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final Mode getCurrentMode() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final boolean getFilterTouchEvents() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final Mode getMode() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final T getRefreshableView() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final boolean getShowViewWhileRefreshing() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final boolean hasPullFromTop() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final boolean isDisableScrollingWhileRefreshing() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final boolean isPullToRefreshEnabled() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final boolean isPullToRefreshOverScrollEnabled() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final boolean isRefreshing() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void onRefreshComplete() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void setDisableScrollingWhileRefreshing(boolean z) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void setFilterTouchEvents(boolean z) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setLastUpdatedLabel(CharSequence charSequence) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setLoadingDrawable(Drawable drawable) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setLoadingDrawable(Drawable drawable, Mode mode) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setLongClickable(boolean z) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void setMode(Mode mode) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void setOnRefreshListener(OnRefreshListener<T> onRefreshListener) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void setOnRefreshListener(OnRefreshListener2<T> onRefreshListener2) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setPullLabel(String str) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setPullLabel(String str, Mode mode) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void setPullToRefreshEnabled(boolean z) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void setPullToRefreshOverScrollEnabled(boolean z) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void setRefreshing() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void setRefreshing(boolean z) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setRefreshingLabel(String str) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setRefreshingLabel(String str, Mode mode) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setReleaseLabel(String str) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setReleaseLabel(String str, Mode mode) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setScrollAnimationInterpolator(Interpolator interpolator) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void setShowViewWhileRefreshing(boolean z) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setPageIndex(int i) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public int getPageIndex() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected final void addViewInternal(View view, int i, LayoutParams layoutParams) {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected final void addViewInternal(View view, LayoutParams layoutParams) {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected LoadingLayout createLoadingLayout(Context context, Mode mode, TypedArray typedArray) {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected final FootLoadingLayout getFooterLayout() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected final int getHeaderHeight() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected final LoadingLayout getHeaderLayout() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected FrameLayout getRefreshableViewWrapper() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected final int getState() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void handleStyledAttributes(TypedArray typedArray) {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void onPullToRefresh() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void onReleaseToRefresh() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void onRestoreInstanceState(Parcelable parcelable) {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected Parcelable onSaveInstanceState() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void resetHeader() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected final void setHeaderScroll(int i) {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void setRefreshingInternal(boolean z) {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected final void smoothScrollTo(int i) {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected final void smoothScrollTo(int i, long j) {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void updateUIForMode() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setFootViewClickEvent() {
        throw new Error("Unresolved compilation problem: \n");
    }

    private void addRefreshableView(Context context, T t) {
        throw new Error("Unresolved compilation problem: \n");
    }

    private void init(Context context, AttributeSet attributeSet) {
        throw new Error("Unresolved compilation problems: \n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n\tR cannot be resolved to a variable\n");
    }

    private boolean isReadyForPull() {
        throw new Error("Unresolved compilation problem: \n");
    }

    private void measureView(View view) {
        throw new Error("Unresolved compilation problem: \n");
    }

    private boolean pullEvent() {
        throw new Error("Unresolved compilation problem: \n");
    }

    private void refreshLoadingViewsHeight() {
        throw new Error("Unresolved compilation problem: \n");
    }
}
