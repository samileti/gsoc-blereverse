package com.vc.pulltorefresh;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListAdapter;
import com.vc.pulltorefresh.PullToRefreshBase.Mode;
import com.vc.pulltorefresh.PullToRefreshBase.OnLastItemVisibleListener;

public abstract class PullToRefreshAdapterViewBase<T extends AbsListView> extends PullToRefreshBase<T> implements OnScrollListener {
    static final boolean DEFAULT_SHOW_INDICATOR = true;
    private View mEmptyView;
    private OnLastItemVisibleListener mOnLastItemVisibleListener;
    private OnScrollListener mOnScrollListener;
    private int mSavedLastVisibleIndex;
    private boolean mScrollEmptyView;
    private boolean mShowIndicator;

    public abstract ContextMenuInfo getContextMenuInfo();

    public PullToRefreshAdapterViewBase(Context context) {
        throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n");
    }

    public PullToRefreshAdapterViewBase(Context context, AttributeSet attributeSet) {
        throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n");
    }

    public PullToRefreshAdapterViewBase(Context context, Mode mode) {
        throw new Error("Unresolved compilation problems: \n\tThe import com.vc.lib cannot be resolved\n\tR cannot be resolved to a variable\n");
    }

    public boolean getShowIndicator() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void onScroll(AbsListView absListView, int i, int i2, int i3) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void onScrollStateChanged(AbsListView absListView, int i) {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void onScrollChanged(int i, int i2, int i3, int i4) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setAdapter(ListAdapter listAdapter) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void setEmptyView(View view) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void setScrollEmptyView(boolean z) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void setOnLastItemVisibleListener(OnLastItemVisibleListener onLastItemVisibleListener) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public final void setOnScrollListener(OnScrollListener onScrollListener) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setShowIndicator(boolean z) {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void handleStyledAttributes(TypedArray typedArray) {
        throw new Error("Unresolved compilation problem: \n\tR cannot be resolved to a variable\n");
    }

    protected boolean isReadyForPullDown() {
        throw new Error("Unresolved compilation problem: \n");
    }

    public void setPageIndex(int i) {
        throw new Error("Unresolved compilation problem: \n");
    }

    public int getPageIndex() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected boolean isReadyForPullUp() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void onPullToRefresh() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void onReleaseToRefresh() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void resetHeader() {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void setRefreshingInternal(boolean z) {
        throw new Error("Unresolved compilation problem: \n");
    }

    protected void updateUIForMode() {
        throw new Error("Unresolved compilation problem: \n");
    }

    private void addIndicatorViews() {
        throw new Error("Unresolved compilation problem: \n");
    }

    private boolean getShowIndicatorInternal() {
        throw new Error("Unresolved compilation problem: \n");
    }

    private boolean isFirstItemVisible() {
        throw new Error("Unresolved compilation problem: \n");
    }

    private boolean isLastItemVisible() {
        throw new Error("Unresolved compilation problem: \n");
    }

    private void removeIndicatorViews() {
        throw new Error("Unresolved compilation problem: \n");
    }

    private void updateIndicatorViewsVisibility() {
        throw new Error("Unresolved compilation problem: \n");
    }
}
