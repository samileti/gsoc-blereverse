package org.androidannotations.api.sharedpreferences;

import android.content.SharedPreferences;

public final class FloatPrefField extends AbstractPrefField {
    private final float defaultValue;

    FloatPrefField(SharedPreferences sharedPreferences, String key, float defaultValue) {
        super(sharedPreferences, key);
        this.defaultValue = defaultValue;
    }

    public float get() {
        return getOr(this.defaultValue);
    }

    public float getOr(float defaultValue) {
        float f;
        try {
            f = this.sharedPreferences.getFloat(this.key, defaultValue);
        } catch (ClassCastException e) {
            try {
                f = Float.parseFloat(this.sharedPreferences.getString(this.key, "" + defaultValue));
            } catch (Exception e2) {
                throw e;
            }
        }
        return f;
    }

    public void put(float value) {
        apply(edit().putFloat(this.key, value));
    }
}
