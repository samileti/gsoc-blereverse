package org.androidannotations.api.sharedpreferences;

import android.content.SharedPreferences;

public final class IntPrefField extends AbstractPrefField {
    private final int defaultValue;

    IntPrefField(SharedPreferences sharedPreferences, String key, int defaultValue) {
        super(sharedPreferences, key);
        this.defaultValue = defaultValue;
    }

    public int get() {
        return getOr(this.defaultValue);
    }

    public int getOr(int defaultValue) {
        int i;
        try {
            i = this.sharedPreferences.getInt(this.key, defaultValue);
        } catch (ClassCastException e) {
            try {
                i = Integer.parseInt(this.sharedPreferences.getString(this.key, "" + defaultValue));
            } catch (Exception e2) {
                throw e;
            }
        }
        return i;
    }

    public void put(int value) {
        apply(edit().putInt(this.key, value));
    }
}
