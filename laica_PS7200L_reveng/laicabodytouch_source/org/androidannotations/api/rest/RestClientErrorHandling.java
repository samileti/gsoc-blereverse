package org.androidannotations.api.rest;

public interface RestClientErrorHandling {
    void setRestErrorHandler(RestErrorHandler restErrorHandler);
}
