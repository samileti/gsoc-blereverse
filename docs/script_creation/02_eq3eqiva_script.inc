-----------------
Eq3 Eqiva Script
-----------------

An english translation of the software is available
`here <https://gitlab.com/sergioalberti/gsoc-blereverse/tree/master/eq3_eqiva_reveng/reference_implementation>`_
(while `here <http://sl-lab.it/dokuwiki/doku.php/tesi:reveng-termovalvole>`_ is the original) and consists of:

* a series of functions useful to interact with a single valve
* some scripts for the simultaneous management of multiple valves

Everything has been developed and tested on a *bash shell* and this should ensure portability on most *Unix*
systems without having to make many changes. However, the :ref:`BlueZ stack <bluez_stack>` dependency limits
its use to GNU/Linux operating systems.

.. container:: update

    .. container:: update-title

        Updated:

    We have created an
    `updated version <https://gitlab.com/sergioalberti/gsoc-blereverse/tree/master/eq3_eqiva_reveng/updated_implementation>`_ of
    the scripts. It corresponds to the one implemented in the
    `eq3eqiva .deb package <https://gitlab.com/sergioalberti/gsoc-blereverse/blob/master/eq3_eqiva_reveng/eq3eqiva-deb/eq3eqiva_1.0-1_all.deb>`_.
    The next sections will refer to the "original" (old) version. Changes and additions of the new implementation will
    be reported with the **Updated** tag.


.. _gplv3_license:

GPLv3 License
=============

As mentioned in the :doc:`Introduction <../introduction/00_introduction>`, the project is (among other things) aimed at providing
the possibility to integrate these valves into free home automation systems.

The code is then released under the **GPLv3** license (GNU General Public License), the conditions of which have
to guarantee the *four fundamental freedoms* [1]_ defined by the Free Software Foundation:

1. The freedom to run the program as you wish, for any purpose (freedom 0).

2. The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1).
   Access to the source code is a precondition for this.

3. The freedom to redistribute copies so you can help others (freedom 2).

4. The freedom to distribute copies of your modified versions to others (freedom 3). By doing this you can give
   the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.

Basically, it is a noticeably **copyleft** license: anyone who wants to distribute copies of a software bound to
these conditions, whether free or behind the payment of a price, is obliged to recognize the same rights that it
received to the recipient. He must also guarantee access to the source code.

.. _single_valve_management:

Single Valve Management
========================

Everything necessary to manage a single valve is contained in two files:

* ``basic_functions.sh`` deals with *sending* and *receiving* data, translating notifications and also contains
  some frequently used functions.
* ``valve_commands.sh`` deals with the syntactic/semantic *composition* of each command

Both files are commented and describe each function, therefore only some relevant aspects will be discussed below.

It is interesting to note that, although the *CalorBT* application requires a **pairing procedure** in order to be able
to communicate with the valve, this procedure is not necessary outside the Android/iOS context. This is clearly a
**security flaw** because it allows communication with the device while knowing only the MAC address, which can easily
be obtained through external tools such as the already described :ref:`hcitool <hcitool_section>`

basic_functions.sh File
------------------------

.. _send_command_function:

**send_command()**

::

    input: <device_address> <command>
    output: value of the notification received after execution

It writes the value of the ``command`` argument on the :ref:`"send command" characteristic <send_command_char>`,
thus causing the execution of the corresponding command.

The transmission is based on the use of the *Gatttool* tool, as discussed in the :ref:`previous section <bluez_stack>`,
through the following line of code:

.. code-block:: shell

    output=$(timeout $TIMEOUT_SEC gatttool -b $1 --char-write-req -a 0x0411 -n $2 --listen)

The parameters ``-b`` and ``-n`` allow to specify respectively the address of the device and the value to send; at
the time of execution they will be replaced by ``<device_address>`` and ``<command>``. The use of ``-listen`` puts the
tool in a locked state, waiting for one or more notifications from the counterpart.

However, there is no way to indicate a temporal term to this condition, and it is necessary to use ``timeout $ TIMEOUT SEC``
to store what is received in the output variable after a certain period of time and move on to the next instruction. The
``TIMEOUT SEC`` variable is defined in the ``config.sh`` file and will be detailed in the :ref:`Field Test section <field_test>`.

-----------------------------------------------

.. _parse_value_function:

**parse_return_value()**

::

    input: <notification_value>
    output: notification translated into readable content

It translates the notification content and brings it to the standard output through a series of ``echo`` commands.
The parameter ``<notification_value>`` must be composed of the received bytes, **each separated by a space**: the
same format used by *Gatttool*.

The translation is performed by interpreting the value of each byte consistently with the information in section
:ref:`Notifications <notifications_description>`. If it is a :ref:`Holiday Mode notification <notifications_holiday>`,
the parsing is done with the help of the ``parse_holiday_params()`` function, which is also contained in
``basic_functions.sh``, using bytes 7, 8, 9 and 10.

.. _profile_req_screen:
.. figure:: ../images/profile_request.png
    :scale: 125%
    :alt: profile_req.sh example
    :align: center

    profile_req.sh example

As an example, the ``profile_req.sh`` script allows to request information on a day schedule using the appropriate command.
The output produced is an example of using the ``parse_return_value()`` function. The :ref:`above figure <profile_req_screen>`
shows the translation of the notification received following the request relating to the day of Tuesday
(indicated by the parameter ``02``) to the valve called "camera".

------------------------------------------------------

**calculate_temp()**

::

    input: temperature in decimal base
    output: (temperature*2) in hexadecimal base and rounded

It allows to encode the temperature according to the format used by the valve. The result is obtained by multiplying by 2 and
then rounding to a value equal to ``XX.0`` or ``XX.5``.

**Notes**

This function must not be used for the :ref:`Select Holiday Mode <select_holiday_mode>` command.
It uses the rule ``(temperature*2)+128``. For this purpose there is the function ``calculate_temp_128()``, which is also
contained in ``basic_functions.sh``.

--------------------------------------------------------

.. _search_by_name:

**search_by_name()**

::

    input: <valve_name> <file_name>
    output: valve MAC address (-1 if it does not exists)

It allows to find the **MAC address** of a single device.

For this purpose, each valve is identified by a user-assigned name within the file ``file_name``. Within this file, each
valve must be represented on a new line according to the format: ``NAME/ADDRESS``. The function does not distinguish between
uppercase and lowercase letters; therefore, the line ``valve1/00:11:22:33:44:55`` is completely equivalent to
``VALVE1/00:11:22:33:44:55``.

The file is scanned line by line: if there are duplicates, the *first occurrence* will be selected.

.. container:: update-large

    .. container:: update-title-large

        Updated:

    The following functions have been included in the updated version.

    **check_bt_status()**

    ::

        input: -
        output: an error message if Bluetooth isn't active

    Refer to :ref:`this section <check_bt_status_fun>` for more details.

    .. _validate_mac_fun:

    **validate_mac()**

    ::

        input: <MAC_address>
        output: 0 if valid, -1 otherwise

    Check if a MAC address is syntactically valid. To be *valid*, an address must be composed of a succession of six pairs
    of hexadecimal values, separated by "``:``". The check is done through a
    `regular expression <https://en.wikipedia.org/wiki/Regular_expression>`_, using the following code.

    .. code-block:: shell

        #check if $1 is a valid mac address
        if [[ "$1" =~ ^([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}$ ]]; then
            echo "0"
        else
            echo "-1"
        fi

    In particular, it requires that a pair ``{2}`` of hexadecimal values ``[a-fA-F0-9]`` be repeated five times ``{5}``,
    each time followed by "``:``". There must then be a sixth pair, again identified by ``[a-fA-F0-9]{2}``, which must not
    be followed by the separator "``:``" (because it is the last pair).


valve_commands.sh File
------------------------

As already disclosed, ``valve_commands.sh`` manages the *syntactic composition* of every possible command and requires
its execution through the following functions. Note that all functions **automatically round the entered temperature**
to values of the type ``XX.0`` or ``XX.5``.

* **send_init()** ``<device_address>``
    Send the current date and time, automatically calculated through the command ``date``. It is not essential, but it is
    useful to start the communication in order to guarantee the synchronization between the central device and the valve
    and to receive a notification that reports the status.

* **boost_mode()** ``<device_address>``
    Causes `boost mode` activation.

* **stop_boost_mode()** ``<device_address>``
    Causes `boost mode` deactivation.

* **auto_mode()** ``<device_address>``
    Activate the automatic mode and adjust the temperature accordingly (as selected in the weekly schedule).

* **manual_mode()** ``<device_address>``
    Activate the manual mode.

* **set_temperature()** ``<device_address>`` ``<temperature>``
    Set the temperature to the value indicated by the second parameter.

* **set_comfort_reduction_temp()** ``<device_addr>`` ``<comf_temp>`` ``<red_temp>``
    Changes the *"comfort temperature"* and *"reduced temperature"* values within the valve settings.

* **holiday_mode()** ``<device_address>`` ``<DD/MM/YYYY>`` ``<hh:mm>`` ``<temperature>``
    Activate the holiday mode; therefore maintains the same temperature until the end indicated by the parameters.

* **read_profile()** ``<device_address>`` ``<day>``
    Require the daily schedule. Days of the week are counted starting from Saturday (``00`` is *Saturday*, .., ``06`` is *Friday*)

* **set_profile()** ``<device_address>`` ``<day>`` ``<int1>`` ``[int2]`` ``[int3]`` ``[int4]`` ``[int5]`` ``[int6]`` ``[int7]``

    Set the daily schedule. Days of the week are counted starting from Saturday (``00`` is *Saturday*, .., ``06`` is *Friday*).
    Each interval ``intX`` must be in the form ``TEMPERATURE/hh:mm`` and together they must guarantee coverage for the whole
    day following the guidelines in the section :ref:`Set Daily Profile <set_daily_profile>`. As a result, the intervals 2-7 may
    turn out to be unnecessary and certainly the last one specified must have the time ``24:00`` or ``00:00``.

* **lock()** ``<device_address>``
    Locks the physical keys on the valve.

    NOTE: This does not prevent interaction through Bluetooth.

* **unlock()** ``<device_address>``
    Unlocks the physical keys on the valve.

* **set_window()** ``<device_address>`` ``<temperature>`` ``<duration>``
    Set *window mode* temperature and duration.

* **set_offset()** ``<device_address>`` ``<temperature>``
    Set the offset temperature. It must be between -3.5 and +3.5.

The following is a general example of how these functions work, since they are all quite similar.

.. code-block:: shell
    :linenos:
    :class: code-with-numbers

    read_profile() {
        if [ $2 -lt 0 -o $2 -gt 6 ]; then
            echo "Week goes from 00 (saturday) to 06 (friday)."
            return
        fi

        day=$(printf "%02x" $2) # $2 = day
        echo $( send_command $1 20$day ) # $1 = device_address
    }

The first part, represented here by lines 2-7 but not always necessary, checks the correctness of the inputs and calculates the
coding of the parameters according to the format required by the valve. The second part (*line 8*) sends the command using the
:ref:`send_command <send_command_function>` function and prints the notification received.

**Parsing/Translation**

As shown in the example, the default behavior does not provide *parsing* and *translation* of what has been received. For this
to happen you need to use the :ref:`parse_return_value <parse_value_function>` method, in two possible alternative ways:

1. replacing it with the ``echo`` command in the last line of each function (*line 8 in the example*)::

    parse_return_value $( send_command $1 20$day )

2. moving the ``parse_return_value()`` call outside of the requested function (*read_profile()* in the example), thus
   obtaining the following code::

    parse_return_value $( read_profile ....... )

Multiple Valve Management
==========================

The functions described in the previous section are useful to create larger scripts that automate the management of
multiple valves. Below are listed the **scripts** made, which want to form a simple guideline in the development of larger
projects.

**Configuration**

For simplicity, using the scripts requires assigning a name chosen by the user to the MAC address of each valve. It is
therefore necessary to indicate which will be the file containing the *name-address associations* through the
``VALVE_FILE`` variable inside the ``config.sh`` file.

The file referred to by ``VALVE_FILE`` must be compiled according to the syntax required by the documentation of
:ref:`search_by_name <search_by_name>`, thus using the format ``NAME/MAC_ADDRESS``.

MAC addresses can be found as described in the :ref:`Bluez Stack <bluez_stack>` section through the ``hcitool lescan`` command.

In order to run the scripts you need execution permissions:

.. code-block:: shell

    $ cd /path/to/scripts/directory
    $ chmod u+x *.sh

Then, to run a script:

.. code-block:: shell

    $ ./script_name.sh parameter1 parameter2

.. _parse_option:

.. container:: update

    .. container:: update-title

        Updated:

    For each script, except for *profile_req*, you can use the ``-p`` or ``--parse`` option to activate
    the parsing of the received notifications. Otherwise, the value of notifications is shown in
    hexadecimal base.

    **Example:** ``./auto_mode.sh -p bathroom kitchen``


* **auto_mode** ``<valve_name>`` ``[valve_name ...]``
    Set the "auto mode" on all the valves identified by the names provided by command line. At least one parameter is required,
    while the subsequent ones are optional.

    NOTE: the temperatures set on each valve after the execution of the script *are dependent* on how they have been programmed
    individually.

* **manual_mode** ``<temperature>`` ``<valve_name>`` ``[valve_name ...]``
    Set the "manual mode" on all the valves identified by the names provided by command line. At least one parameter is required,
    while the subsequent ones are optional.

* **set_temperature** ``<temperature>`` ``<valve_name>`` ``[valve_name ...]``
    Rounds the ``<temperature>`` value and sends it to all the valves identified by the names provided by command line.
    Requires the first and second parameters, while the subsequent ones are optional.

* **set_all_temp** ``<temperature>``
    Rounds the ``<temperature>`` value and sends it to all the valves. Names and addresses of the valves to which the
    data are sent are taken from the file referenced by ``VALVE_FILE``.

    NOTE: The file is scanned line by line. The presence of duplicates implies a double execution of the command on
    the same valve

* **set_profile** ``<profile_file>`` ``<valve_name>`` ``[valve_name ...]``
    Set profiles for one or more days on all the valves identified by the names provided by command line. The values
    to be set are supplied via a ``profile_file``. It must contain the schedule for one or more days in the following
    format::

        day (1=monday, ..., 7=sunday)
        base_temperature
        HH:MM-HH:MM-TEMP (first interval)
        HH:MM-HH:MM-TEMP (secondo optional interval)
        HH:MM-HH:MM-TEMP (third optional interval)
        end

    NOTE: within the same file, you can specify the schedule for *several days* separating each block by an empty line.

    As an example, the Monday schedule with a base temperature of 18°C and 20°C in the two ranges 06:30-08:00 and 17:00-20:00 is
    carried out in this way::

        01
        18
        06:30-08:00-20
        17:00-20:00-20
        end

    .. container:: update

        .. container:: update-title

            Updated:

        In the new implementation **days** are indicated by their names and not numerically. They must be written in English in
        complete or abbreviated form (e.g. "Saturday" is equivalent to "sat"). They are *not case sensitive*.

        The previous example becomes::

            Monday
            18
            06:30-08:00-20
            17:00-20:00-20
            end

* **profile_req** ``<day>`` ``<valve_name>`` ``[valve_name ...]``
    It requires and prints the daily schedule for all the valves identified by the names provided by command line.
    The ``<day>`` parameter is counted starting from ``01`` (*Monday*) up to ``07`` (*Sunday*).

    .. container:: update

        .. container:: update-title

            Updated:

        In the new implementation **days** are indicated by their names and not numerically. They must be written in English in
        complete or abbreviated form (e.g. "Saturday" is equivalent to "sat"). They are *not case sensitive*.

        **Example:** ``./profile_req Sunday bathroom kitchen``

The operating principle of each script is almost identical. First of all, the presence of the parameters required
to run it is checked. Then each parameter representing a device activates the search function of the MAC address.
Once this is done, the commands are sent to the valves. The following lines of code, extracted from the
``auto_mode.sh`` script, clarify the functions used.

.. _script_example:

.. code-block:: shell
    :linenos:
    :class: code-with-numbers

    #!/bin/bash
    . ./valve_commands.sh

    if [ -z $1 ]; then
        printf "Usage: ./auto_mode.sh <valve_name> [valve_name ...]\n"
        exit
    fi

    for name in "$@"; do
        address=$( search_by_name $name $VALVE_FILE )

        if [ "$address" != "-1" ]; then
            auto_mode $address
        else
            printf "%s valve not found\n" "$name"
        fi
    done

The inclusion of `valve_commands.sh`` (*line 2*) makes available all the primitives present in the section
:ref:`Single Valve Management <single_valve_management>`. This causes the implicit inclusion of ``basic_functions.sh``
and ``config.sh`` and makes usable the :ref:`search_by_name <search_by_name>` function and the ``VALVE_FILE``
variable.

*Lines 4-7* check for the required parameters and cause the script to exit if the check is not passed. The *for* loop
(*lines 9-17*) allows to go by all the names of the valves supplied as an argument to the script. From each of these
the MAC addresses are obtained. Now sending the request to the valve is simple and is based on the call to an already
known function (*line 13*): ``valve_commands.sh`` provides the necessary to carry out all the operations made
available by the *CalorBT* application.

The part of the code that deals with the transmission of data to the valve is located within the ``for`` cycle.
For this reason the requested command is sent to **one device at a time**, in the order in which the names are supplied
to the script at invocation time (see :ref:`scripts_seq_diagram`). The output produced corresponds to the notifications
received from time to time, after the execution of each command.

If the address of a valve is not found, a control (*line 12*) causes an error message to be printed (*line 15*).
Then starts the search for the next address (if required by the entered parameters).

.. _scripts_seq_diagram:
.. figure:: ../images/scripts_seq_diagram.png
    :alt: auto_mode script sequence diagram
    :align: center

    Sequence Diagram of *auto_mode.sh*

The above *Sequence Diagram* [2]_ shows the exchange of messages between three valves and the central device during
the execution of the ``auto_mode.sh`` script, activated as follows::

    ./auto_mode.sh bedroom sitting_room office

Correctly, as we have just seen, the value ``0x0400`` (:ref:`Select Auto Mode <select_auto_mode>`) is sent in succession
for each valve. The next command is sent only after receiving the notification of the previous one.

The ``set_profile.sh`` script is the only exception to this behavior, because it has to read the supplied file. This can
contain instructions related to the schedule of several days, making it necessary to send **several commands to the same valve**.
For this purpose it was decided to minimize the number of calls to the ``search_by_name()`` function by immediately searching
for the requested MAC addresses and storing the result in an array (the ``VALVES_ADDR`` variable, inside the script).

.. container:: update-large

    .. container:: update-title-large

        Updated:

    In the new implementation, in each script was added a control on Bluetooth activation, syntax validation of MAC addresses
    and the possibility to use the ``-p`` or ``--parse`` option (see the :ref:`previous note <parse_option>`).

    The first two points were obtained through the functions:

    * :ref:`check_bt_status <check_bt_status_fun>`

    * :ref:`validate_mac <validate_mac_fun>`

    The third point was obtained through the following code

    .. code-block:: shell
        :linenos:
        :class: code-with-numbers

        #check if the -p or --parse option is present
        case $1 in
                -p|--parse)
                parsing=1
                shift #discard the argument

                ..  #do things
                ;;

                *)
                parsing=0
                ;;
        esac

    If the first parameter (*represented by $1*) corresponds to ``-p`` or ``--parse``, the variable ``parsing`` is set
    to 1 and the parameter already used is discarded through the **shift** command. Now $1 contains the next parameter
    supplied by the user. In all other cases (*identified by* "``*)``"), ``parsing`` is set to 0 and no parameters need to
    be discarded.

    As a result, the overall structure of each script is a little changed. The new structure is:

    .. code-block:: shell
        :linenos:
        :class: code-with-numbers

        #check if the parameter -p is present
        .. #code above

        check_bt_status  #check if bluetooth is active

        for name in "$@"; do
          address=$( search_by_name $name $VALVE_FILE )

          if [[ "$address" != "-1" && $(validate_mac $address) != "-1" ]]; then

            notification=$(auto_mode $address)

            if [[ ! -z $notification && $parsing = 0 ]]; then
              printf "\n%s: %s\n" "$name" "$notification"
            elif [[ ! -z $notification && $parsing = 1 ]]; then
              printf "\n%s:\n" "$name"
              parse_return_value $notification
            else
              #received empty notif
              printf "\n%s: error. try to increase the timeout or move close to the valve\n" "$name"
          fi
          else
            #mac address error or valve not found
            printf "\n%s: not found in '%s' or invalid MAC address\n" "$name" "$VALVE_FILE"
          fi
        done

    First of all we check the presence of the ``-p|--parse`` option and the activation of the Bluetooth (*line 4*).
    Then there is the usual ``for`` cycle, in which there are some differences with respect to the
    :ref:`previous implementation <script_example>`.

    The *line 9* not only verifies that the MAC address has been found, but also that it is **correct at syntax level**
    (through the :ref:`validate_mac <validate_mac_fun>` function).

    Furthermore, in this version the notification received from the valve is saved in the ``notification`` variable
    (*line 11*). This is because depending on whether the ``parsing`` variable is set to ``0`` or ``1`` (respectively,
    *line 13* and *line 15*), the notification must be shown in hexadecimal values (*line 14*) or translated (*line 17*).

    The **errors** are substantially divided into two cases:

    - something went wrong in the communication and the notification received is empty (*line 20*)
    - no valve was found with that name or the MAC address entered by the user is not *syntactically* correct (*line 24*)

    In both cases error messages are printed.

.. _field_test:

Field Test
===========

The information in the previous sections was tested using:

* a Bluetooth 4.0 *Class 2 adapter* on the central device
* *Ubuntu 14.04 LTS* GNU/Linux
* the *4.101 version of the BlueZ stack* (included by default in the chosen OS)

However, the scripts have also been tested on the latest BlueZ versions.

**Range And Related Problems**

The EQ3 company, which produces the valves, declares a maximum range of 10 meters outdoors (and therefore in the absence
of obstacles) [3]_.  Despite this, the use via smartphone (through the appropriate application) allowed us to reach the distance
of 12.5 meters without drops in reception.

The signal's **range and strength** directly influence the content of the ``TIMEOUT_SEC`` variable, defined in ``config.sh``.
This variable is used in the :ref:`send_command <send_command_function>` function. Its value represents the **maximum time
(in seconds)** within which it is certain to be able to carry out the following operations:

1. connect to the valve
2. send the command
3. receive the subsequent notification

|

.. table::
    :align: center

    +------------+---------------+------------+
    |  DISTANCE  | AVERAGE TIME  | STD. DEV.  |
    +============+===============+============+
    |  2 meters  |   02.99 sec.  |  0.44 sec. |
    +------------+---------------+------------+
    |  4 meters  |   03.78 sec.  |  0.59 sec. |
    +------------+---------------+------------+
    |  6 meters  |   13.73 sec.  |  1.80 sec. |
    +------------+---------------+------------+

|

The value (time) of ``TIMEOUT_SEC`` is set by default to *5 seconds*, given the results obtained with the Class 2 adapter used
in the tests. As can be seen from the table, this adapter has signal losses even at a distance of 6 meters, making it impossible
to deepen the analysis. As a consequence, the most appropriate value was selected with respect to the results obtained under good
reception conditions (*2 and 4 meters*). In all likelihood, these values will not suffer large increases over medium to long
distances using a more powerful Bluetooth connector (i.e. *Class 1*).

.. note::
    ``TIMEOUT_SEC`` is in any case a fundamental parameter that needs to be *manually adapted* depending on the application
    context. The goal is to find a good compromise between waiting times and errors (due to not receiving the notification within
    the short time available).

The calculation of the elapsed time between connection, sending and receiving was done using the ``time`` command in the
following way::

    time gatttool -b $ADDRESS --char-write-req -a 0x0411 -n 4000

This sends, by way of example, the :ref:`Select Auto Mode <select_auto_mode>` instruction to the valve using :ref:`gatttool <gatttool_section>`.
The average times and standard deviations shown in the table result from the execution of this instruction on three different valves. In total,
*20 trials* were performed for each distance.

Each test was carried out by the same **starting state**: valves disconnected from the central device. The disconnection
automatically occurs 45 seconds after execution of a command.

**Parallel Connection**

The :ref:`gatttool <gatttool_section>` tool used for the connection does not allow parallel sending of commands.
The central device is able to communicate with only *one device at a time*.

Indeed, after a write operation of a characteristic, ``gatttool`` does not allow the execution of operations on other devices
until the actual confirmation of success (in the form of a *notification*) is received. This makes every attempt useless.

Similar Projects
================

Recently a `Python library <https://github.com/rytilahti/python-eq3bt>`_ that allows the use of these valves has been
`integrated <https://www.home-assistant.io/components/climate.eq3btsmart/>`_ into the
`home-assistant <https://www.home-assistant.io/>`_ platform. The library can be used "stand alone"
(i.e. without *home-assistant*) through a CLI interface. Here are some differences with respect to our implementation.

**Pros:**

* Better Bluetooth management, probably thanks to `bluepy <https://github.com/IanHarvey/bluepy>`_.
  It seems **more reliable** when something goes wrong and on average requires less waiting time.

* Provides information on the status of the valve battery.

**Cons:**

* Requires *python* and *bluepy* (not present in Debian repositories).

* With the CLI interface it is possible to **memorize only one valve** (by exporting the MAC address to an environment
  variable or by specifying it manually at each command). Through the ``home-assistant`` platform apparently you can pair
  "name-address" (more than one), but it is a feature implemented in ``home-assistant`` itself.

* There is no way to set up *daily schedules*. It also returns strange values (mixed with correct information) when they are read.

.. note::

    The repository does not provide information about the Eq3 Eqiva protocol: *it is not documented*.
    Although this is not a real "downside" in use, we think it is important to spread the result of a
    reverse engineering activity.

----------------------------------

.. [1] `The Free Software Foundation. What is free software? <http://www.gnu.org/philosophy/free-sw.html>`_
.. [2] `Sequence Diagram - Wikipedia <https://en.wikipedia.org/wiki/Sequence_diagram>`_
.. [3] `Bluetooth Smart Radiator Thermostat - EQ3 <https://www.eq-3.com/products/eqiva/bluetooth-smart-radiator-thermostat.html#technik>`_
