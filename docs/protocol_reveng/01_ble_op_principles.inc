BLE: Operating Principles
==========================
The connection and transmission of data between two devices requires multiple steps and involves
multiple elements [1]_.
The most important high-level components are listed and discussed below.

.. (atrent) bibliografia?!? almeno qualche link ai siti ufficiali degli standard
.. (sergioalberti) [CHIUSO]

GAP (Generic Access Profile)
-----------------------------
In order to notify the presence of a BLE device to the outside world, a process called *advertising*
is necessary. It basically consists of constantly sending informative packets to devices enabled
to use Bluetooth within a certain distance.

What manages the aspects related to the connection, to the advertising and finally determines whether
two devices can interact with each other is the GAP, acronym of Generic Access Profile ([2]_ and [3]_).

The division into **roles** is fundamental. We will distinguish:

+ **Peripheral devices** (or just *peripheral*), with few resources and extremely variable nature
+ **Central devices**, such as smartphones, tablets and computers with a lot of computing power and memory

.. _topology_fig:
.. figure:: ../images/topology.*
    :alt: BLE topology
    :align: center

    Topology [5]_

Peripheral devices cannot be connected to one another, they only communicate with central device one at a time (see :ref:`topology_fig`).
Therefore, when a connection is established, they will block the advertising process until the connection is terminated.

In contrast, central devices can simultaneously manage data exchange with multiple devices. Consequently, the communication
between two peripheral devices requires the creation of a special system that exploits this possibility.

Usually, the classic BLE devices in IoT (valves, lamps, scales and so on) fall into the type of *peripheral devices* while the application
that manages them is installed on a *central device*. This is also what respectively happen with the radiator valves
and the CalorBT application that we are considering.

.. _gatt_section:

GATT (Generic Attribute Profile)
--------------------------------
When the connection is established, the bidirectional transmission takes place through the ATT
protocol (*Attribute Protocol*) and uses the concepts of GATT profile, service and characteristic [4]_,
which will be discussed soon.

A significant aspect is given by the relationship that is created between the peripheral device and
the central device. The former is referred to as **GATT Server** (or Slave) as it provides services and
characteristics, while the latter is called **GATT Client** (or Master).

All transactions start from the Master and receive a response from the Slave which, at the time of the
first connection, suggests a connection interval. At the end of each interval the Master reconnects to
check the availability of new data. This is only a suggestion provided by the peripheral, which however
does not place time constraints on the central device.

As already mentioned, GATT transactions are based on hierarchical high-level objects: profiles, services
and characteristics (see :ref:`gatt_hierarchy_fig`).

.. _gatt_hierarchy_fig:
.. figure:: ../images/gatt_hierarchy.*
    :alt: Hierarchy of profiles, services and characteristics
    :align: center

    Hierarchy of profiles, services and characteristics [5]_

**Profiles**

Profiles define possible applications of the device, describing its functionality and use cases.
The BLE specification provides a wide range of standard profiles that are used in various fields,
but also allows manufacturers to create new profiles using GATT. This facilitates the development
of innovative applications that still maintain interoperability with other Bluetooth devices.

For example, the "Blood Pressure Profile" and the "Proximity Profile" are *predefined* profiles.
They are designed to be implemented by a blood pressure meter and to monitor the distance between
two devices.

**Services**

Services allow to perform a first and not very detailed logical division of the data. They are
composed of one ore more characteristics and are identified through a *UUID* consisting of:

+ **16 bit** for the predefined services
+ **128 bit** for those created specifically by peripheral devices manufacturers

For example, the aforementioned "Blood Pressure Profile" provides the services "Blood Pressure
Service" and "Device Information Service", necessary for the transmission of data about blood
pressure and device status.

**Characteristics**

Characteristics are the main interaction point with the BLE peripheral and represent the most
granular layer in the logical data division. Each characteristic handles information related to
a single aspect, dealing with the transmission in one or both directions. For this reason they
will be given **properties** such as *Read* or *Write*. Like the services, they are also identified
through 16 or 128 bit UUIDs.

For example, the "Blood Pressure Measurement" characteristic provided by the "Blood Pressure
Service" can be used to read the values measured by the blood pressure meter.

**Notifications**

As already mentioned, it is usually the GATT Client (*the Central device*) that initiates a
transaction. However, even if they are not represented in the hierarchy, the BLE provides
**Notifications** and **Indications** so that the GATT Server (*the Peripheral device*) can
request for data or simply send information to the counterpart with or without an explicit
signal from the latter.

In general, notifications are used to inform the client about the value assumed by a characteristic.
For this reason, they are one of the possible values that can be assigned to the properties of a
characteristic, together with the already mentioned Read and Write.

In order for the mechanism to work, an explicit request to receive notifications from the client
is required.

To better clarify these concepts, consider that one of the characteristics defined
by the manufacturers of the radiator valves *EQ3 Eqiva* has the following specifications:

+ UUID: d0e8434d-cd29-0996-af41-6c90f4e0eb2a
+ Property: read/write/notify

The 128-bit UUID allows us to understand that the characteristic has been defined by the producers
and its properties tell us what operations we can perform on it.

.. (atrent) {vale in generale} valuta tu (Sergio) quanto approfondire i dettagli del protocollo BLE, qui basta dare un'idea generale del funzionamento e poi fare riferimento (cfr. mio todo su bibliografia) a documentazione ufficiale
.. (sergioalberti) in generale penso che questo basti per comprendere tutto il resto della guida (non so se siete d'accordo). magari anche a seconda del tempo rimanente inserisco la differenza a livello di connessione tra Bluetooth e BLE se riesco a scriverla senza complicare troppo le cose?
.. (atrent) per me è un SI
.. .. [CHIUSO] (sergioalberti)

-----------------------------------------

.. [1] `Bluetooth Core Specification 5.0, Volume 1, Part A, Section 1.2 <https://www.bluetooth.com/specifications/bluetooth-core-specification>`_
.. [2] `Bluetooth Core Specification 5.0, Volume 1, Part A, Section 6.2 <https://www.bluetooth.com/specifications/bluetooth-core-specification>`_
.. [3] `Bluetooth Core Specification 5.0, Volume 3, Part C, Page 1966 <https://www.bluetooth.com/specifications/bluetooth-core-specification>`_
.. [4] `Bluetooth Specifications - GATT Overview <https://www.bluetooth.com/specifications/gatt/generic-attributes-overview>`_
.. [5] `Kevin Townsend. Introduction to bluetooth low energy. A basic overview of key concepts for BLE. <https://learn.adafruit.com/ introduction-to-bluetooth-low-energy/>`_
