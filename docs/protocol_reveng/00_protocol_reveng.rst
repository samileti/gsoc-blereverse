========================================
Application Protocol Reverse Engineering
========================================

.. (atrent) {vale DAPPERTUTTO!} mi raccomando, niente copia incolla da internet senza citare correttamente (i.e., tra virgolette, con indicazione della fonte)
.. (sergioalberti) recepito. al momento non dovrei (verifico) aver fatto copia/incolla tranne in un punto del cap.3 in cui ho citato la fonte!

Bluetooth Low Energy (*BLE* or *Bluetooth Smart*) technology was born as a personal project
of the Finnish company Nokia and only in 2010 was introduced in the Bluetooth 4.0 specification.

BLE has gained importance in the Internet Of Things (*IoT*) because it wants to ensure low energy
consumption while maintaining a good range of communication. This is the reason why the main producers
of mobile and desktop OSes provide complete support, allowing to design devices able to
communicate with all the most modern platforms.

The reverse engineering work is carried out through two parallel actions:

* Logging and inspection of Bluetooth packages exchanged between the smartphone and the BLE device
* Decompilation of the Android application

This allows to understand the protocol used and exploit the application code to verify its
correctness, also to ensure greater consistency with respect to the original specifications.

.. note::
    This guide requires that you have an Android device available. However, there are advantages in
    using an emulator. If you are interested in this, take a look at the
    :ref:`Logging With An Emulator <logging_emulator>` section.

.. include:: 01_ble_op_principles.inc

.. include:: 02_bt_ble_diff.inc

.. include:: 03_logging_via_android.inc

.. include:: 04_android_app_analysis.inc

.. include:: 05_other_guides.inc


.. (atrent) IDEA, credo importante, potresti promuovere questo progetto a un progetto di crowdsourcing invitando altri "hackers" a provare la tua guida e ad aggiungere capitoli su nuovi device, cioè potresti proprio anche scrivere una meta-guida, un piccolo template, una checklist da portare a termine per aggiungere un device alla lista di quelli revengizzati
.. (sergioalberti) ho fatto un tentativo creando la pagina "Contributions", non so se puo' andar bene (almeno vagamente)!
