
.. _bt_ble_diff:

BT/BLE: Main Differences
========================
This section is not essential to understand the rest of *this* guide, but it is still useful to better figure
out what is detected through the :ref:`logging activity <logging_via_android>`.

The next paragraphs explain the main differences between the classic Bluetooth implementation
(`Bluetooth BR/EDR <https://learn.sparkfun.com/tutorials/bluetooth-basics/common-versions>`_) and the
Bluetooth Low Energy. The goal is to deepen some details related to aspects of connection and consumption.
Receiving and transmitting data requires a lot of energy and consequently interesting solutions have been
studied to optimize these activities for some use cases.

Important aspects of Bluetooth BR/EDR [15]_:

* transmits all **types of data** (including audio/video streams), ensuring high throughput
* **requires pairing**
* allows the use of different topologies (`Piconet <https://en.wikipedia.org/wiki/Piconet>`_ and `Scatternet <https://en.wikipedia.org/wiki/Scatternet>`_)

Important aspects of Bluetooth Low Energy [15]_:

* **asynchronous data exchanges**, with low throughput (no streams)
* **optional pairing**
* basically uses Piconet topology
* low consumption: allows to power devices with *coin cell batteries* and still last over time

Just above the physical layer, which we will not cover, there are two very different **Data Link** layers.
Both define a series of states in which a *single device* can be. The :ref:`next image <state_machines_fig>`
immediately highlights how Bluetooth BR/EDR devices are more complex.

.. _state_machines_fig:
.. figure:: ../images/FSM.png
    :alt: BLE and BT BR/EDR states
    :align: center

    *BR/EDR* States [12]_ (left) // *BLE* States [13]_ (right)

Observing the ``Connection`` states in the two state machines, we can see the *connection oriented* nature of
the Bluetooth BR/EDR. Once the connection is established, the slave device can reduce its consumption by exploiting
some **substates** (*Sniff* or *Hold* mode) or by entering in *Park mode* (see :ref:`BR/EDR Connected Slave Substates <fsm_substates_fig>`).
These alternatives keep the connection to the master device active, although *Park Mode* hides the device from the network.

.. _fsm_substates_fig:
.. figure:: ../images/FSM_substates.*
    :alt: bluetooth BR/EDR* connected slave substates
    :align: center

    Bluetooth *BR/EDR* Connected Slave Substates [12]_

In the case of BLE, the only way to save energy is to enter the ``Standby`` state. This however leads the device to
lose the connection and restart the ``Advertising`` or ``Initiating`` phases (respectively for *Master* and *Slave*),
creating a continuous sequence of *standby-search-connection* phases. The reason why repeating this "steps" allows
you to consume few energy and maintain low response times lies in various factors.

First of all, BLE technology uses only **3 physical search channels** (*BR/EDR* uses 32 channels). Given the time
required for packet transfer, this phase requires between ``0.6ms`` and ``1.2ms`` (while ``22.5ms`` in *BR/EDR*). This
leads to a power saving of 10-20 times compared to the classic Bluetooth [14]_ .

.. note::

    Given these different "architectures":

    * using Bluetooth *BR/EDR* a master can connect with **up to 7 slaves** in active mode and 255 in park mode
    * using BLE there are **no theoretical limitations** on the number of slaves to which a master can be connected

Still within the Data Link level, substantial differences are present in the transmission of packets:

    * In Bluetooth BR/EDR each communication channel is divided into **slots of 625us** used alternately by the
      master and the slave. They can transmit a packet per slot. However, sending a package can take up to *5 slots*.

    * In BLE the time units are **Events**. They vary in length depending on the decisions of the
      master (for *connection events*) and the advertiser (for *advertising events*).

While the Bluetooth BR/EDR uses a very strict transmission method, the one used by BLE is more flexible and can be
optimized according to various parameters.

As an example, for Connections Events, the BLE specification provides a ``connInterval`` [16]_ value which indicates
the minimum time that must elapse between two consecutive events of this type (between *7.5ms* and *4s*).
Another parameter, ``connSlaveLatency`` [16]_, defines the number of Connection Events in which the slave is not forced
to listen to the master and can stay in standby. This parameters are responsible for **consumption and latency times**
and exist also for the Advertising Events.

By increasing ``connSlaveLatency`` and keeping ``connInterval`` low, you can guarantee excellent consumption
without lengthening latency times too much.

As already mentioned, the BR/EDR standard is designed to transmit any type of information, while the BLE prefers
a few data at a time. The result is that in the first case several types of **logical transport** are defined:
`SCO and eSCO <https://en.wikipedia.org/wiki/List_of_Bluetooth_protocols#Synchronous_connection-oriented_(SCO)_link>`_
for synchronous communication, `ACL <https://en.wikipedia.org/wiki/List_of_Bluetooth_protocols#Asynchronous_Connection-Less_[logical_transport]_(ACL)>`_
for the asynchronous one and two types of Broadcast. The BLE alternative instead implements **only asynchronous ACL communication**.

This leads to a big difference in package format. As the :ref:`next image <BT_packets>` shows, **BLE packages are shorter**
(max 376 bit vs 2871 bit) and therefore require less transmission time. This was achieved by removing redundant
information and limiting the payload size.

.. _BT_packets:
.. figure:: ../images/BT_packets.png
    :alt: Bluetooth BR and BLE packet format
    :align: center

    *Bluetooth BR* and *BLE* packet format

By taking advantage of all these design choices, BLE can complete a connection (scan for devices, link, send data, authenticate,
and go back to a standby state) in just ``3ms``. The same activity with Bluetooth BR/EDR takes *hundreds* of milliseconds [14]_.

Reduced times lead to lower energy consumption and lower latency.

------------------------------

.. [12] `Confronto Tra Bluetooth Basic Rate e Bluetooth Low Energy <http://tesi.cab.unipd.it/44150/1/tesi_tibertoa.pdf>`_
.. [13] `How BLE works <https://zpcat.blogspot.com/2013/10/how-bluetooth-le-works-link-layer.html>`_
.. [14] `One Small Step For Bluetooth Low Energy Technology <https://www.wirelessdesignmag.com/article/2010/08/one-small-step-bluetooth-low-energy-technology>`_
.. [15] `Ten Important Differences Between Bluetooth BREDR And Bluetooth Smart <http://blog.bluetooth.com/ten-important-differences-between-bluetooth-bredr-and-bluetooth-smart>`_
.. [16] `Bluetooth Core Specification 5.0, Volume 6, Part B, Page 2638 <https://www.bluetooth.com/specifications/bluetooth-core-specification>`_
