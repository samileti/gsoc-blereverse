Translations
=============

Translations of this guide in other languages (*even just small parts*) are always welcome.
If you are interested, `contact us <https://gitlab.com/sergioalberti/gsoc-blereverse>`_!
