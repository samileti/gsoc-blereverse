.. _contributions_sec:

==============
Contributions
==============

The writing of this guide has mostly developed around a single BLE device. However, this document
*does not* mean to be a "closed chapter". Instead, it would like to be an evolving project, in which
to gather information on reverse engineering techniques and to make available works already done in this area.

Every contribution, regarding **any section** of the guide, is welcome. If you are interested, check out
our `GitLab page <https://gitlab.com/sergioalberti/gsoc-blereverse>`_.


.. include:: 01_add_devices.inc

.. include:: 02_translations.inc
