
Add Other BLE Devices
=====================

If you reverse-engineered a BLE device (*even just some features*) and you want to add it to this project these
are short guidelines to follow. Basically, you should document your work in these two sections:

1. :ref:`protocol_description_sec` - **mandatory**
2. :ref:`script_creation_sec` - if you wrote some scripts/software

|

**"Protocol Description" Section**

This section should contain, not necessarily in this order:

* A **description of the BLE device**, its features and how to use it. In addition, you can add details,
  useful information and issues related to the *product* or *company*. (e.g. permissions required by the Android
  application or discussions/contacts with the company)

* **Information on Bluetooth communication**. That is: what features of the BLE are used to exchange data between
  the peripheral and the central device. (e.g. services/characteristics used, useful information in advertising
  packages, optional pairing)

* A **detailed description of the protocol syntax**.
  Describe accurately the composition of the commands, of the notifications or in any case of all the data concerning
  the reverse-engineering protocol.

* *Optional* - If the protocol is composed of several commands/notifications, represent it using a **formal method** for syntax and
  grammars (e.g. :ref:`EBNF <extended_bnf>`)

|

**"Script Creation" Section**

This section is quite free, depending on the scripts/software created and how they work. In general, it should contain:

* **General information** on what the scripts do, the system on which they were tested and *the license* (essential!).

* **Information on the software required** to use the scripts and possibly an installation guide.

  If you think the tools used are interesting, you can add a section about these at the top of the page, after the one
  on the :ref:`bluez_stack` (or integrating it).

* A description of:
    + **how to use the scripts** (e.g. *permissions*, *what each script does*)
    + **interesting aspects of the implementation** of each individual file/function (so that the end user understands how it works)

* *Optional* - Tests, analysis, future developments or other interesting details.

.. note::

    Scripts and other material can be uploaded to a *devicename_reveng* directory on
    our `GitLab repo <https://gitlab.com/sergioalberti/gsoc-blereverse>`_
